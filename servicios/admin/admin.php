<?php 

class admin {

	/**
	 * getBD
	 * retorna la base de datos en uso o su lista de tablas.
	 * @return string con nombre de la bd
	 */
	public static function getBD($dar='BD'){
			if($dar=='BD') return "BD";
			if($dar=='Tb') return array('');
	}

	/**
	 * accesoDef
	 * valores por defecto de acceso a cada método.
	 * @param string $metodo el nombre del método a validar 
	 * @return int nivel  de acceso
	 */
	public static function accesoDef($metodo){
		switch ($metodo) {
		    case "getBD": 			return 1; break;
		    case "total": 			return 1; break;
		    case "analyze": 		return 1; break;
		    case "accesoDef": 	return 1; break;
		    case "test": 				return 1; break;
		    case "metodos": 		return 2; break;
		    default: 						return 1; 
		}
	}

	/**
	 * entregar
	 * retorna la base de datos en uso o su lista de tablas.
	 * @return string con nombre de la bd
	 */
	public static function entregar($data=null, $fn=__FUNCTION__){
		return $data;
		if(strpos("$_SERVER[REQUEST_URI]", __CLASS__."/".$fn) !== false){
			return entregar($data);  
		}else{
			return $data;
		}
	}

	/**
	 * test
	 * pruebas varias.
	 * @return array
	 */
	public static function test($test=null){
		global $DBPG;

//		$data = $DBPG->select ("ws_rec", ["fecha", "ip", "url"], ["id[>]" => 8]);
		$data = $DBPG->select("information_schema(columns)", 
	    ["column_name", "data_type"], 
	    ["table_name" => $table]
	  );

		if(strpos("$_SERVER[REQUEST_URI]", __CLASS__."/".__FUNCTION__) !== false) return entregar($data); else return $data;
	}

	/**
	 * servicios
	 * informacion de servicios
	 * @return array
	 */
	public static function servicios(){
		$data = array_keys(dirToArray($_SERVER["DOCUMENT_ROOT"]."/servicios/"));

		if(strpos("$_SERVER[REQUEST_URI]", __CLASS__."/".__FUNCTION__) !== false) return entregar($data); else return $data;
	}

	/**
	 * metodos
	 * informacion de metodos
	 * @return array
	 */
	public static function metodos($metodos=null){
		global $conf;
		$data = array();
		if(empty($metodos)) $metodos = traerValorDe("metodos");

		$servicios = $metodos?explode(",", $metodos):self::servicios();
/*
		if($metodos) $servicios = explode(",", $metodos);
		else $servicios = admin::servicios();
*/		
		foreach($servicios as $servicio){
			$fclase = $_SERVER["DOCUMENT_ROOT"]."/servicios/$servicio/$servicio.php";
			if(is_file($fclase)){
				require_once $fclase;
				$data[$servicio] = get_class_methods($servicio);
				if($conf['servicio']['acceso']){
					$aquitar = array();
					foreach ($data[$servicio] as $key => $metodo) {
							$usuario = empty($_SESSION['usuario'])? null:$_SESSION['usuario'];
			        $ejecutar = acceso::control("$servicio::$metodo", $usuario);
							if(!$ejecutar) unset($data[$servicio][$key]);
					}
					if(empty($data[$servicio])) unset($data[$servicio]); 
				}
			}
		}

		if(strpos("$_SERVER[REQUEST_URI]", __CLASS__."/".__FUNCTION__) !== false) return entregar($data); else return $data;
	}

	/**
	 * tablas
	 * informacion de tablas
	 * @param $tablas string con el nombre de algun servicio
	 * @return array
	 */
	public static function tablas($tablas=null){
		$data = array();
		if(empty($tablas)) $tablas = traerValorDe("tablas");
		$serv = $tablas;
		if($serv){
			$fclase = $_SERVER["DOCUMENT_ROOT"]."/servicios/$serv/$serv.php";
			if(is_file($fclase)){
				require_once $fclase;
				if(method_exists($serv, "getBD")){
					$data = $serv::getBD("Tb");
				}
			}
		}else{
			$carpetas = admin::servicios();		
			foreach($carpetas as $carpeta){
				$fclase = $_SERVER["DOCUMENT_ROOT"]."/servicios/$carpeta/$carpeta.php";
				if(is_file($fclase)){
					require_once $fclase;
					if(method_exists($carpeta, "getBD")){
						$tablas = $carpeta::getBD("Tb");
						if(is_array($tablas)){
							foreach($tablas as $indice => $tabla){
								if(!in_array($tabla, $data)) $data[] = $tabla;
							}
						}
					}
				}
			}
		}
		
		if(strpos("$_SERVER[REQUEST_URI]", __CLASS__."/".__FUNCTION__) !== false) return entregar($data); else return $data;
	}

	/**
	 * total
	 * Total registros relacionados
	 * @return array de datos de persona
	 */
	public static function total(){
		$data = array();
		foreach (admin::tablas() as $tabla) $data[$tabla] = totalRegBDTable($tabla, self::getBD());
		
		if(strpos("$_SERVER[REQUEST_URI]", __CLASS__."/".__FUNCTION__) !== false) return entregar($data); else return $data;
	}

	/**
	 * analyze
	 * Actualiza las estadísticas de cada tabla
	 * @return array de datos de persona
	 */
	public static function analyze(){
		$data = array();
		foreach (admin::tablas() as $tabla) $data[$tabla] = analyzeBDTable($tabla, self::getBD());
		$clase = __CLASS__;$clase::entregar($data,__FUNCTION__);
	}



// fin clase geo
}


?>