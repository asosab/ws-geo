<?php 
require_once $_SERVER["DOCUMENT_ROOT"] ."/servicios/correo/correo.php";
require_once $_SERVER["DOCUMENT_ROOT"] ."/servicios/feedback/feedback.php";

class acceso {

	public static function test($test=null){
		if(empty($test)) $test = traerValorDe("test");
//		print_r(feedback::data("El usuario $test acaba de activar su cuenta",null));
	}

	/**
	 * getBD
	 * retorna la base de datos en uso o sus tablas.
	 * @param string $dar
	 * @return mixed string con nombre de la bd | array con lista de tablas
	 */
	public static function getBD($dar='BD'){
		if($dar=='BD') return "BD";
		if($dar=='Tb') return array('ws_usuarios','ws_usuario_grupo','ws_metodos','ws_grupos','ws_control','ws_rec');
	}

	/**
	 * accesoDef
	 * valores por defecto de acceso a cada método.
	 * @param string $metodo el nombre del método a validar 
	 * @return int nivel  de acceso
	 */
	public static function accesoDef($metodo){
		switch ($metodo) {
				case "test": 				return 1; break;
		    case "getBD": 			return 1; break;
		    case "accesoDef": 	return 1; break;
		    case "control": 		return 1; break;
		    case "getGrupo": 		return 1; break;
		    case "getMetodo":		return 1; break;
		    case "setMetodo": 	return 1; break;
		    case "setAcceso": 	return 1; break;
		    case "delAcceso": 	return 1; break;
		    case "activarUsr":	return 1; break;
		    case "bloquearUsr": return 1; break;
		    case "delUsr": 			return 1; break;
		    case "setUsr": 			return 3; break;
		    case "login": 			return 3; break;
		    case "getSesion": 	return 3; break;
		    case "getUsrGrupos":return 1; break;
		    case "logout": 			return 3; break;
		    case "activByMail": return 1; break;
		    case "activByKey": 	return 3; break;
		    case "ChgUsrKey": 	return 1; break;
		    case "getUsr": 			return 1; break;
		    case "setSesion": 	return 1; break;
		    case "setSecAk": 		return 1; break;
		    case "setApikey": 	return 1; break;
		    case "sendBienvenida": 	return 1; break;
		    case "getAuthkey": 	return 1; break;
		    case "setPermisos": return 1; break;
		    case "rec": 				return 1; break;
		    default: 						return 1; 
		    
		    
		}
	}

	/**
	 * setApikey
	 * Crea un nuevo apikey para un usuario segun su correo
	 * @param string $correo el correo del usuario
	 * @return boolean dependiendo si logra setear o no la nueva llave
	 */
	public static function setApikey($correo=null){
		$errores    = array();
		$data       = array();
		$data['estatus'] = '0';
		$wsusuarios = acceso::getBD("Tb")[0];
		if(empty($correo)) $correo	= traerValorDe("correo"); 
		if(empty($correo)) $errores['correo'] = $data['mensaje'] = 'Falta la dirección de correo';

		if (empty($errores)){

			$date 	= new DateTime();
			$apikey = md5($correo) . md5($date->getTimestamp().rand());
			$apikey	= substr($apikey, 5, 40);
			$sql = "UPDATE $wsusuarios SET apikey='$apikey' WHERE correo='$correo' RETURNING id";
			$resultado = DbLink::resultados($sql,acceso::getBD());
			if($resultado !==false){
				$data['estatus'] 	= '1';
				$data['apikey'] 	= $apikey;
				$data['mensaje'] 	= 'Se ha actualizado la llave de aplicación';
			}else{
				$data['estatus'] = '0';
				$errores['correo'] = $data['mensaje'] = 'Correo incorrecto';
			}
		}
		if (!empty($errores)){
			$data['estatus'] = '0';
			$data['error'] = $errores;
		}
		if(strpos("$_SERVER[REQUEST_URI]", __CLASS__."/".__FUNCTION__) !== false)return entregar($data); else return $data;
	}	
	
	/**
	 * setSecAk
	 * Inicia una sesión de usuario correspondiente al apikey
	 * @param string $apikey la llave a verificar
	 * @return boolean dependiendo si logra iniciar o no la sesion
	 */
	public static function setSecAk($apikey=null){
		$errores    = array();
		$data       = array();
		$data['estatus'] = '0';
		$wsusuarios = acceso::getBD("Tb")[0];
		if(empty($apikey)) $apikey	= traerValorDe("apikey"); 
		if(empty($apikey)) $errores['apikey'] = $data['mensaje'] = 'Falta la llave de verificación';
		if(empty($errores)){
			$sql = "SELECT correo FROM $wsusuarios WHERE apikey = '$apikey'";
			$resultado = DbLink::resultados($sql,acceso::getBD());
			if($resultado !==false) $correo = $resultado[0]['correo'];
			else $errores['apikey'] = $data['mensaje'] = 'No existe esa llave de verificación';
		}

		if(empty($errores)) $data = acceso::setSesion($correo);

		if (!empty($errores)){$data['estatus'] = '0';$data['error'] = $errores;}
		if(strpos("$_SERVER[REQUEST_URI]", __CLASS__."/".__FUNCTION__) !== false)return entregar($data); else return $data;
	}	

  public static function activByMail($correo=null){
    global $conf;
    if(!$conf['servicio']['correo']) $errores['servicio'] = $data['mensaje'] = 'Está desactivado el servicio de envío de correos';
		$data       = array();
		$data['estatus'] = '0';
		$wsusuarios = acceso::getBD("Tb")[0];
		if(empty($correo)) $correo	= traerValorDe("correo"); 
		if(empty($correo)) $errores['correo'] = $data['mensaje'] = 'Falta la dirección de correo';

		if(empty($errores)){
			$sql = "SELECT authkey FROM $wsusuarios WHERE correo = '$correo'";
			$resultado = DbLink::resultados($sql,acceso::getBD());
			if($resultado !==false) $authkey = $resultado[0]['authkey'];
			else $errores['correo'] = $data['mensaje'] = 'El correo no coincide con ninguno en la BD';
		}

		if(empty($errores)){
      $datos = array(
        'para'        => $correo,
        'titulo'        => $conf['app']['nombre_corto']." - Verificación de correo", 
//      'dir_html'      => $conf['app']['web_base'] ."/acceso/vista/activar/$correo/$authkey/",
        'cuerpoplano'   => "Haga click aquí para activar su cuenta: ".$conf['app']['web_base']."/acceso/activByKey/correo/$correo/authkey/$authkey/",
        );
      if(correo::enviar($datos)['estatus'] =='1') $data['mensaje'] = 'Revisa tu correo por instrucciones de activación';

		}
		if (!empty($errores)){
			$data['estatus'] = '0';
			$data['error'] = $errores;
		}
		if(strpos("$_SERVER[REQUEST_URI]", __CLASS__."/".__FUNCTION__) !== false)return entregar($data); else return $data;
  }

  public static function sendBienvenida($correo=null){
    global $conf;
    if(!$conf['servicio']['correo']) $errores['servicio'] = $data['mensaje'] = 'Está desactivado el servicio de envío de correos';
		$data = array();
		$data['estatus'] = '0';
		$wsusuarios = acceso::getBD("Tb")[0];
		if(empty($correo)) $correo	= traerValorDe("correo"); 
		if(empty($correo)) $errores['correo'] = $data['mensaje'] = 'Falta la dirección de correo';

		if(empty($errores)){
			$sql = "SELECT apikey FROM $wsusuarios WHERE correo = '$correo'";
			$resultado = DbLink::resultados($sql,acceso::getBD());
			if($resultado !==false) $apikey = $resultado[0]['apikey'];
			else $errores['correo'] = $data['mensaje'] = 'El correo no coincide con ninguno en la BD';
		}

		if(empty($errores)){
      $datos = array(
        'para'        => $correo,
        'titulo'        => $conf['app']['nombre_corto']." - Cuenta activada", 
//      'dir_html'      => $conf['app']['web_base'] ."/acceso/vista/activar/$correo/$authkey/",
        'cuerpoplano'   => "Su cuenta ha sido activada, su llave de aplicación es: $apikey \r Para usarla sólo agréguela a cualquier petición en la URL de este modo: ".$conf['app']['web_base']."/geo/entidad/tal/apikey/$apikey/ \r Procure guardar su llave de aplicación en un lugar seguro y no la comparta con nadie.",
        );
      if(correo::enviar($datos)['estatus'] =='1'){
      	$data['estatus'] = '1';
      	$data['mensaje'] = 'Correo de bienvenida enviado';
      }else{
				$data['estatus'] = '0';
      	$errores['correo'] = $data['mensaje'] = 'Error al enviar el correo de bienvenida';
      }

		}
		if (!empty($errores)){
			$data['estatus'] = '0';
			$data['error'] = $errores;
		}
		if(strpos("$_SERVER[REQUEST_URI]", __CLASS__."/".__FUNCTION__) !== false)return entregar($data); else return $data;
  }

	/**
	 * setSesion
	 * Inicia una sesión PHP de un usuario registrado
	 * @param correo string la dirección de correo
	 * @return array con datos de usuario si el usuario si existe
	 */
	public static function setSesion($correo=null){
		$errores    = array();
		$data       = array();
		$data['estatus'] = '0';
		$wsusuarios = acceso::getBD("Tb")[0];
		if(empty($correo)) $correo	= traerValorDe("correo"); 
		if(empty($correo)) $errores['correo'] = $data['mensaje'] = 'Falta la dirección de correo';

		if (empty($errores)){
			$data = acceso::getUsr($correo);
			if($data['estatus']==0) $errores['correo'] = $data['mensaje'];
		}

		if (empty($errores)){
			$data['grupos'] = acceso::getUsrGrupos($data['id']);
			session_regenerate_id();
			$_SESSION['usuario'] = $data;
		}
		if (!empty($errores)){
			$data['estatus'] = '0';
			$data['error'] = $errores;
		}
		if(strpos("$_SERVER[REQUEST_URI]", __CLASS__."/".__FUNCTION__) !== false)return entregar($data); else return $data;
	}	

	/**
	 * getUsr
	 * Obtiene la información de un usuario de la bd
	 * @param correo string la dirección de correo
	 * @return array con datos de usuario si el usuario si existe
	 */
	public static function getUsr($correo=null){
		$errores    = array();
		$data       = array();
		$data['estatus'] = '0';
		$wsusuarios = acceso::getBD("Tb")[0];
		if(empty($correo)) $correo	= traerValorDe("correo"); 
		if(empty($correo)) $errores['correo'] = $data['mensaje'] = 'Falta la dirección de correo';

		if (empty($errores)){
			$sql = "SELECT id, correo, activo, bloqueado FROM $wsusuarios WHERE correo = '$correo'";
			$resultado = DbLink::resultados($sql,acceso::getBD());
			if($resultado !==false){
				$data = $resultado[0];
				$data['estatus'] = '1';
			}else{
				$data['estatus'] = '0';
				$errores['correo'] = $data['mensaje'] = 'El correo no coincide con ninguno en la BD';
			}
		}
		if (!empty($errores)){
			$data['estatus'] = '0';
			$data['error'] = $errores;
		}
		if(strpos("$_SERVER[REQUEST_URI]", __CLASS__."/".__FUNCTION__) !== false)return entregar($data); else return $data;
	}

	/**
	 * getAuthkey
	 * Obtiene la Authkey de un usuario 
	 * @param correo string la dirección de correo
	 * @return array con datos de usuario si el usuario si existe
	 */
	public static function getAuthkey($correo=null){
		$errores    = array();
		$data       = array();
		$data['estatus'] = '0';
		$wsusuarios = acceso::getBD("Tb")[0];
		if(empty($correo)) $correo	= traerValorDe("correo"); 
		if(empty($correo)) $errores['correo'] = $data['mensaje'] = 'Falta la dirección de correo';

		if (empty($errores)){
			$sql = "SELECT authkey FROM $wsusuarios WHERE correo = '$correo'";
			$resultado = DbLink::resultados($sql,acceso::getBD());
			if($resultado !==false){
				$data = $resultado[0];
				$data['estatus'] = '1';
			}else{
				$data['estatus'] = '0';
				$errores['correo'] = $data['mensaje'] = 'El correo no coincide con ninguno en la BD';
			}
		}
		if (!empty($errores)){
			$data['estatus'] = '0';
			$data['error'] = $errores;
		}
		if(strpos("$_SERVER[REQUEST_URI]", __CLASS__."/".__FUNCTION__) !== false)return entregar($data); else return $data;
	}


	/**
	 * ChgUsrKey
	 * Cambia la authkey de un usuario
	 * @param correo string la dirección de correo
	 * @param authkey string la clave de activacion 
	 * @return array con datos de usuario si el usuario queda activado, false si no
	 */
	public static function ChgUsrKey($correo=null, $authkey=null){
		$errores    = array();
		$data       = array();
		$data['estatus'] = '0';
		$wsusuarios = acceso::getBD("Tb")[0];
		if(empty($correo)) $correo	= traerValorDe("correo"); 
		if(empty($correo)) $errores['correo'] = $data['mensaje'] = 'Falta la dirección de correo';
		if(empty($authkey)) $authkey	= traerValorDe("authkey"); 
		if(empty($authkey)) $errores['authkey'] = $data['mensaje'] = 'Falta la clave de activación';

		if (empty($errores)){
			$date 		= new DateTime();
			$nauthkey = md5($date->getTimestamp().rand());
			$sql = "UPDATE $wsusuarios SET authkey='$nauthkey' WHERE correo='$correo' AND authkey='$authkey' RETURNING id";
			$resultado = DbLink::resultados($sql,acceso::getBD());
			if($resultado !==false){
				$data['estatus'] = '1';
				$data['mensaje'] = 'Se ha actualizado la clave de activación';
			}else{
				$data['estatus'] = '0';
				$errores['correo'] = $data['mensaje'] = 'Correo o clave de activación incorrecta';
			}
		}
		if (!empty($errores)){
			$data['estatus'] = '0';
			$data['error'] = $errores;
		}
		if(strpos("$_SERVER[REQUEST_URI]", __CLASS__."/".__FUNCTION__) !== false)return entregar($data); else return $data;
	}

	/**
	 * activByKey
	 * Activa un usuario con su clave de activación. En caso de lograr la activación, se efectua un cambio de clave de activación.
	 * @param correo string la dirección de correo
	 * @param authkey string la clave de activacion 
	 * @return array con datos de usuario si el usuario queda activado, false si no
	 */
	public static function activByKey($correo=null, $authkey=null){
		global $conf;
		$errores    = array();
		$data       = array();
		$data['estatus'] = '0';
		$wsusuarios = acceso::getBD("Tb")[0];
		if(empty($correo)) $correo	= traerValorDe("correo"); 
		if(empty($correo)) $errores['correo'] = $data['mensaje'] = 'Falta la dirección de correo';
		if(empty($authkey)) $authkey	= traerValorDe("authkey"); 
		if(empty($authkey)) $errores['authkey'] = $data['mensaje'] = 'Falta la clave de activación';

		if (empty($errores)){
			$sql = "UPDATE $wsusuarios SET activo='t' WHERE correo='$correo' AND authkey='$authkey' RETURNING id";
			$resultado = DbLink::resultados($sql,acceso::getBD());
			if($resultado !==false){
				$data = acceso::setSesion($correo);
				$data = acceso::ChgUsrKey($correo, $authkey);
				
				$data['estatus'] = '1';
				$data['mensaje'] = "Se ha activado al usuario $correo";
				if($conf['servicio']['correo']) $data = acceso::sendBienvenida($correo);
				feedback::data("El usuario $correo acaba de activar su cuenta");
			}else{
				$data['estatus'] = '0';
				$errores['correo'] = $data['mensaje'] = 'Correo o clave de activación incorrecta';
			}
		}
		if (!empty($errores)){
			$data['estatus'] = '0';
			$data['error'] = $errores;
		}
		if(strpos("$_SERVER[REQUEST_URI]", __CLASS__."/".__FUNCTION__) !== false)return entregar($data); else return $data;
	}



	/**
	 * delAcceso
	 * elimina permisos de ejecución de un grupo sobre un método
	 * @param string $metodo el nombre del metodo 
	 * @param string $grupo el nombre del grupo
	 * @return mixed boolean true si el usuario tiene acceso | json con información de acceso no permitido
	 */
	public static function delAcceso($metodo=null, $grupo=null){
		$errores    = array();
		$data       = array();
		$data['estatus'] = '0';
		$wscontrol = acceso::getBD("Tb")[4];

		if(empty($metodo)) $metodo	= traerValorDe("metodo"); 
		if(empty($metodo)) $errores['metodo'] = $data['mensaje'] = 'Falta el nombre del método';
		if(empty($grupo)) $grupo	= traerValorDe("grupo"); 
		if(empty($grupo)) $errores['grupo'] = $data['mensaje'] = 'Falta el nombre del grupo';

		if (empty($errores)){
			if(is_numeric($metodo)) $idMetodo = $metodo; else $idMetodo = acceso::getMetodo($metodo);
			if(!$idMetodo) $errores['metodo'] = $data['mensaje'] = 'El método no existe';

			if(is_numeric($grupo)) $idGrupo = $grupo; else $idGrupo = acceso::getGrupo($grupo);
			if(!$idGrupo) $errores['grupo'] = $data['mensaje'] = 'El grupo no existe';
		}

		if (empty($errores)){
			$sql = "SELECT grupo_id FROM $wscontrol WHERE metodo_id = $idMetodo AND grupo_id = $idGrupo";
			$resultado = DbLink::resultados($sql,acceso::getBD());
			if($resultado ==false) $errores['DELETE'] = $data['mensaje'] = 'La relación no existe';
		}

		if (empty($errores)){
			$sql = "DELETE FROM $wscontrol WHERE metodo_id = $idMetodo AND grupo_id = $idGrupo RETURNING grupo_id;";
			$resultado = DbLink::resultados($sql,acceso::getBD());
			if($resultado !==false){
				$data['estatus'] = '1';
				$data['mensaje'] = "Se ha eliminado el permiso de ejecución de $metodo para el grupo $grupo";
			}else{
				$data['estatus'] = '0';
				$errores['INSERT'] = $data['mensaje'] = 'No se pudo eliminar la relación';
				$data['error'] = $errores;
			}
		}else $data['error'] = $errores;
		if(strpos("$_SERVER[REQUEST_URI]", __CLASS__."/".__FUNCTION__) !== false)return entregar($data); else return $data;
	}

	/**
	 * setAcceso
	 * Define permisos de ejecución de un método
	 * @param string $metodo el nombre del metodo 
	 * @param string $grupo el nombre del grupo
	 * @return mixed boolean true si el usuario tiene acceso | json con información de acceso no permitido
	 */
	public static function setAcceso($metodo=null, $grupo=null){
		$errores    = array();
		$data       = array();
		$data['estatus'] = '0';
		$wscontrol = acceso::getBD("Tb")[4];

		if($metodo ==null) $metodo	= traerValorDe("metodo"); if(!$metodo)$errores['metodo'] = $data['mensaje'] = 'Falta el nombre del método';
		if($grupo ==null) $grupo	= traerValorDe("grupo"); if(!$grupo)$errores['grupo'] = $data['mensaje'] = 'Falta el nombre del grupo';

		if (empty($errores)){
			if(is_numeric($metodo)) $idMetodo = $metodo; else $idMetodo = acceso::getMetodo($metodo);
			if(!$idMetodo) $errores['metodo'] = $data['mensaje'] = 'El método no existe';

			if(is_numeric($grupo)) $idGrupo = $grupo; else $idGrupo = acceso::getGrupo($grupo);
			if(!$idGrupo) $errores['grupo'] = $data['mensaje'] = 'El grupo no existe';
		}

		if (empty($errores)){
			$sql = "SELECT grupo_id FROM $wscontrol WHERE metodo_id = $idMetodo AND grupo_id = $idGrupo";
			$resultado = DbLink::resultados($sql,acceso::getBD());
			if($resultado !==false) $errores['INSERT'] = $data['mensaje'] = 'La relación ya existe';
		}

		if (empty($errores)){
			$sql = "INSERT INTO $wscontrol (metodo_id, grupo_id) VALUES ($idMetodo, $idGrupo) RETURNING grupo_id;";
			$resultado = DbLink::resultados($sql,acceso::getBD());
			if($resultado !==false){
				$data['estatus'] = '1';
				$data['mensaje'] = "Se ha creado el permiso de ejecución de $metodo para el grupo $grupo";
			}else{
				$data['estatus'] = '0';
				$errores['INSERT'] = $data['mensaje'] = 'No se pudo crear la relación';
				$data['error'] = $errores;
			}
		}else $data['error'] = $errores;
		if(strpos("$_SERVER[REQUEST_URI]", __CLASS__."/".__FUNCTION__) !== false)return entregar($data); else return $data;
	}

	/**
	 * activarUsr
	 * activa o desactiva un usuario
	 * @param $correo la dirección de correo
	 * @return array estatus de la acción
	 */
	public static function activarUsr($correo=null, $desact=null){
		$errores    = array();
		$data       = array();
		$data['estatus'] = '0';
		$tabla = acceso::getBD('Tb')[0];
		$correo 	= traerValorDe("correo"); if(!$correo)$errores['correo'] = $data['mensaje'] = 'Falta el correo';
		$desact 	= traerValorDe("desact"); 	
		if (empty($errores)){
			$activar = $desact? 'f':'t';
			$tactivo = $desact? 'desactivado':'activado';
			$sql = "UPDATE $tabla SET activo = '$activar' returning correo;";
			$resultado = DbLink::resultados($sql,acceso::getBD());
			if($resultado !==false){
				$data['estatus'] = '1';
				$data['mensaje'] = "Se ha $tactivo el usuario $correo";
			}
		}else $data['error']  = $errores;
		if(strpos("$_SERVER[REQUEST_URI]", __CLASS__."/".__FUNCTION__) !== false)return entregar($data); else return $data;
	}

	/**
	 * bloquearUsr
	 * bloquea o desbloquea un usuario
	 * @param $correo la dirección de correo
	 * @return array estatus de la acción
	 */
	public static function bloquearUsr($correo=null, $desblo=null){
		$errores    = array();
		$data       = array();
		$data['estatus'] = '0';
		$correo 	= traerValorDe("correo"); 	if(!$correo) 	$errores['correo'] 	= $data['mensaje'] = 'Falta el correo';
		$desblo 	= traerValorDe("desblo"); 	
		if (empty($errores)){
			$tabla = acceso::getBD('Tb')[0];
			$bloquear = $desblo? 'f':'t';
			$tbloquea = $desblo? 'desbloqueado':'bloqueado';
			$sql = "UPDATE $tabla SET bloqueado = '$bloquear' returning correo;";
			$resultado = DbLink::resultados($sql,acceso::getBD());
			if($resultado !==false){
				$data['estatus'] = '1';
				$data['mensaje'] = "Se ha $tbloquea el usuario $correo";
			}
		}else $data['error']  = $errores;
		if(strpos("$_SERVER[REQUEST_URI]", __CLASS__."/".__FUNCTION__) !== false)return entregar($data); else return $data;
	}

	/**
	 * delUsr
	 * borra un usuario
	 * @param $correo la dirección de correo
	 * @return array estatus de la acción
	 */
	public static function delUsr($delUsr=null){
		$errores    = array();
		$data       = array();
		$data['estatus'] = '0';
		$tabla = self::getBD('Tb')[0];
		$TbUsrGrp = self::getBD('Tb')[1];
		if(empty($delUsr)) $delUsr = traerValorDe("delUsr");
		if(empty($delUsr)) $errores['correo'] = $data['mensaje'] = 'Falta el correo';

		if (empty($errores)){
			$resultado = DbLink::resultados("SELECT id FROM $tabla WHERE correo = '$delUsr';",self::getBD());
			if($resultado !==false) $idUsr = $resultado[0]['id'];
			else $errores['correo'] = $data['mensaje'] = 'No existe el correo';
		}

		if (empty($errores)){
			$resultado = DbLink::resultados("DELETE FROM $TbUsrGrp WHERE usuario_id = $idUsr;",self::getBD());
			$sql = "DELETE FROM $tabla WHERE correo = '$delUsr' returning correo;";
			$resultado = DbLink::resultados($sql,self::getBD());
			if($resultado !==false){
				$data['estatus'] = '1';
				$data['mensaje'] = "Se ha borrado el usuario $delUsr";
			}
			
		}else $data['error']  = $errores;
		if(strpos("$_SERVER[REQUEST_URI]", __CLASS__."/".__FUNCTION__) !== false) return entregar($data); else return $data;
	}

	/**
	 * setUsr
	 * Crea un usuario
	 * @param $correo string el correo a crear
	 * @param $clave string la clave del correo
	 * @return array estatus de la acción
	 */
	public static function setUsr($correo=null, $clave=null){
		global $conf;
		$actMail = $conf['servicio']['correo'];
		$errores    = array();
		$data       = array();
		$data['estatus'] = '0';
		$tabla 		= acceso::getBD('Tb')[0];
		$TbUsrGrp = acceso::getBD('Tb')[1];
		if(empty($correo)) $correo = traerValorDe("correo"); 	
		if(empty($correo)) $errores['correo'] = $data['mensaje'] = 'Falta el correo';
		if(empty($clave)) $clave = traerValorDe("clave"); 		
		if(empty($clave)) $errores['clave'] = $data['mensaje'] = 'Falta la clave';

		if(empty($errores)){
			$resultado = DbLink::resultados("SELECT correo FROM $tabla WHERE correo = '$correo'",acceso::getBD());
			$existe = isset($resultado[0]['correo'])? 1:0;
			if(isset($resultado[0]['correo'])) $errores['correo'] = $data['mensaje'] = "El usuario '$correo' ya existe";
		}

		if(empty($errores)){
			$lcorreo = strtolower($correo);
			$clavemd5 = MD5($clave.$lcorreo); 	
			$date 		= new DateTime();
			$authkey	= md5($date->getTimestamp().rand());
			$apikey = md5($correo) . md5($date->getTimestamp().rand());
			$apikey	= substr($apikey, 5, 40);
			$activo 	= $actMail?'f':'t';

			$sql = "INSERT into $tabla (correo, clave, authkey, activo, bloqueado, apikey) 
							VALUES ('$lcorreo', '$clavemd5','$authkey', '$activo','f','$apikey') RETURNING id ";
			$result = DbLink::resultados($sql,acceso::getBD());
			if(isset($result[0]['id'])){
				$data['estatus'] = '1';
				$data['id'] = $result[0]['id'];
				$data['mensaje'] = "Se ha creado el usuario $correo";
			}
			$sql = "INSERT into $TbUsrGrp (usuario_id, grupo_id) 
							VALUES ({$data['id']}, 2),({$data['id']}, 3) RETURNING usuario_id ";
			$result = DbLink::resultados($sql,acceso::getBD());
		}else $data['error']  = $errores;

		if(empty($errores)){
			if($actMail) $data['porCorreo'] = acceso::activByMail($correo);
		}

		if(strpos("$_SERVER[REQUEST_URI]", __CLASS__."/".__FUNCTION__) !== false)return entregar($data); else return $data;
	}

	/**
	 * login
	 * inicia sesion de un usuario
	 * @param $correo string el correo a crear
	 * @param $clave string la clave del correo
	 * @return array estatus de la acción
	 */
	public static function login($correo=null, $clave=null){
		$errores    = array();
		$data       = array();
		$data['estatus'] = '0';
		$tabla = acceso::getBD('Tb')[0];

		if(empty($correo)) $correo	= traerValorDe("correo"); 
		if(empty($correo)) $errores['correo'] = $data['mensaje'] = 'Falta la dirección de correo';

		if(empty($clave)) $clave	= traerValorDe("clave"); 
		if(empty($clave)) $errores['clave'] = $data['mensaje'] = 'Falta la clave';

		if(empty($errores)){
			$lcorreo = strtolower($correo);
			$clavemd5 = MD5($clave.$lcorreo);
			$sql = "SELECT id, activo, bloqueado FROM $tabla WHERE correo = '$correo' AND clave ='$clavemd5'";
			$resultado = DbLink::resultados($sql,acceso::getBD());
			if(!empty($resultado[0]['id'])){
				$data = $resultado[0]; 
				if(empty($data['bloqueado']) ||$data['bloqueado'] == 't') $errores['bloqueado'] = $data['mensaje'] = 'Usuario bloqueado';
				if(empty($data['activo']) || $data['activo'] == 'f') $errores['inactivo'] = $data['mensaje'] = 'Usuario inactivo';
			}else{
				$errores['datos'] = $data['mensaje'] = 'Correo o clave incorrecta';
				$data['error'] = $errores;
			} 
		}else $data['error'] = $errores;

		if(empty($errores))	$data = acceso::setSesion($lcorreo);
		else $data['error'] = $errores;
		
		if(strpos("$_SERVER[REQUEST_URI]", __CLASS__."/".__FUNCTION__) !== false)return entregar($data); else return $data;
	}

	/**
	 * getSesion
	 * identifica la sesión de usuario iniciada
	 * @return array estatus de la acción
	 */
	public static function getSesion(){
		$errores    = array();
		$data       = array();
		$data['estatus'] = '0';
		if(!isset($_SESSION['usuario']))  $errores['invitado'] = $data['mensaje'] = 'No hay usuarios logueados en este equipo';

		if(empty($errores)){
			$data = $_SESSION['usuario'];
			$data['estatus'] = '1';
		}else $data['error'] = $errores;
		
		if(strpos("$_SERVER[REQUEST_URI]", __CLASS__."/".__FUNCTION__) !== false)return entregar($data); else return $data;
	}

	/**
	 * getUsrGrupos
	 * Retorna la lista de grupos de un usuario
	 * @return array estatus de la acción
	 */
	public static function getUsrGrupos($idUsuario){
		$data = array();
		$TbUsrGrp = acceso::getBD('Tb')[1];
		$sql = "SELECT grupo_id FROM $TbUsrGrp WHERE usuario_id = $idUsuario";
		$resultado = DbLink::resultados($sql,acceso::getBD());
		$lista = array();
		if($resultado !==false){
			foreach ($resultado as $elem) $lista[] = $elem['grupo_id'];
		}
		return $lista;
	}

	/**
	 * logout
	 * borra la sesion actual e inicia una nueva
	 * @return array estatus de la acción
	 */
	public static function logout(){
		session_unset();
		session_destroy();
		$data = acceso::getSesion();
		
		if(strpos("$_SERVER[REQUEST_URI]", __CLASS__."/".__FUNCTION__) !== false)return entregar($data); else return $data;
	}

	/**
	 * control
	 * Controla el metodo en uso, permitiendo su ejecución o bloqueandolo
	 * @param string $metodo el metodo que se desea ejecutar
	 * @param string $usuario el usuario registrado, de haber uno
	 * @return mixed boolean true si el usuario tiene acceso | json con información de acceso no permitido
	 */
	public static function control($metodo, $usuario){
		$grupos = array();
		if(CapturarIp() == "127.0.0.1") return true;
		$wsmetodo = acceso::getBD("Tb")[2];
		$wscontrol = acceso::getBD("Tb")[4];

		$apikey = traerValorDe("apikey");
		if($apikey){
			$logueo = acceso::setSecAk($apikey);
			if($logueo['estatus'] == '0') return false;
			else return true;
		}


		//si un usuario está logueado, toma sus grupos, si no, el grupo es 'visitantes'
		if(empty($_SESSION['usuario']['grupos'])) $grupos[] = 3;
		else $grupos = $_SESSION['usuario']['grupos'];

		//se revisa la tabla ws_metodos, si el metodo no está registrado se registra, se toma su id
		$metodoID = acceso::getMetodo($metodo);
		if(!$metodoID) $metodoID = acceso::setMetodo($metodo);
		if(!$metodoID) return false;

		//si alguno de los grupos tiene permiso de ejecución sobre $metodo, retorna true
		$arrCompara = array();
		foreach ($grupos as $grupo) $arrCompara[] = "grupo_id = $grupo";

		$listaOR = implode(' OR ', $arrCompara);
		$sql = "SELECT metodo_id FROM $wscontrol WHERE metodo_id = $metodoID AND ($listaOR)";

		$resultado = DbLink::resultados($sql,acceso::getBD());
		if($resultado !==false) return true;
		else return false;

		//si el grupo del usuario no tiene permisos sobre el metodo retorna un json de acceso no autorizado
	}

	/**
	 * getGrupo
	 * Retorna el ID de un grupo a partir de su nombre
	 * @param string $grupo nombre del grupo
	 * @return int ID del grupo
	 */
	public static function getGrupo($nombre){
		$wsgrupos = acceso::getBD("Tb")[3];
		$sql = "SELECT id FROM $wsgrupos WHERE nombre = '$nombre'";
		$resultado = DbLink::resultados($sql,acceso::getBD());
		if($resultado !==false) return $resultado[0]['id'];
		else return false;
	}

	/**
	 * getMetodo
	 * retorna el identificador del método
	 * @param string $metodo el metodo
	 * @return mixed boolean true si el usuario tiene acceso | json con información de acceso no permitido
	 */
	public static function getMetodo($metodo){
		$wsmetodo = acceso::getBD("Tb")[2];
		$sql = "SELECT id FROM $wsmetodo WHERE nombre = '$metodo'";
		$resultado = DbLink::resultados($sql,acceso::getBD());
		if($resultado !==false) return $resultado[0]['id'];
		else return false;
	}

	/**
	 * setMetodo
	 * crea un nuevo método en la tabla ws_metodos, le asigna el grupo admin para su ejecución y retorna su id
	 * @param string $metodo el metodo 
	 * @return mixed boolean true si el usuario tiene acceso | json con información de acceso no permitido
	 */
	public static function setMetodo($metodo){
		$wsmetodo = acceso::getBD("Tb")[2];
		$wscontrol = acceso::getBD("Tb")[4];

		$sql = "INSERT INTO $wsmetodo (nombre) VALUES ('$metodo') RETURNING id;";
		$resultado = DbLink::resultados($sql,acceso::getBD());
		if($resultado !==false) $idMetodo = $resultado[0]['id'];
		else return false;

		//TODO crear acceso por defecto
		$clase = explode('::', $metodo)[0];
		$metodo = explode('::', $metodo)[1];
		if(existeModelo($clase)){
			require_once $_SERVER["DOCUMENT_ROOT"] ."/servicios/$clase/$clase.php";
			if(method_exists($clase, $metodo)){			
				if(method_exists($clase, "accesoDef")){
					$grupoID = $clase::accesoDef($metodo);
					$setAcceso = acceso::setAcceso($idMetodo, $grupoID);
				}
				$setAcceso = acceso::setAcceso($idMetodo, 1);
				if($setAcceso['estatus']=='1') return true;
			}else return false;
		}else return false;
	}

	/**
	 * resetPermisos
	 * coloca todos los permisos en su estado inicial
	 * @return array 
	 */
	public static function resetPermisos(){
		$data = array();
		$data = self::delPermisos();
		$data = self::setPermisos();
		if(strpos("$_SERVER[REQUEST_URI]", __CLASS__."/".__FUNCTION__) !== false)return entregar($data); else return $data;
	}	

	/**
	 * delPermisos
	 * Elimina de BD todos los permisos creados
	 * @return array 
	 */
	public static function delPermisos(){
		$data = array();
		require_once $_SERVER["DOCUMENT_ROOT"]."/servicios/admin/admin.php";
		$clases = admin::metodos();
		foreach ($clases as $clase => $metodos) {
			foreach ($metodos as $key => $metodo) {
				if(existeModelo($clase)){
					require_once $_SERVER["DOCUMENT_ROOT"] ."/servicios/$clase/$clase.php";
					if(method_exists($clase, $metodo)){		
						$idmetodo = acceso::getMetodo("$clase::$metodo");
						if(!empty($idmetodo)){
							$data["$clase::$metodo::2"] = acceso::delAcceso($idmetodo,2);
							$data["$clase::$metodo::3"] = acceso::delAcceso($idmetodo,3);
						}
//echo "$metodo $idmetodo"; die();
					}
				}
			}
		}

		if(strpos("$_SERVER[REQUEST_URI]", __CLASS__."/".__FUNCTION__) !== false)return entregar($data); else return $data;
	}	

	/**
	 * setPermisos
	 * Registra en BD todos los permisos por defecto
	 * @return array 
	 */
	public static function setPermisos(){
		$data = array();
		require_once $_SERVER["DOCUMENT_ROOT"]."/servicios/admin/admin.php";
		$clases = admin::metodos();
		foreach ($clases as $clase => $metodos) {
			foreach ($metodos as $key => $metodo) {
				if(existeModelo($clase)){
					require_once $_SERVER["DOCUMENT_ROOT"] ."/servicios/$clase/$clase.php";
					if(method_exists($clase, $metodo)){		
						$idmetodo = acceso::getMetodo("$clase::$metodo");
						if(empty($idmetodo)) $idmetodo = acceso::setMetodo("$clase::$metodo");
//echo "$metodo $idmetodo"; die();
						if(method_exists($clase, "accesoDef")){
							$grupoID = $clase::accesoDef($metodo);
							$setAcceso = acceso::setAcceso($idmetodo, $grupoID);
							
							$data[$clase][$metodo] = $grupoID;
						}
						$setAcceso = acceso::setAcceso($idmetodo, 1);
					}
				}
			}
		}

		if(strpos("$_SERVER[REQUEST_URI]", __CLASS__."/".__FUNCTION__) !== false)return entregar($data); else return $data;
	}	

	/**
	 * rec
	 * Registra información de acceso
	 * @return array con datos de registro
	 */
	public static function rec(){
		$data = array(
			'fecha' 			=> date('Y-m-d H:i:s'),
			'ip'					=> CapturarIp(),
			'url' 				=> $_SERVER['REQUEST_URI'],
			'usuario_id' 	=> empty($_SESSION['usuario']['id'])?null:$_SESSION['usuario']['id'],
			'sesion_id'  => session_id(),
			'tabla' 			=> self::getBD("Tb")[5]
			);

		$data['set'] = loadDataTable(self::getBD(), self::getBD("Tb")[5], $data);
		if(strpos("$_SERVER[REQUEST_URI]", __CLASS__."/".__FUNCTION__) !== false)return entregar($data); else return $data;
	}	

// fin de la clase acceso
}