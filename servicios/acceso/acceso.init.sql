
CREATE TABLE ws_usuarios
(
  id serial,
  correo character varying(100) UNIQUE,
  clave character varying(32),
  authkey character varying(32), 
  activo boolean,
  bloqueado boolean,
  apikey character varying(40),
  CONSTRAINT ws_usuarios_id_pkey PRIMARY KEY (id),
  CONSTRAINT ws_usuarios_correo_key UNIQUE (correo)
)
WITH (OIDS=FALSE);
ALTER TABLE ws_usuarios OWNER TO postgres;
GRANT ALL ON TABLE ws_usuarios TO postgres;
GRANT ALL ON TABLE ws_usuarios TO data;

--ALTER TABLE ws_usuarios ADD COLUMN apikey character varying(16);

CREATE TABLE ws_metodos
(
  id serial NOT NULL,
  nombre character varying(25) NOT NULL,
  CONSTRAINT ws_metodos_id_pkey PRIMARY KEY (id),
  CONSTRAINT ws_metodos_nombre_key UNIQUE (nombre)
)
WITH (OIDS=FALSE);
ALTER TABLE ws_metodos OWNER TO postgres;
GRANT ALL ON TABLE ws_metodos TO postgres;
GRANT ALL ON TABLE ws_metodos TO data;

CREATE TABLE ws_grupos
(
  id serial NOT NULL,
  nombre character varying(25) NOT NULL,
  CONSTRAINT ws_grupos_id_pkey PRIMARY KEY (id),
  CONSTRAINT ws_grupos_nombre_key UNIQUE (nombre)
)
WITH (OIDS=FALSE);
ALTER TABLE ws_grupos OWNER TO postgres;
GRANT ALL ON TABLE ws_grupos TO postgres;
GRANT ALL ON TABLE ws_grupos TO data;

INSERT INTO ws_grupos (nombre) VALUES ('admin'), ('usuarios'), ('visitantes');

CREATE TABLE ws_usuario_grupo
(
  usuario_id integer NOT NULL,
  grupo_id integer NOT NULL,
  CONSTRAINT ws_usuario_grupo_pkey PRIMARY KEY (usuario_id, grupo_id),
  CONSTRAINT ws_grupo_usuario_id_fkey FOREIGN KEY (usuario_id)
      REFERENCES ws_usuarios (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT ws_usuario_grupo_id_fkey FOREIGN KEY (grupo_id)
      REFERENCES ws_grupos (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (OIDS=FALSE);
ALTER TABLE ws_usuario_grupo OWNER TO postgres;
GRANT ALL ON TABLE ws_usuario_grupo TO postgres;
GRANT ALL ON TABLE ws_usuario_grupo TO data;

CREATE TABLE ws_control
(
  metodo_id integer NOT NULL,
  grupo_id integer NOT NULL,
  CONSTRAINT ws_control_id_pkey PRIMARY KEY (metodo_id, grupo_id),
  CONSTRAINT ws_control_metodo_id_fkey FOREIGN KEY (metodo_id)
      REFERENCES ws_metodos (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT ws_control_grupo_id_fkey FOREIGN KEY (grupo_id)
      REFERENCES ws_grupos (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (OIDS=FALSE);
ALTER TABLE ws_control OWNER TO postgres;
GRANT ALL ON TABLE ws_control TO postgres;
GRANT ALL ON TABLE ws_control TO data;


CREATE TABLE ws_rec
(
  id serial not null,
  fecha timestamp not null,
  ip character varying(16),
  url character varying(512), 
  usuario_id bigint,
  session_id bigint,
  CONSTRAINT ws_rec_pkey PRIMARY KEY (id)
)
WITH (OIDS=FALSE);
ALTER TABLE ws_rec OWNER TO postgres;
GRANT ALL ON TABLE ws_rec TO postgres;
GRANT ALL ON TABLE ws_rec TO data;