
CREATE TABLE ws_crontab
(
	nombre    character varying(250) not null,
  command   character varying(250) not null,
  schedule  character varying(250) not null,
  output    character varying(250) not null,
  enabled   boolean not null,
  CONSTRAINT ws_crontab_pkey PRIMARY KEY (nombre)
)
WITH (OIDS=FALSE);
ALTER TABLE ws_crontab OWNER TO postgres;
GRANT ALL ON TABLE ws_crontab TO postgres;
GRANT ALL ON TABLE ws_crontab TO data;
