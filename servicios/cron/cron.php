<?php 
require_once $_SERVER["DOCUMENT_ROOT"] ."/servicios/admin/admin.php";
class cron {

	/**
	 * accesoDef
	 * valores por defecto de acceso a cada método.
	 * @param string $metodo el nombre del método a validar 
	 * @return int nivel  de acceso
	 */
	public static function accesoDef($metodo){
		switch ($metodo) { default: return 1;}
	}

	public static function listar(){
		$data = array();
		$servicios = admin::servicios();		
		foreach($servicios as $servicio){
			$fclase = $_SERVER["DOCUMENT_ROOT"]."/servicios/$servicio/$servicio.php";
			if(is_file($fclase)){
				require_once $fclase;
				if(method_exists($servicio, "getCron")){
					$tareas = $servicio::getCron();
					foreach($tareas as $job){
				    $job = array_filter($job);
				    $data['tareas'][] = $job;
					}
				}
			}
		}
		$data['estatus'] = '1';
		if(strpos("$_SERVER[REQUEST_URI]", __FUNCTION__) !== false)return entregar($data); else return $data;
	}

	public static function run(){
		global $conf;
		$data = array();
		if(!$conf['servicio']['cron']){
			$data['estatus'] = '0';
			$data['mensaje'] = 'El servicio cron está apagado';
			$data['tareas'] = array();
		}else{
			$jobby = new Jobby\Jobby();
			$tareas = self::listar()['tareas'];
			foreach($tareas as $job){
		    $job = array_filter($job);
		    $data['tareas'][] = $job;
		    $jobName = $job['nombre'];
		    unset($job['nombre']);
		    if($job['enabled']) $jobby->add($jobName, $job);
			}
			$jobby->run();
			$data['estatus'] = '1';
		}

		if(strpos("$_SERVER[REQUEST_URI]", __FUNCTION__) !== false)return entregar($data); else return $data;
	}

// fin clase cron
}


						 		
