<?php 

/*
requires root privileges
sudo apt-get install libcap2-bin
sudo chgrp adm /usr/bin/nmap
sudo chmod 750 /usr/bin/nmap
sudo setcap cap_net_raw,cap_net_admin,cap_net_bind_service+eip /usr/bin/nmap
export NMAP_PRIVILEGED=""


*/

use Nmap\Host;
use Nmap\Nmap;
use Nmap\Port;

class rastrear {

	/**
	 * accesoDef
	 * valores por defecto de acceso a cada método.
	 * @param string $metodo el nombre del método a validar 
	 * @return int nivel  de acceso
	 */
	public static function accesoDef($metodo){
		switch ($metodo) {
		    case "getBD": 			return 1; break;
		    case "total": 			return 1; break;
		    case "analyze": 		return 1; break;
		    case "accesoDef": 	return 1; break;
		    case "getCron": 		return 1; break;
		    case "test": 				return 1; break;
		    default: 						return 1; 
		}
	}

	/**
	 * getBD
	 * retorna la base de datos en uso.
	 * @return string con nombre de la bd
	 */
	public static function getBD($dar='BD'){
			if($dar=='BD') return "BD";
			if($dar=='Tb') return array('ws_hosts','ws_hosts_hist');
	}

	public static function test(){
		$data = Nmap::create()->scan([ '127.0.0.1' ]);
		$salida = ".";
//		$salida = shell_exec('arp -a -n');
		echo $salida;die();
		if(strpos("$_SERVER[REQUEST_URI]", __CLASS__."/".__FUNCTION__) !== false) return entregar($data); else return $data;
	}

	/**
	 * getCron
	 * Tareas a ejecutar por crontab
	 * @return array tareas a ejecutar
	 */
	public static function getCron(){ 
		$serializer = new SuperClosure\Serializer();
		$fn = function(){self::setRNCNext();};
		$wbase = "http://" . $_SERVER['SERVER_NAME'];
		$fn = "wget -O /dev/null $wbase/rastrear/get/192.168.1.*/";
		$tareas = array();
		$tarea1 = array(
			'nombre'	=>'scan_local',
			'command'	=> $fn,
			'schedule'=>'*/3 * * * *',
			'output'	=>'',  
			'enabled'	=> false,
		);
		$tareas[] = $tarea1;
		return $tareas;
	}

		/**
		 * total
		 * Total registros relacionados
		 * @return array de datos de persona
		 */
		public static function total(){
			$data = array();
			foreach (rnc::getBD('Tb') as $tabla) $data[$tabla] = totalRegBDTable($tabla, rnc::getBD());
			if(strpos("$_SERVER[REQUEST_URI]", __CLASS__."/".__FUNCTION__) !== false) return entregar($data); else return $data;
		}

		/**
		 * analyze
		 * Actualiza las estadísticas de cada tabla
		 * @return array de datos de persona
		 */
		public static function analyze(){
			$data = array();
			foreach (rnc::getBD('Tb') as $tabla) $data[$tabla] = analyzeBDTable($tabla, rnc::getBD());
			if(strpos("$_SERVER[REQUEST_URI]", __FUNCTION__) !== false)return entregar($data); else return $data;
		}


	/**
	 * get
	 * Obtiene información de uno o varios equipos.
	 * @param string $get ip/dominio de uno o varios equipos
	 * @param string $OsDetection 
	 * @param string $ServiceInfo 
	 * @param string $Verbose
	 * @param string $PortScan
	 * @param string $ReverseDNS
	 * @return array información de uno o varios equipos
	 */
	public static function get($get=null, $set=false, $OsDetection=false, $ServiceInfo=false, $Verbose=false, $DisablePortScan=false, $DisableReverseDNS=false){
		$errores    = array();
		$data       = array();
		$segundos = rand(0, 10);
		$fecha = new DateTime();

		if(empty($get)) 							$get 							= traerValorDe("get");
		if(empty($get)) 							$errores['host'] 	= $data['mensaje'] ='falta el host';
		if(empty($set)) 							$set 							= traerValorDe("set");
		if(empty($OsDetection)) 			$OsDetection 			= traerValorDe("OsDetection");
		if(empty($ServiceInfo)) 			$ServiceInfo 			= traerValorDe("ServiceInfo");
		if(empty($Verbose)) 					$Verbose 					= traerValorDe("Verbose");
		if(empty($DisablePortScan)) 	$DisablePortScan 	= traerValorDe("DisablePortScan");
		if(empty($DisableReverseDNS)) $DisableReverseDNS= traerValorDe("DisableReverseDNS");
		$host = explode(",", $get);

		if(empty($errores)){
			$nmap = new Nmap();
			if($OsDetection) $nmap->enableOsDetection();
			if($ServiceInfo) $nmap->enableServiceInfo();
			if($Verbose) $nmap->enableVerbose();
			if($DisablePortScan) $nmap->disablePortScan();
			if($DisableReverseDNS) $nmap->disableReverseDNS();
			$nmap->scan($host);

			$xml = file_get_contents('/tmp/output.xml');
//			$data = simplexml_load_string($xml);
			$json = json_encode(simplexml_load_string($xml));
			$data = json_decode($json, TRUE);


			if(!empty($data['host'])){
				if(!empty($data['host']['status'])){
					$dato = [
						"ip"=> $data['host']['address']['@attributes']['addr'],
						"data"=> json_encode($data['host']),
						"fecha_modificacion"=> date("Y-m-d H:i:s"),
						"up"=> ($data['host']['status']['@attributes']['state']=="up")?'t':'f'
					];
					$data['set']['host'] = bd::loadData($dato, self::getBD('Tb')[0], null, self::getBD());
					$data['set']['hist'] = bd::loadData($dato, self::getBD('Tb')[1], null, self::getBD());
				}else{
					foreach ($data['host'] as $key => $value) {
						$dato = [
							"ip"=> $value['address']['@attributes']['addr'],
							"data"=> json_encode($value),
							"fecha_modificacion"=> date("Y-m-d H:i:s"),
							"up"=> ($value['status']['@attributes']['state']=="up")?'t':'f'
						];
						$data['set']['host'] = bd::loadData($dato, self::getBD('Tb')[0], null, self::getBD());
						$data['set']['hist'] = bd::loadData($dato, self::getBD('Tb')[1], null, self::getBD());
					}
				}
			}

			$data['estatus'] = '1';
		}

		if(!empty($errores)){
			$data['estatus'] 		= '0';
			$data['error']  = $errores;
		}

		if(strpos("$_SERVER[REQUEST_URI]", __CLASS__."/".__FUNCTION__) !== false) return entregar($data); else return $data;
	}


// fin clase INCES
}
