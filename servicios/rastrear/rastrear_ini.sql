
CREATE TABLE ws_hosts
(
  ip                  character varying(15) NOT NULL,
  data                jsonb NOT NULL,
  fecha_modificacion  timestamp without time zone,
  CONSTRAINT ws_hosts_ip_pkey PRIMARY KEY (ip)
)
WITH (OIDS=FALSE);
ALTER TABLE ws_hosts OWNER TO postgres;
GRANT ALL ON TABLE ws_hosts TO postgres;
GRANT ALL ON TABLE ws_hosts TO data;
COMMENT ON TABLE ws_hosts IS 'Información de IPs que se han escaneado';

CREATE TABLE ws_hosts_hist
(
  id                  serial NOT NULL,
  ip                  character varying(15) NOT NULL,
  up                  boolean NOT NULL,
  fecha_modificacion  timestamp without time zone,
  CONSTRAINT ws_hosts_hist_id_pkey PRIMARY KEY (id)
)
WITH (OIDS=FALSE);
ALTER TABLE ws_hosts_hist OWNER TO postgres;
GRANT ALL ON TABLE ws_hosts_hist TO postgres;
GRANT ALL ON TABLE ws_hosts_hist TO data;
COMMENT ON TABLE ws_hosts_hist IS 'Histórico de conección de servidores';