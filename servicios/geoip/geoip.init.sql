
CREATE TABLE geoip_location
(
  locId    	bigint NOT NULL,
  pais   		character varying(50),
  region   	character varying(50),
  ciudad   	character varying(50),
  cp   			character varying(10),
  lat 			character varying(50) NOT NULL,
  lon 			character varying(50) NOT NULL,
  dmaCod 		bigint,
  areaCod 	bigint,
  CONSTRAINT geoip_location_pkey PRIMARY KEY (locId)
)
WITH (OIDS=FALSE);
ALTER TABLE geoip_location OWNER TO postgres;
GRANT ALL ON TABLE geoip_location TO postgres;
GRANT ALL ON TABLE geoip_location TO data;

CREATE TABLE geoip_bloques
(
  ipini bigint NOT NULL,
  ipfin bigint NOT NULL,
  locId bigint NOT NULL,
  CONSTRAINT geoip_bloques_pkey PRIMARY KEY (ipini, ipfin, locId)
)
WITH (OIDS=FALSE);
ALTER TABLE geoip_bloques OWNER TO postgres;
GRANT ALL ON TABLE geoip_bloques TO postgres;
GRANT ALL ON TABLE geoip_bloques TO data;