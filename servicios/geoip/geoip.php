<?php 
	require_once $_SERVER["DOCUMENT_ROOT"] . '/servicios/google/google.php';

class geoip {

	/**
	 * accesoDef
	 * valores por defecto de acceso a cada método.
	 * @param string $metodo el nombre del método a validar 
	 * @return int nivel  de acceso
	 */
	public static function accesoDef($metodo){
		switch ($metodo) {
		    case "getBD": 			return 1; break;
		    case "total": 			return 1; break;
		    case "analyze": 		return 1; break;
		    case "accesoDef": 	return 1; break;
		    case "buscar": 			return 1; break;
		    case "test": 				return 1; break;
		    default: 						return 1; 
		}
	}



	/**
	 * ipToInt
	 * transforma un ip en un entero 
	 * @param $ip string una representacion de un ip
	 * @return un entero usable en la bd geoip_bloques
	 */
	public static function ipToInt($ipToInt=null){
		if(empty($ipToInt)) $ipToInt = valorde("ipToInt"); if(empty($ipToInt)) $data = false;
		list($a,$b,$c,$d) = explode(".", $ipToInt,4);
		$data = ((16777216 * $a)+(65536 * $b)+(256 * $c)+$d);

		if(strpos("$_SERVER[REQUEST_URI]", __CLASS__."/".__FUNCTION__) !== false) echo $data; else return $data;
	}

	/**
	 * intToIp
	 * transforma un entero en un ip 
	 * @param $int string una representacion de un entero
	 * @return string una representacion de una direccion ip
	 */
	public static function intToIp($intToIp=null){
		if(empty($intToIp)) $intToIp = valorde("intToIp"); if(empty($intToIp)) $data = false;
		$a = intval($intToIp / 16777216) % 256;
		$b = intval($intToIp / 65536) % 256;
		$c = intval($intToIp / 256) % 256;
		$d = intval($intToIp) % 256;		
		$data = "{$a}.{$b}.{$c}.{$d}";
		if(strpos("$_SERVER[REQUEST_URI]", __CLASS__."/".__FUNCTION__) !== false) echo $data; else return $data;
	}

// fin clase geoip
}

