<?php 

class geo {

//https://www.google.co.ve/maps/dir/'10.505153238773,-66.897473931313'/10.4929975,-66.9210023/
	/**
	 * accesoDef
	 * valores por defecto de acceso a cada método.
	 * @param string $metodo el nombre del método a validar 
	 * @return int nivel  de acceso
	 */
	public static function accesoDef($metodo){
		switch ($metodo) {
		    case "getBD": 			return 1; break;
		    case "total": 			return 1; break;
		    case "analyze": 		return 1; break;
		    case "accesoDef": 	return 1; break;
		    case "test": 				return 1; break;
		    default: 						return 2; 
		}
	}

	/**
	 * getBD
	 * retorna la base de datos en uso o su lista de tablas.
	 * @return string con nombre de la bd
	 */
	public static function getBD($dar='BD'){
			if($dar=='BD') return "BD";
			if($dar=='Tb') return array(
				'geo_ubigeo', 		
				'geo_regiones',
				'geo_entidades',
				'geo_lugar',
				'geo_clima',				// estaciones climatolǵicas del mundo
				'geo_tipo',
				'geo_tipo_lugar'
			);
	}

	/**
	 * cercaDe
	 * Muestra una lista de lugares cercanos a la ubicación proporcionada
	 * @param $cercaDe string coordenada geográfica, geohash o ubigeo
	 * @param $distancia int kilómetros de distancia donde buscar
	 * @return array lista de ubicaciones
	 */
	public static function cercaDe($cercaDe=null, $lugar=null, $distancia=null, $limit=null){
		$data = array();
		$errores = array();
		$tregion = geo::getBD('Tb')[1];
		if(empty($cercaDe)) $cercaDe = traerValorDe("cercaDe");
		if(empty($cercaDe)) $errores['lugar'] = $data['mensaje'] = 'Falta la ubicación';
		if(empty($lugar)) $lugar = traerValorDe("lugar");
		if(empty($distancia)) $distancia = traerValorDe("distancia");
		if(empty($limit)) $limit = traerValorDe("limit");

		if(empty($errores)){
			if((strlen($cercaDe)==6||strlen($cercaDe)==4||strlen($cercaDe)==2) && preg_match("/^[0-9]+$/", $cercaDe)){
				$coord = self::entidad(substr($cercaDe, 0, 2));
				$coord = self::geohash($coord[0]['lat'].",".$coord[0]['lon']);
			}else{
				$coord = self::geohash($cercaDe);
			}
			if(empty($coord['lat'])) $errores['lugar'] = $data['mensaje'] = 'La ubicación es indescifrable';
		}
		if(empty($errores)){
			if(!empty($lugar)){
				$lugares = self::getTipo($lugar);
				if($lugares['estatus']=="1"){
					$whereitem = array();
					foreach ($lugares['tipos'] as $key => $value){
						$whereitem[] = "tl.id_tipo = '{$value['id']}'";
					} 
					$whereitems = implode(" OR ", $whereitem);
					$sql = "SELECT l.hash, l.nombre 
									FROM geo_lugar l, geo_tipo_lugar tl
									WHERE ($whereitems) AND tl.hash = l.hash";
					$lugares = DbLink::resultados($sql,self::getBD());
				}else{
					$errores['lugar'] = $data['mensaje'] = 'No existe un tipo de lugar como el que buscas';
				}
			}else{
				$sql = "SELECT l.hash, l.nombre FROM geo_lugar l";
				$lugares = DbLink::resultados($sql,self::getBD());
			}
			if($lugares ==false) $errores['lugar'] = $data['mensaje'] = 'No hay nada cercano';
		}
		$sitios = array();
		$sitio = array();
		if(empty($errores)){
			foreach ($lugares as $key => $value) {
				$sitio['nombre'] 		= $value['nombre'];
				$sitio['hash'] 			= $value['hash'];
				$sitio['relacion'] 	= geo::coordRel($coord['geohash'].",".$value['hash']);
				$sitio['km'] 				= $sitio['relacion']['km'];
				if(!empty($distancia)){
					if($sitio['relacion']['km']< $distancia){$sitios[] = $sitio;} 
				} 
				else $sitios[] = $sitio;
			}
			usort($sitios, function($a, $b){return $a['km'] - $b['km'];});
			if(!empty($limit)){
				if($limit < count($sitios))$sitios = array_slice($sitios, 0, $limit);
			}
			$data = $sitios;
			$data['estatus'] = '1';
		}

		if(!empty($errores)){
			$data['estatus'] = '0';
			$data['error'] = $errores;
		}

		if(strpos("$_SERVER[REQUEST_URI]", __CLASS__."/".__FUNCTION__) !== false)return entregar($data); else return $data;
	}

	/**
	 * destino
	 * dada una coordenada inicial, un anglo y una distancia, retorna la coordenada de destino
	 * @return array
	 */
	public static function destino($coordenada=null,$angulo=null,$distancia=null){
		$data 		= array();
		$errores 	= array();
		if(empty($coordenada)) $coordenada 	= traerValorDe("coordenada");
		if(empty($coordenada)) $errores['coordenada'] = $data['mensaje'] ='falta coordenada inicial';
		if(empty($angulo)) 		$angulo = traerValorDe("angulo");
		if(empty($angulo)) 		$errores['angulo'] 	= $data['mensaje'] ='falta el ángulo';
		elseif(!is_numeric($angulo))$errores['angulo']= $data['mensaje'] ='el ángulo debe ser un número';
		elseif($angulo>360||$angulo<0)$errores['angulo']= $data['mensaje'] ='el ángulo debe ser un valor entre 0 y 360';
		if(empty($distancia)) $distancia 	= traerValorDe("distancia");
		if(empty($distancia)) $errores['distancia'] = $data['mensaje'] ='falta la distancia';
		elseif(!is_numeric($distancia))$errores['distancia']= $data['mensaje'] ='la distancia debe ser un número expresado en Kms';

		if(empty($errores)){  // defino coordenada inicial
			if(strpos($coordenada, ",") !== false){
				$coords = explode(",", $coordenada);
				if(count($coords)==2){
					$coordA   = new \League\Geotools\Coordinate\Coordinate([$coords[0], $coords[1]]);
					$data['ini']['lat'] 		= $coords[0];
					$data['ini']['lon'] 		= $coords[1];
				}else{
					$errores['coordenada'] = $data['mensaje'] ='coordenada de entrada ilegible';
				}
			}else{
				$coordat  = self::geohash($coordenada);
				$data['ini']['lat'] 		= $coordat['lat'];
				$data['ini']['lon'] 		= $coordat['lon'];
				$coordA   = new \League\Geotools\Coordinate\Coordinate([$coordat['lat'], $coordat['lon']]);
			}
		}
		if(empty($errores)){
			$geotools = new \League\Geotools\Geotools();
			$distMeters = $distancia * 1000;
			$destinationPoint = $geotools->vertex()->setFrom($coordA)->destination($angulo, $distMeters);
			$data['fin']['lat'] = $destinationPoint->getLatitude();
			$data['fin']['lon'] = $destinationPoint->getLongitude();
			$coordRel = $data['ini']['lat'].",".$data['ini']['lon'].",".$data['fin']['lat'].",".$data['fin']['lon'];
			$data = self::coordRel($coordRel);
			$data['estatus'] = '1';
		}

		if(!empty($errores)){
			$data['estatus'] = '0';
			$data['error'] = $errores;
		}

		if(strpos("$_SERVER[REQUEST_URI]", __CLASS__."/".__FUNCTION__) !== false) return entregar($data); else return $data;
	}



	/**
	 * coordRel
	 * datos de relación entre dos coordenadas
	 * @return array
	 */
	public static function coordRel($coordRel=null){
		$data = array();
		if(empty($coordRel)) $coordRel = traerValorDe("coordRel");
		if(!empty($coordRel)){
			if(strpos($coordRel, ",") !== false){
				$geotools = new \League\Geotools\Geotools();
				$coords = explode(",", $coordRel);
				if(count($coords)==4){
					$coordA   = new \League\Geotools\Coordinate\Coordinate([$coords[0], $coords[1]]);
					$coordB   = new \League\Geotools\Coordinate\Coordinate([$coords[2], $coords[3]]);

					$data['ini'] = self::geohash($coords[0].",".$coords[1]);
					$data['fin'] = self::geohash($coords[2].",".$coords[3]);

				}elseif(count($coords)==2){
					$coordat  = self::geohash($coords[0]);
					$coordbt  = self::geohash($coords[1]);
					$coordA   = new \League\Geotools\Coordinate\Coordinate([$coordat['lat'], $coordat['lon']]);
					$coordB   = new \League\Geotools\Coordinate\Coordinate([$coordbt['lat'], $coordbt['lon']]);

					$data['ini'] = self::geohash($coordat['lat'].",".$coordat['lon']);
					$data['fin'] = self::geohash($coordbt['lat'].",".$coordbt['lon']);
				}
				$distance = $geotools->distance()->setFrom($coordA)->setTo($coordB);
				$vertex   = $geotools->vertex()->setFrom($coordA)->setTo($coordB);
				$middlePoint = $vertex->middle();
 
				$data['estatus'] 	= '1';
				$data['flat'] 		= $distance->flat();
				$data['gc'] 			= $distance->greatCircle();
				$data['km'] 			= floor($distance->in('km')->vincenty());
				$data['iniBear'] 	= $vertex->initialBearing();
				$data['iniCard'] 	= $vertex->initialCardinal();
				$data['finBear'] 	= $vertex->finalBearing();
				$data['finCard'] 	= $vertex->finalCardinal();
				$data['punto_medio'] = self::geohash($middlePoint->getLatitude().",".$middlePoint->getLongitude());

			}else{
				$data['estatus'] = '0';
				$data['error']['datos'] = $data['mensaje'] ='coordenadas de entrada ilegibles';
			}

		}

		if(strpos("$_SERVER[REQUEST_URI]", __CLASS__."/".__FUNCTION__) !== false) return entregar($data); else return $data;
	}


	/**
	 * geohash
	 * @return 
	 */
	public static function geohash($geohash=null, $precision=10){
		$data = array();
		if(empty($geohash)) $geohash = traerValorDe("geohash");
		if(!empty($geohash)){
			$geotools = new \League\Geotools\Geotools();
			if(strpos($geohash, ",") !== false){
				$coordenadas = new \League\Geotools\Coordinate\Coordinate(trim($geohash));

				$encoded = $geotools->geohash()->encode($coordenadas, $precision);
				$boundingBox = $encoded->getBoundingBox();
				$southWest   = $boundingBox[0];
				$northEast   = $boundingBox[1];

				$data['geohash'] = $encoded->getGeohash();
				$data['lat'] = $coordenadas->getLatitude();
				$data['lon'] = $coordenadas->getLongitude();
				$data['coord'] = $data['lat'].",".$data['lon'];
				$data['box']['lat0'] = $southWest->getLatitude();
				$data['box']['lon0'] = $southWest->getLongitude();
				$data['box']['lat1'] = $northEast->getLatitude();
				$data['box']['lon1'] = $northEast->getLongitude();
			}else{
				$decoded = $geotools->geohash()->decode($geohash);
				$boundingBox = $decoded->getBoundingBox();
				$southWest   = $boundingBox[0];
				$northEast   = $boundingBox[1];

				$data['geohash'] = $geohash;
				$data['lat'] = $decoded->getCoordinate()->getLatitude();
				$data['lon'] = $decoded->getCoordinate()->getLongitude();
				$data['coord'] = $data['lat'].",".$data['lon'];
				$data['box']['lat0'] = $southWest->getLatitude();
				$data['box']['lon0'] = $southWest->getLongitude();
				$data['box']['lat1'] = $northEast->getLatitude();
				$data['box']['lon1'] = $northEast->getLongitude();


			}

		}

		if(strpos("$_SERVER[REQUEST_URI]", __CLASS__."/".__FUNCTION__) !== false) return entregar($data); else return $data;
	}


	/**
	 * test
	 * pruebas varias.
	 * @return array
	 */
	public static function test($test=null){
		 echo (360+180)%360;
	}

	/**
	 * setGeoClima
	 * Carga de BD con lista de estaciones climáticas
	 * @return true
	 */
	public static function setGeoClima(){
		return false;  // comentar para actualizar base de datos, por protección
		$data = array();
		ini_set('memory_limit', '-1');  // este proceso es largo y pesado
		$url = "http://bulk.openweathermap.org/sample/city.list.json.gz"; // TODO descargar y descomprimir el original para estar realmente actualizado
		$file = $_SERVER["DOCUMENT_ROOT"]."/privado/city.list.json";
		$handle = fopen($file, "r");
		if ($handle) {
				$reg = array();
		    while (($line = fgets($handle)) !== false) {
		    	$linea = json_decode($line,true);
		      $reg['id'] 			= $linea['_id'];
/**/
		      if(filter_var($linea['coord']['lat'], FILTER_VALIDATE_INT) || $linea['coord']['lat'] == 0){
		      	$reg['lat'] = $linea['coord']['lat'] . ".00";
		      }else{$reg['lat'] 		= $linea['coord']['lat'];}

		      if(filter_var($linea['coord']['lon'], FILTER_VALIDATE_INT) || $linea['coord']['lon'] == 0){
		      	$reg['lon'] = $linea['coord']['lon'] . ".00";
		      }else{$reg['lon'] = $linea['coord']['lon'];}

		      $reg['country'] = $linea['country'];
		      $reg['name'] 		= $linea['name'];
/*
		      if($reg['id'] == 56083){ 
		      	print_r($reg);
		      	print_r($linea);
		      	loadDataTable(self::getBD(), self::getBD("Tb")[4], $reg);
		      	die();
		      };
*/
		    $result = loadDataTable(self::getBD(), self::getBD("Tb")[4], $reg);
		    }
		    fclose($handle);
		} else {
		    echo "error abriendo el archivo $file";
		} 
		$data = $result;
		if(strpos("$_SERVER[REQUEST_URI]", __CLASS__."/".__FUNCTION__) !== false) return entregar($data); else return $data;
	}

	/**
	 * coord
	 * muestra información relacionada con una coordenada geográfica
	 * @param $coord string coordenada geográfica
	 * @param $nocache boolean si es true entoces forza la búsqueda de la información en Internet y la actualización de datos en la BD. Por defecto trae la información de la BD como primera opcion
	 * @return array
	 */
	public static function coord($coord=null, $nocache=null, $nombre=null, $tipo=null){
		$data 		= array();
		$ggl 			= array();
		$clima 		= array();
		$errores  = array();
		$tcoord 	= $tregion = self::getBD('Tb')[3];
		if(empty($coord)) 	$coord 		= traerValorDe("coord"); 
		if(empty($nocache)) $nocache 	= traerValorDe("nocache");
		if(empty($nombre)) 	$nombre 	= traerValorDe("nombre");
		if(empty($nombre)) 	$nombre 	= "";
		if(empty($tipo)) 		$tipo 		= traerValorDe("tipo");
		if(empty($tipo)) 		$tipo 		= "";
		if(!$coord) $errores['coorenadas'] = $data['mensaje'] = 'Faltan las coordenadas';

		if(empty($errores)){
			$coord = self::geohash($coord);
			$lat = $coord['lat'];
			$lon = $coord['lon'];
			$hash = $coord['geohash'];
			$sql = "SELECT data FROM $tcoord WHERE hash = '$hash'";
			$resultado = DbLink::resultados($sql,self::getBD());
			if($resultado !== false && !$nocache){
				$json = $resultado[0]['data'];
				$data = json_decode($json,true);
			}else{

				$fclase = $_SERVER["DOCUMENT_ROOT"]."/servicios/google/google.php";
  			if(is_file($fclase)){
  				require_once $fclase;
  				$ggl = google::coord("$lat,$lon");
  			}

				$fclase = $_SERVER["DOCUMENT_ROOT"]."/servicios/clima/clima.php";
  			if(is_file($fclase)){
  				require_once $fclase;
  				$clima = clima::get("$lat,$lon",1);
  			}
  			$data = array_merge($ggl,$clima);
  			$toBD = array('hash' => $hash, 'nombre' => $nombre, 'data' => json_encode($data));
  			$data['set'] = loadDataTable(self::getBD(), $tcoord, $toBD);
  			if(!empty($tipo)){
  				$tipos = explode(",", $tipo);
  				$ids = array();
  				//print_r($tipos);die();
  				foreach ($tipos as $key => $value) {
  					$sql = "SELECT * FROM geo_tipo WHERE tipo = '$value'";
  					$resultado = DbLink::resultados($sql,self::getBD());
  					if($resultado !==false) $ids[] = $resultado[0]['id'];
  				}
  				foreach ($ids as $key => $value) {
  					$toBD = array('hash' => $hash, 'id_tipo' => $value);
  					$data['set']['tipos'][] = loadDataTable(self::getBD(), 'geo_tipo_lugar', $toBD);
  				}
  			}

			}
			$data['estatus'] = '1';
		}

		if(!empty($errores)){
			$data['estatus'] = '0';
			$data['error'] = $errores;
		}

		if(strpos("$_SERVER[REQUEST_URI]", __CLASS__."/".__FUNCTION__) !== false) return entregar($data); else return $data;
	}

	/**
	 * region
	 * Retorna una lista de regiones segun criterio de búsqueda
	 * @param $region string criterio de búsqueda, nulo por defecto
	 * @return array Lista de regiones que cumplen con el criterio de búsqueda
	 */
	public static function region($region=null){
		$data       = array();
		$tregion = geo::getBD('Tb')[1];
		if(empty($region)) $region = traerValorDe("region");
		if($region) $region = remove_accents(strtolower($region)); else $region = "";
		$sql="select * from $tregion where lower(sp_ascii(nombre)) LIKE '%$region%'";
		$resultado = DbLink::resultados($sql,geo::getBD());
		if($resultado !== false) $data = $resultado;
		else $data["error"][404] = 'No se encuentra';
		if(strpos("$_SERVER[REQUEST_URI]", __CLASS__."/".__FUNCTION__) !== false)return entregar($data); else return $data;
	}

	/**
	 * ubigeo
	 * Retorna los datos de una parroquia determinada o una lista de estados municipios
	 * @param $ubigeo string el código a buscar, de dos, cuaro o seis caracteres numéricos
	 * @return array con datos geográficos basados en el ubigeo
	 */
	public static function ubigeo($ubigeo=null){
		$errores    = array();
		$data       = array();
		$data["error"][404] = 'No se encuentra';
		if(empty($ubigeo)) $ubigeo = traerValorDe("ubigeo"); 
		if(!$ubigeo) $errores['ubigeo'] = $data['mensaje'] = 'Falta el código';

		if (empty($errores)){
			if(!preg_match("/^[0-9]+$/", $ubigeo)){
				$ubigeo = remove_accents(strtolower($ubigeo));
				$sql="select * from geo_ubigeo where lower(sp_ascii(parroquia)) LIKE '%$ubigeo%'";
			}
			elseif(!in_array(strlen($ubigeo), array(2,4,6))){
				 $errores['ubigeo'] = $data['mensaje'] = 'El número de caractéres no es correcto';
			} 
			elseif(strlen($ubigeo)==6)	$sql="select * from geo_ubigeo where cod_par='$ubigeo'";
			elseif(strlen($ubigeo)==4)	$sql="select * from geo_ubigeo where cod_mun='$ubigeo'";
			elseif(strlen($ubigeo)==2)	$sql="select * from geo_ubigeo where cod_ent='$ubigeo'";
			//$data["error"]['sql'] = $sql;
			$resultado = DbLink::resultados($sql,geo::getBD());
			if($resultado !== false){
				if(strlen($ubigeo)==6) $data = $resultado[0];
				else $data = $resultado;
			} 
			$data['estatus'] = '1';
		}

		if(!empty($errores)){
			$data['estatus'] = '0';
			$data['error'] = $errores;
		}

		if(strpos("$_SERVER[REQUEST_URI]", __CLASS__."/".__FUNCTION__) !== false)return entregar($data); else return $data;
	}

	/**
	 * entidad
	 * Retorna una lista de entidades segun un criterio de búsqueda
	 * @param $entidad string criterio de búsqueda, nulo por defecto
	 * @return array {-[cod_ent,entidad],n}
	 */
	public static function entidad($entidad=null, $columnas=null){
		$data       = array();
		$tentidad = geo::getBD('Tb')[2];
		if(empty($entidad)) $entidad = traerValorDe("entidad");
		if($entidad) $entidad = remove_accents(strtolower($entidad)); else $entidad = "";
		if(empty($columnas)) $columnas = traerValorDe("columnas");
		if($columnas){
			$columnas = remove_accents(strtolower($columnas));
			$cols = explode(",", $columnas);
			$rtabla = getColumTable($tentidad, geo::getBD());
			$columnasF = array();
			foreach ($cols as $columna){
				if(array_search($columna, array_column($rtabla, 'column_name')) !== false) {
				  $columnasF[] = $columna;
				}
			}
			$columnas = implode(",", $columnasF);
		}  else $columnas = "*";

		if(preg_match("/^[0-9]+$/", $entidad) && strlen($entidad) == 2){
			$sql="select $columnas from $tentidad where ubigeo = '$entidad'";
		}else{
			$sql="select $columnas from $tentidad where lower(sp_ascii(nombre)) LIKE '%$entidad%'";
		}
		$resultado = DbLink::resultados($sql,geo::getBD());
		if($resultado !== false) $data = $resultado; 
		else $data["error"][404] = 'No se encuentra';
		if(strpos("$_SERVER[REQUEST_URI]", __CLASS__."/".__FUNCTION__) !== false)return entregar($data); else return $data;
	}

	/**
	 * municipio
	 * Retorna una lista de municipios segun un criterio de búsqueda
	 * @param $municipio string criterio de búsqueda, nulo por defecto
	 * @return array {-[cod_mun,municipio],n}
	 */
	public static function municipio($municipio=null){
		$errores    = array();
		$data       = array();
		$municipio = traerValorDe("municipio");
		if($municipio) $municipio = remove_accents(strtolower($municipio));
		else $municipio = "";
		$data["error"][404] = 'No se encuentra';
		if(strlen($municipio)==2 && preg_match("/^[0-9]+$/", $municipio)){
			$sql="select DISTINCT ON (cod_mun) *
					from geo_ubigeo where cod_ent = '$municipio'";
		}else{
			$sql="select DISTINCT ON (cod_mun) *
					from geo_ubigeo where lower(sp_ascii(municipio)) LIKE '%$municipio%'";
		}

		if (empty($errores)){
			$resultado = DbLink::resultados($sql,geo::getBD());
			if($resultado !== false){
				$data = $resultado;
			} 
			$data['estatus'] = '1';
		}

		if(!empty($errores)){
			$data['estatus'] = '0';
			$data['error'] = $errores;
		}

		if(strpos("$_SERVER[REQUEST_URI]", __CLASS__."/".__FUNCTION__) !== false)return entregar($data); else return $data;
	}

	/**
	 * getTipo
	 * Retorna una lista de tipos de lugares que se asemejan al lugar pedido
	 * @param $getTipo string corresponde a un posible tipo de lugar
	 * @return array llista de tipos de lugares
	 */
	public static function getTipo($getTipo=null, $set=null){
		$errores    = array();
		$data       = array();
		if(empty($getTipo)) $getTipo = traerValorDe("getTipo");
		if(!empty($getTipo)) $getTipo = remove_accents(strtolower($getTipo));
		else $getTipo = "";
		if(empty($set)) $set = traerValorDe("set");
//		if($set=='no') $set = false;
		
		$tipos = explode(",", $getTipo);

		if(!empty($set) && $set !=='no'){
			foreach ($tipos as $key => $value){
				$result = loadDataTable(self::getBD(), self::getBD("Tb")[5], array('tipo'=>$value));
			}
			$data = geo::getTipo($getTipo,'no');
			//$data = array_merge($data, $result);

		}else{
			$whereitem = array();
			foreach ($tipos as $key => $value) $whereitem[] = "lower(sp_ascii(tipo)) LIKE '%$value%'";
			$whereitems = implode(" OR ", $whereitem);

			$sql="SELECT id, tipo FROM geo_tipo WHERE $whereitems";
			$resultado = DbLink::resultados($sql,geo::getBD());
			if($resultado !== false) $data['tipos'] = $resultado; 
			else $errores['tipo'] = $data['mensaje'] = "Sin resultados";
			$data['estatus'] = '1';
		}

		if(!empty($errores)){
			$data['estatus'] = '0';
			$data['error'] = $errores;
		}
		if($set=='no') return $data;
		if(strpos("$_SERVER[REQUEST_URI]", __CLASS__."/".__FUNCTION__) !== false)return entregar($data); else return $data;
	}

	/**
	 * total
	 * Total registros relacionados
	 * @return array de datos de persona
	 */
	public static function total(){
		$data = array();
		foreach (geo::getBD('Tb') as $tabla) $data[$tabla] = totalRegBDTable($tabla, geo::getBD());
		if(strpos("$_SERVER[REQUEST_URI]", __CLASS__."/".__FUNCTION__) !== false)return entregar($data); else return $data;
	}

	/**
	 * analyze
	 * Actualiza las estadísticas de cada tabla
	 * @return array de datos de persona
	 */
	public static function analyze(){
		$data = array();
		foreach (geo::getBD('Tb') as $tabla) $data[$tabla] = analyzeBDTable($tabla, geo::getBD());
		if(strpos("$_SERVER[REQUEST_URI]", __CLASS__."/".__FUNCTION__) !== false)return entregar($data); else return $data;
	}



// fin clase geo
}


?>