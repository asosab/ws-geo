-- creo la tabla
CREATE TABLE geo_ubigeo
(
  ID_DPT    bigint NOT NULL,
  cod_par   character varying(6) NOT NULL,
  cod_ent   character varying(2) NOT NULL,
  entidad   character varying(50) NOT NULL,
  cod_mun   character varying(4) NOT NULL,
  municipio character varying(50) NOT NULL,
  adesechar character varying(50) NOT NULL,
  parroquia character varying(50) NOT NULL,
  CONSTRAINT ine_pkey PRIMARY KEY (cod_par)
)
WITH (OIDS=FALSE);
ALTER TABLE geo_ubigeo OWNER TO postgres;
GRANT ALL ON TABLE geo_ubigeo TO postgres;
GRANT ALL ON TABLE geo_ubigeo TO data;

-- cargo los datos del archivo .csv
copy geo_ubigeo(id,cod_par,cod_ent,entidad,cod_mun,municipio,adesechar,parroquia) FROM '01062016-DPTOFE_2013.CSV' DELIMITER ';' CSV;

-- borro la columna con el id de parroquia (es el ubigeo original)
ALTER TABLE geo_ubigeo DROP adesechar;

-- limpio los campos y arreglo el código de municipo
UPDATE geo_ubigeo 
SET entidad   = trim(both ' ' from entidad),
    municipio = trim(both ' ' from municipio),
    parroquia = trim(both ' ' from parroquia),
    cod_mun   = cod_ent||cod_mun

  -- Colocar los acentos donde van
UPDATE geo_ubigeo SET entidad = 'Anzoátegui' where entidad = 'Anzoategui';
UPDATE geo_ubigeo SET entidad = 'Bolívar' where entidad = 'Bolivar';
UPDATE geo_ubigeo SET entidad = 'Falcón' where entidad = 'Falcon';
UPDATE geo_ubigeo SET entidad = 'Guárico' where entidad = 'Guarico';
UPDATE geo_ubigeo SET entidad = 'Mérida' where entidad = 'Merida';
UPDATE geo_ubigeo SET entidad = 'Táchira' where entidad = 'Tachira';

UPDATE geo_ubigeo SET municipio = 'Simón Bolívar' where municipio = 'Simon Bolivar';
UPDATE geo_ubigeo SET municipio = 'Simón Rodríguez' where municipio = 'Simon Rodriguez';
UPDATE geo_ubigeo SET municipio = 'Simón Planas' where municipio = 'Simon Planas';
UPDATE geo_ubigeo SET municipio = 'Autónomo Atures' where municipio = 'Autonomo Atures';
UPDATE geo_ubigeo SET municipio = 'José Gregorio Monagas' where municipio = 'Jose Gregorio Monagas';
UPDATE geo_ubigeo SET municipio = 'Pedro María Freites' where municipio = 'Pedro Maria Freites';
UPDATE geo_ubigeo SET municipio = 'Píritu' where municipio = 'Piritu';
UPDATE geo_ubigeo SET municipio = 'Caroní' where municipio = 'Caroni';
UPDATE geo_ubigeo SET municipio = 'Bolívar' where municipio = 'Bolivar';
UPDATE geo_ubigeo SET municipio = 'Gómez' where municipio = 'Gomez';
UPDATE geo_ubigeo SET municipio = 'José Félix Ribas' where municipio = 'Jose Felix Ribas';
UPDATE geo_ubigeo SET municipio = 'Morán' where municipio = 'Moran';
UPDATE geo_ubigeo SET municipio = 'Bermúdez' where municipio = 'Bermudez';
UPDATE geo_ubigeo SET municipio = 'Guásimos' where municipio = 'Guasimos';

UPDATE geo_ubigeo SET parroquia = 'Simón Bolívar' where parroquia = 'Simon Bolivar';
UPDATE geo_ubigeo SET parroquia = 'Mesa Bolívar' where parroquia = 'Mesa Bolivar';
UPDATE geo_ubigeo SET parroquia = 'Bolívar' where parroquia = 'Bolivar';
UPDATE geo_ubigeo SET parroquia = 'Urbana Juan Vicente Bolívar y Ponte' where parroquia = 'Urbana Juan Vicente Bolivar y Ponte';
UPDATE geo_ubigeo SET parroquia = 'No Urbana Simón Bolívar' where parroquia = 'No Urbana Simón Bolivar';


CREATE OR REPLACE FUNCTION sp_ascii(character varying)
RETURNS text AS
$BODY$
SELECT TRANSLATE
($1,
'áàâãäéèêëíìïóòôõöúùûüÁÀÂÃÄÉÈÊËÍÌÏÓÒÔÕÖÚÙÛÜçÇ',
'aaaaaeeeeiiiooooouuuuAAAAAEEEEIIIOOOOOUUUUcC');
$BODY$
LANGUAGE 'sql';

CREATE TABLE geo_entidades
(
  ubigeo character(2) NOT NULL,             -- Código de representación de entidades, municipios y parroquias
  nombre character varying(50),             -- Nombre de la entidad 
  iso31662 character varying(5),            -- iso_3166-2
  bandera_URL character varying(512),       -- deberá ser cambiado por una url oficial
  escudo_URL character varying(512),        -- deberá ser cambiado por una url oficial
  llamada character varying(512),           -- otro nombre o frase por la que se le conoce
  gentilicio character varying(30),         --
  lema character varying(512),              --
  km2_superficie int,                       --
  km_costa int,                             --
  km_fronteras int,                         --
  altitud_media int,                        -- medida en metros sobre el nivel del mar
  region character varying(30),             -- Este campo deberá ser cambiado por region_id al momento de definir los identificadores de regiones
  capital character varying(50),            -- Este campo deberá ser cambiado por capital_id cando se definan los identificadores de centros poblados
  hh character varying(5),                  -- Huso horario
  lat numeric(12,8),                        -- latitud geográica
  lon numeric(12,8),                        -- longitud geográfica
  geohash character varying(10),
  poblacion integer,
  simbolo character varying(100),
  CONSTRAINT geo_entidades_pkey PRIMARY KEY (ubigeo)
)
WITH (OIDS=FALSE);
ALTER TABLE geo_entidades OWNER TO postgres;
GRANT ALL ON TABLE geo_entidades TO postgres;
GRANT ALL ON TABLE geo_entidades TO data;
COMMENT ON COLUMN geo_entidades.geohash IS 'hash de coordenadas geográficas';

CREATE TABLE geo_regiones
(
  id serial,                                -- a discutir, debería ser cambiado por un identificador con significado
  nombre character varying(50),             -- Nombre de la region 
  poblacion int,
  superficie int,
  CONSTRAINT geo_regiones_pkey PRIMARY KEY (id)
)
WITH (OIDS=FALSE);
ALTER TABLE geo_regiones OWNER TO postgres;
GRANT ALL ON TABLE geo_regiones TO postgres;
GRANT ALL ON TABLE geo_regiones TO data;


/*
Soporte postgis
sudo -u postgres psql -c "CREATE EXTENSION postgis; CREATE EXTENSION postgis_topology;" NOMBRE_BD
*/

CREATE TABLE geo_lugar
(
  hash character varying(10) NOT NULL,          -- geohash de las coordenadas geográficas del lugar
  nombre character varying(200),
  data jsonb NOT NULL,                          -- datos recabados de servicios web
  CONSTRAINT geo_lugar_pkey PRIMARY KEY (hash)
)
WITH (OIDS=FALSE);
ALTER TABLE geo_lugar OWNER TO postgres;
GRANT ALL ON TABLE geo_lugar TO postgres;
GRANT ALL ON TABLE geo_lugar TO data;

CREATE TABLE geo_tipo_lugar
(
  id_tipo bigint NOT NULL,          
  hash character varying(10) NOT NULL,                         
  CONSTRAINT geo_tipo_lugar_pkey PRIMARY KEY (id_tipo,hash),
  CONSTRAINT geo_tipo_lugar_id_tipo_fkey FOREIGN KEY (id_tipo)
    REFERENCES geo_tipo (id) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT geo_tipo_lugar_hash_fkey FOREIGN KEY (hash)
    REFERENCES geo_lugar (hash) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (OIDS=FALSE);
ALTER TABLE geo_tipo_lugar OWNER TO postgres;
GRANT ALL ON TABLE geo_tipo_lugar TO postgres;
GRANT ALL ON TABLE geo_tipo_lugar TO data;

CREATE TABLE geo_tipo
(
  id serial NOT NULL,          
  tipo character varying(200),                         
  CONSTRAINT geo_tipo_pkey PRIMARY KEY (id),
  CONSTRAINT geo_tipo_tipo UNIQUE (tipo)
)
WITH (OIDS=FALSE);
ALTER TABLE geo_tipo OWNER TO postgres;
GRANT ALL ON TABLE geo_tipo TO postgres;
GRANT ALL ON TABLE geo_tipo TO data;

-- estaciones climatológicas
CREATE TABLE geo_clima
(
  id      bigint NOT NULL,                          -- identificador ante openweathermap.org
  lat     numeric(12,8) NOT NULL,                   -- latitud geográica
  lon     numeric(12,8) NOT NULL,                   -- longitud geográfica
  name    character varying(50),                    -- nombre de área geográfica de la estación climatológica
  country character varying(2),                     -- código del país
  alt     integer,                                   -- altura msnm
  CONSTRAINT geo_clima_pkey PRIMARY KEY (id)
)
WITH (OIDS=FALSE);
ALTER TABLE geo_clima OWNER TO postgres;
GRANT ALL ON TABLE geo_clima TO postgres;
GRANT ALL ON TABLE geo_clima TO data;
