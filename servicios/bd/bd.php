<?php 

class bd {

	/**
	 * accesoDef
	 * valores por defecto de acceso a cada método.
	 * @param string $metodo el nombre del método a validar 
	 * @return int nivel  de acceso
	 */
	public static function accesoDef($metodo){
		switch ($metodo) {
		    case "getBD": 			return 1; break;
		    case "total": 			return 1; break;
		    case "analyze": 		return 1; break;
		    case "accesoDef": 	return 1; break;
		    case "getDato": 		return 1; break;
		    case "getCron": 		return 1; break;
		    case "test": 				return 1; break;
		    case "getINCES": 		return 5; break;
		    default: 						return 1; 
		}
	}

	/**
	 * getBD
	 * retorna la base de datos en uso.
	 * @return string con nombre de la bd
	 */
	public static function getBD($dar='BD'){
			if($dar=='BD') return "BD";
			if($dar=='Tb') return array('');
	}

	public static function test($test=null){
		$data = array();
		if(empty($test)) $test = traerValorDe("test");

		if(strpos("$_SERVER[REQUEST_URI]", __CLASS__."/".__FUNCTION__) !== false) return entregar($data); else return $data;
	}


	/**
	 * columnas
	 * Retorna el último registro actualizado de una tabla
	 * @return array de datos de persona
	 */
	public static function columnas($columnas=null, $db='BD'){
		$data = array();
		if(empty($columnas)) $columnas = traerValorDe("columnas");

		$tabla = $columnas;
	  $sql = "
	  	SELECT * 
	  	FROM information_schema.columns 
	  	WHERE table_name = '$tabla'";
		$resultado = DbLink::resultados($sql,$db);

	  if($resultado !== false){
	  	foreach ($resultado as $key => $value) {
	  		$data[$value['column_name']] = $value;
	  	}
	  }else{
	  	$data['estatus'] = '0';
	  } 

		if(strpos("$_SERVER[REQUEST_URI]", __CLASS__."/".__FUNCTION__) !== false) return entregar($data); else return $data;
	}

	/**
	 * getINCES
	 * Retorna contenido de la tabla INCES
	 * @return array de datos de un registro
	 */
	public static function getINCES(){
		$data = self::getData("inces");
		if(strpos("$_SERVER[REQUEST_URI]", __CLASS__."/".__FUNCTION__) !== false) return entregar($data); else return $data;
	}

	/**
	 * getData
	 * Retorna contenido de una tabla
	 * @return array de datos de un registro
	 */
	public static function getData($getData=null, $campos=null, $where=null, $pag=null,$can=null, $orden=null, $dir=null, $BD=null){
		$data = array();
		if(empty($getData)) $getData = traerValorDe("getData");
		if(empty($can)) $can = traerValorDe("can");if(empty($can)) 	$can = 100;
		if(empty($pag)) $pag = traerValorDe("pag");if(empty($pag)) 	$pag = 0;
		if(empty($dir)) $dir = traerValorDe("dir");if(empty($dir)) 	$dir = 'DESC';
		if(empty($orden)) $orden = traerValorDe("orden");
		if(empty($where)) $where = traerValorDe("where");
		if(empty($campos)) $campos = traerValorDe("campos");if(empty($campos)) $campos = '*';
		if(empty($BD)) $BD = traerValorDe("BD");if(empty($BD)) $BD = 'BD';

		$tabla = $getData;
		$llaves = self::getLlaves($tabla, $BD);

		if(!empty($where)){
			$where = "WHERE $where";
		}else{
			$where = "";
		}

		if(!empty($orden)){
			$orden = "ORDER BY ".implode(" $dir,", explode(",", $orden)). " $dir ";
		}else{
			$orden = "ORDER BY ". implode(" $dir,", $llaves)." $dir ";
		}
		$sql = "SELECT $campos FROM $tabla $where $orden LIMIT $can OFFSET $pag";
//echo $sql;die();
		$resultado = DbLink::resultados($sql,self::getBD());
		if($resultado !==false){
			$data = $resultado;
		}else{
			$data['estatus'] = '0';
			$errores['query'] = $data['mensaje'] = 'sin registros';
		}

		if(strpos("$_SERVER[REQUEST_URI]", __CLASS__."/".__FUNCTION__) !== false) return entregar($data); else return $data;
	}

	/**
	 * lastUpdate
	 * Retorna el último registro actualizado de una tabla
	 * @return array de datos de persona
	 */
	public static function lastUpdate($lastUpdate=null, $BD='BD', $colFechaAct='fecha_modificacion'){
		$data = array();
		if(empty($lastUpdate)) $lastUpdate 	= traerValorDe("lastUpdate");
		$tabla = $lastUpdate;

		$columnas = self::columnas($tabla, $BD);
		if(array_search($colFechaAct,array_column($columnas, 'column_name'))){
		  $sql = "
		  	SELECT * FROM $tabla 
		  	WHERE $colFechaAct is not null 
		  	ORDER BY $colFechaAct DESC LIMIT 1";
		  $resultado = DbLink::resultados($sql,$BD);

		  if($resultado !== false){
		  	$data = $resultado[0];
		  	$data['estatus'] = '1';
		  }else{
		  	$data['estatus'] = '0';
		  }
		}else{
			$data['error'] = "Esta tabla no posee un campo $colFechaAct";
			$data['estatus'] = '0';
		}

		if(strpos("$_SERVER[REQUEST_URI]", __CLASS__."/".__FUNCTION__) !== false) return entregar($data); else return $data;
	}

	/**
	 * getLlaves
	 * Retorna el último registro actualizado de una tabla
	 * @return array de datos de persona
	 */
	public static function getLlaves($getLlaves=null, $db='BD'){
		$data = array();
		if(empty($getLlaves)) $getLlaves = traerValorDe("getLlaves");
		$tabla = $getLlaves;

	  $sql = "
	  	SELECT a.attname
      FROM   pg_index i
      JOIN   pg_attribute a ON a.attrelid = i.indrelid AND a.attnum = ANY(i.indkey)
      WHERE  i.indrelid = '$tabla'::regclass AND i.indisprimary";
		$resultado = DbLink::resultados($sql,$db);
	  if($resultado !== false){
	  	foreach ($resultado as $key => $value) $data[] = $value['attname'];
	  }else{
	  	$data['estatus'] = '0';
	  } 

		if(strpos("$_SERVER[REQUEST_URI]", __CLASS__."/".__FUNCTION__) !== false) return entregar($data); else return $data;
	}

	/**
	 * uniqueFromTbl
	 * 
	 * @return array 
	 */
	public static function uniqueFromTbl($uniqueFromTbl=null, $db='BD'){
		$data = array();
		if(empty($uniqueFromTbl)) $uniqueFromTbl = traerValorDe("uniqueFromTbl");
		$tabla = $uniqueFromTbl;
		$data['unique'] = [];
	  $sql = "
	  	SELECT constraint_name 
	  	FROM information_schema.table_constraints 
	  	WHERE table_name='$tabla' AND constraint_type='UNIQUE'";
		$resultado = DbLink::resultados($sql,$db);
	  if($resultado !== false){
	  	foreach ($resultado as $key => $value) {
	  		$str = str_replace("{$tabla}_", "", $value['constraint_name']);
	  		$str = str_replace("_key", "", $str);
	  		$data['unique'][] = $str;
	  	}
	  	$data['estatus'] = ['1'];
	  }else{
	  	$data['estatus'] = '0';
	  } 

		if(strpos("$_SERVER[REQUEST_URI]", __CLASS__."/".__FUNCTION__) !== false) return entregar($data); else return $data;
	}

	/**
	 * infoTable
	 * 
	 * @return array de datos de persona
	 */
	public static function infoTable($infoTable=null, $db='BD'){
		$data = array();
		if(empty($infoTable)) $infoTable = traerValorDe("infoTable");
		$tabla = $infoTable;

	  $sql = "
	  	SELECT table_name as nombre, table_catalog as bd, table_schema as esquema, udt_catalog, udt_schema
			FROM INFORMATION_SCHEMA.COLUMNS 
			WHERE table_name = '$tabla' LIMIT 1";
		$resultado = DbLink::resultados($sql,$db);
	  if($resultado !== false){
	  	$data = $resultado[0];
	  }else{
	  	$data['estatus'] = '0';
	  } 

		if(strpos("$_SERVER[REQUEST_URI]", __CLASS__."/".__FUNCTION__) !== false) return entregar($data); else return $data;
	}

	/**
	 * existeTbl
	 * 
	 * @return boolean 
	 */
	public static function existeTbl($existeTbl=null, $db='BD'){
		$data = array();
		if(empty($existeTbl)) $existeTbl = traerValorDe("existeTbl");
		if(empty($existeTbl)) $errores['tabla'] = $data['mensaje'] ='falta la tabla';

		if(empty($errores)){
			$tabla = $existeTbl;
			$sql = "
				SELECT EXISTS (
				   SELECT 1
				   FROM   information_schema.tables 
				   WHERE  table_schema = 'public'
				   AND    table_name = '$tabla'
				);
			";
			$resultado = DbLink::resultados($sql,$db);
		  if($resultado !== false){
		  	$data['existe'] = ($resultado[0]['exists'] == 't')? true:false;
		  	$data['estatus'] = '1';
		  }else{
		  	$data['estatus'] = '0';
		  } 
		}
		
		if(!empty($errores)){$data['estatus'] 		= '0';$data['error']  = $errores;}
		if(strpos("$_SERVER[REQUEST_URI]", __CLASS__."/".__FUNCTION__) !== false) return entregar($data); else return $data;
	}

	/**
	 * getTable
	 * Retorna el último registro actualizado de una tabla
	 * @return array de datos de persona
	 */
	public static function getTable($getTable=null, $db='BD'){
		$data = array();
		if(empty($getTable)) $getTable = traerValorDe("getTable");
		if(empty($getTable)) $errores['tabla'] = $data['mensaje'] ='falta la tabla';

		if(empty($errores)){
			$tabla = $getTable;
			if(!self::existeTbl($tabla, $db)['existe']) $errores['tabla'] = $data['mensaje'] ="la tabla $tabla no existe";
		}
		
		if(empty($errores)){
			$data[$tabla] 							= self::infoTable($tabla, $db);
			$data[$tabla]['registros'] 	= self::totalRegistros($tabla, $db);
			$data[$tabla]['columnas'] 	= self::columnas($tabla, $db);
			$data[$tabla]['llaves'] 		= self::getLlaves($tabla, $db);
			$data[$tabla]['unique'] 		= self::uniqueFromTbl($tabla, $db)['unique'];
			

			$data[$tabla]['secuencias'] = [];
			foreach ($data[$tabla]['llaves'] as $key => $value) {
				if(strpos($data[$tabla]['columnas'][$value]['column_default'], 'nextval')!==false){
				  $data[$tabla]['secuencias'][] = $value;
				}
			}
			$data[$tabla]['lastUpdate'] 		= self::lastUpdate($tabla, $db);
			$data['estatus'] = '1';
		}

		if(!empty($errores)){$data['estatus'] = '0';$data['error'] = $errores;}
		if(strpos("$_SERVER[REQUEST_URI]", __CLASS__."/".__FUNCTION__) !== false) return entregar($data); else return $data;
	}

	/**
	 * secuencias
	 * Retorna el último registro actualizado de una tabla
	 * @return array de datos de persona
	 */
	public static function secuencias($db='BD'){
		$data = array();
	  $sql = "SELECT c.relname FROM pg_class c WHERE c.relkind = 'S';";
		$resultado = DbLink::resultados($sql,$db);
	  if($resultado !== false){
	  	foreach ($resultado as $key => $value) $data['secuencias'][] = $value['relname'];
	  	$data['estatus'] = '1';
	  }else{
	  	$data['estatus'] = '0';
	  } 

		if(strpos("$_SERVER[REQUEST_URI]", __CLASS__."/".__FUNCTION__) !== false) return entregar($data); else return $data;
	}

	/**
	 * lastValue
	 * Retorna el último valor de una secuencia
	 * @return array de datos de persona
	 */
	public static function lastValue($lastValue = null, $db='BD'){
		$data = array();

		if(empty($lastValue)) $lastValue = traerValorDe("lastValue");
		if(empty($lastValue))	$tabla = self::secuencias($db)['secuencias'];
		else $tabla = explode(",", $lastValue);
		$tablas = implode(",", $tabla);
		$valor = array();
		foreach ($tabla as $key => $value) $valor[] = "$value.last_value as $value";
		$valores = implode(",", $valor);
	  $sql = "SELECT $valores FROM $tablas";

		$resultado = DbLink::resultados($sql,$db);
	  if($resultado !== false){
	  	$data['last_value'] = $resultado[0];
	  	$data['estatus'] = '1';
	  }else{
	  	$data['estatus'] = '0';
	  } 

		if(strpos("$_SERVER[REQUEST_URI]", __CLASS__."/".__FUNCTION__) !== false) return entregar($data); else return $data;
	}

	/**
	 * totalRegistros
	 * Retorna el último registro actualizado de una tabla
	 * @return array de datos de persona
	 */
	public static function totalRegistros($totalRegistros=null, $db='BD', $nspname='public'){
		if(empty($totalRegistros)) $totalRegistros = traerValorDe("totalRegistros");
		$tabla = $totalRegistros;
	  $sql = "
	  	SELECT reltuples 
	  	FROM pg_class 
	  	JOIN pg_namespace ON (pg_class.relnamespace = pg_namespace.oid)
      WHERE nspname = '$nspname' AND relname = '$tabla'";
		$resultado = DbLink::resultados($sql,$db);

	  if($resultado !== false) $data = $resultado[0]['reltuples'];
	  else $data = false;

		if(strpos("$_SERVER[REQUEST_URI]", __CLASS__."/".__FUNCTION__) !== false) return entregar($data); else return $data;
	}

	/**
	 * loadData
	 * 
	 * @param array $data contiene uno o varos registros para ingresar en la tabla
	 * @param string $table nombre de la tabla
	 * @param array $ref relacion de nombres de columnas con datos suministrados
	 * @param string $db nombre de la base de datos a utilizar
	 * @return array indicando estado del registro
	 */
	public static function loadData($datos=null, $table=null, $ref=null, $db='BD'){
		$data = array();
		$valores = array();
		$errores  = array();
		if(empty($datos)) $datos 	= traerValorDe("datos");
		if(empty($datos)) $errores['datos'] = $data['mensaje'] ='faltan los datos';
		if(empty($table)) $table 	= traerValorDe("table");
		if(empty($table)) $errores['tabla'] = $data['mensaje'] ='falta la tabla a cargar';
		if(empty($ref)) $ref 	= traerValorDe("ref");

		if(empty($errores)){
			$tabla = self::getTable($table, $db)[$table];
			// usar los nombres referidos
			if(!empty($ref)){
				foreach ($datos as $key => $value) {
					if(array_key_exists($key, $ref)) $valores[$ref[$key]] = $value;
				}
			}else{
				$valores = $datos;
			}
//var_dump($datos);die();
			$lista = [];
			foreach ($valores as $key => $value) {
				if(array_key_exists($key, $tabla['columnas'])) $lista[$key] = $value;
			}
			if(array_key_exists('fecha_modificacion', $tabla['columnas'])){
				$lista['fecha_modificacion'] = "" . date("Y-m-d H:i:s");
			}
			$secuencias = [];
			foreach ($tabla['secuencias'] as $key => $value) {
				if(array_key_exists($value, $lista)) $secuencias[] = $value;
			}

			// quienes son llaves
			$llaves = [];
			foreach ($tabla['llaves'] as $key => $value) {
				if(array_key_exists($value, $lista)) $llaves[] = $value;
			}

			// quienes son unicos
			$unicos = [];
			foreach ($tabla['unique'] as $key => $value) {
				if(array_key_exists($value, $lista)) $unicos[] = $value;
			}

			// si hay valores de columnas tipo unique repetidos no se procesa la petición
			if(!empty($unicos)){
				$wherer = [];
				foreach ($unicos as $key => $value) {
					$wherer[] = "$value = " . self::formatDBColum($tabla, $value, $lista[$value], $db);
				}
				$wherel = implode(" OR ", $wherer);
				$sql = "SELECT 1 FROM $table WHERE $wherel";
				$resultado = DbLink::resultados($sql,$db);
				if($resultado !== false){
			  	$errores['tabla'] = $data['mensaje'] ='Se están repitiendo valores de campos únicos';
			  }
			}
		}

		if(empty($errores)){		
      $granWhere = []; 
			if(!empty($secuencias)){
				$where = [];
	      foreach ($secuencias as $value){
	        $where[] = "$value = " . self::formatDBColum($tabla, $value, $lista[$value], $db);
	      }
	      $granWhere[] = "(" . implode(' AND ', $where) . ")";
			}

			if(!empty($llaves)){
				$where = [];
	      foreach ($llaves as $value){
	        $where[] = "$value = " . self::formatDBColum($tabla, $value, $lista[$value], $db);
	      }
	      $granWhere[] = "(" . implode(' AND ', $where) . ")";
			}
      
      $sets = [];
      $keys = [];
      $vals = [];
			foreach ($lista as $key => $value) {
				$keys[] = $key;
				$val 		= self::formatDBColum($tabla, $key, $value, $db);
				$vals[] = $val;
				if(!in_array($key,$llaves))	$sets[] = "$key = $val";
			}
			$setl 	= implode(', ', $sets);
			$wherel = implode(' AND ', $granWhere);
			if(!empty($wherel)) $wherel = "WHERE $wherel";
			$keysl 	= implode(', ', $keys);
			$valsl 	= implode(', ', $vals);
			$sqlUpdate = "";
      if(!empty($wherel)) $sqlUpdate = "UPDATE $table SET $setl $wherel RETURNING 1;";
      $sqlInsert = "
      	INSERT INTO $table ($keysl) 
      	SELECT $valsl ";
      if(!empty($wherel)) $sqlInsert .=	"WHERE NOT EXISTS (SELECT 1 FROM $table $wherel) RETURNING 2;";
      else $sqlInsert .=	" RETURNING 2;";
      $sql = $sqlUpdate . $sqlInsert;
//var_dump($sql);die();
			$resultado = DbLink::resultados($sql,$db);
//var_dump($resultado);
			if($resultado !== false){
		  	if(array_search(1, $resultado[0])) $data['tipo'] = "UPDATE";
		  	if(array_search(2, $resultado[0])) $data['tipo'] = "INSERT";
		  	$data['estatus'] = '1';
		  }else{
		  	$data['tipo'] = null;
		  	$data['mensaje'] ='No requiere actualización';
		  	$data['estatus'] ='0';

		  }
		}

		if(!empty($errores)){$data['estatus'] = '0';$data['error']  = $errores;}
		if(strpos("$_SERVER[REQUEST_URI]", __CLASS__."/".__FUNCTION__) !== false) return entregar($data); else return $data;
	}

	/**
	 * formatDBColum
	 * 
	 * @return string 'valor' para arreglos y fechas y sin comillas para números
	 */
	public static function formatDBColum($dataTable, $campo, $valor){
	  $numerico = array('bigint','integer','decimal','smallint','numeric','real','double precision','smallserial','serial','bigserial','money');
	  $texto = array('character varying','varchar','character','char','text','bytea','timestamp','date','time','interval','boolean','enum','json','jsonb','xml');
	  if (in_array($dataTable['columnas'][$campo]['data_type'], $numerico)) {
	    if(is_numeric((0 + $valor))){
	      $result = 0+$valor;
	    }elseif($valor == "0" || $valor == 0 || $valor == 0.00){
	      $result = 0;
	    }elseif($valor."" ==""){
	      $result = null;
	    } else $result = 0.00;
	  }else{
	  	$chrln = $dataTable['columnas'][$campo]['character_maximum_length'];
			if($chrln !==null && $chrln<strlen($valor)) $valor = substr($valor, 0, $chrln);
	    $valor = pg_escape_string($valor);
			if($campo == 'fecha_modificacion') $valor = date("Y-m-d H:i:s");
	    $result = "'{$valor}'";
	  }
	  return $result;
	}

	/**
	 * loadDatasTable
	 * 
	 * @return string 'valor' para arreglos y fechas y sin comillas para números
	 */
	public static function loadDatasTable($datos=null, $table=null, $ref=null, $db='BD'){
		$data = array();
		$errores  = array();

		if(empty($datos)) $datos 	= traerValorDe("datos");
		if(empty($datos)) $errores['datos'] = $data['mensaje'] ='faltan los datos';
		if(empty($table)) $table 	= traerValorDe("table");
		if(empty($table)) $errores['tabla'] = $data['mensaje'] ='falta la tabla a cargar';
		if(empty($ref)) $ref 	= traerValorDe("ref");

		$data['actualizados'] = 0;
		$data['agregados'] = 0;
		$data['sin_accion'] = 0;
		if(empty($errores)){
		  foreach ($datos as $registro){
		    $temp = self::loadData($registro, $table, $ref, $db);
//var_dump($temp);
		    if(!empty($temp['tipo'])){
		    	if 		($temp['tipo']=="UPDATE") $data['actualizados'] += 1;
		    	elseif($temp['tipo']=="INSERT") $data['agregados'] += 1;
		    	else 														$data['sin_accion'] += 1;
		    }
		  }
		  if($data['actualizados']+$data['agregados']+$data['sin_accion'] == 0){
		  	$data['estatus'] = '0';
		  }else{
		  	$data['estatus'] = '1';
		  }
		}

		if(!empty($errores)){$data['estatus'] = '0';$data['error']  = $errores;}
		if(strpos("$_SERVER[REQUEST_URI]", __CLASS__."/".__FUNCTION__) !== false) return entregar($data); else return $data;
	}


// fin clase bd
}


