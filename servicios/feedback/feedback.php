<?php 
require_once $_SERVER["DOCUMENT_ROOT"] ."/servicios/correo/correo.php";

class feedback {

	public static function test(){
		echo date('h:i:s') . "<br>";
		//sleep for 5 seconds
		usleep(5000000);
		//start again
		echo date('h:i:s');
	}

	/**
	 * accesoDef
	 * valores por defecto de acceso a cada método.
	 * @param string $metodo el nombre del método a validar 
	 * @return int nivel  de acceso
	 */
	public static function accesoDef($metodo){
		switch ($metodo) {
		    case "getBD": 			return 1; break;
		    case "total": 			return 1; break;
		    case "analyze": 		return 1; break;
		    case "accesoDef": 	return 1; break;
		    case "getDato": 		return 1; break;
		    case "getCron": 		return 1; break;
		    case "data": 				return 2; break;
		    default: 						return 1; 
		}
	}

	public static function data($data=null,$img=null){
		$datos = array();
		global $conf;
		if(empty($data)) $data = traerValorDe("data");
		if(empty($data)) $errores['msj'] = $data['mensaje'] = 'Falta el mensaje';
		$msj = $data;
		if(empty($img)) $img = traerValorDe("img");

		if(empty($errores)){
			if(empty($_SESSION['usuario']['correo'])){
				$envia = $conf['usuarios']['super_usuario'];
			}else{
				$envia = $_SESSION['usuario']['correo'];
			}
			
			$datos['para'] = $conf['usuarios']['super_usuario'];
			$datos['titulo'] = "WS - feedback de $envia";
			$datos['cuerpoplano'] = $msj;
			if(!empty($img)){
				$datos['archivos'][0]['binario'] 	= base64_decode(str_replace("data:image/png;base64,", "", $img));
				$datos['archivos'][0]['nombre'] 	= "captura.png";
			}
			$datos = correo::enviar($datos);
		}else{
			$datos['estatus'] 		= '0';
			$datos['error']  = $errores;
		}
		if(strpos("$_SERVER[REQUEST_URI]", __CLASS__."/".__FUNCTION__) !== false) return entregar($datos); else return $datos;
	}	

// fin clase feedback
}
