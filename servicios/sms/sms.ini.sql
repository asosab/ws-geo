
CREATE TABLE sms_get
(
	id									serial NOT NULL,
  de                	character varying(15) NOT NULL,
  para               	character varying(15) NOT NULL,
  msj                	text NOT NULL,
  fecha								timestamp NOT NULL,
  msjid               character varying(40) NOT NULL,
  deviceid            character varying(40) NOT NULL,
  CONSTRAINT sms_get_id_pkey PRIMARY KEY (id)
)
WITH (OIDS=FALSE);
ALTER TABLE sms_get OWNER TO postgres;
GRANT ALL ON TABLE sms_get TO postgres;
GRANT ALL ON TABLE sms_get TO data;
COMMENT ON TABLE sms_get IS 'aquí se guardan los mensajes recibidos';

CREATE TABLE sms_snd
(
	id									serial NOT NULL,
  para               	character varying(15) NOT NULL,
  msj                	text NOT NULL,
  fecha_creacion			timestamp NOT NULL,
  fecha_envio					timestamp,
  enviado 						boolean,
  CONSTRAINT sms_snd_id_pkey PRIMARY KEY (id)
)
WITH (OIDS=FALSE);
ALTER TABLE sms_snd OWNER TO postgres;
GRANT ALL ON TABLE sms_snd TO postgres;
GRANT ALL ON TABLE sms_snd TO data;
COMMENT ON TABLE sms_snd IS 'aquí se guardan los mensajes enviados';

CREATE TABLE sms_tlfcor
(
	telefono						character varying(15) NOT NULL,
  correo 							character varying(100) NOT NULL,
  CONSTRAINT sms_tlfcor_pkey PRIMARY KEY (telefono,correo)
)
WITH (OIDS=FALSE);
ALTER TABLE sms_tlfcor OWNER TO postgres;
GRANT ALL ON TABLE sms_tlfcor TO postgres;
GRANT ALL ON TABLE sms_tlfcor TO data;
COMMENT ON TABLE sms_tlfcor IS 'aquí se guardan los vínculos entre números telefónicos y cuentas de correo';

CREATE TABLE sms_operador
(
	telefono						character varying(15) NOT NULL,
  activo 							boolean NOT NULL,
  totalenvios					integer NOT NULL,
  fecha_modificacion timestamp without time zone,
  CONSTRAINT sms_operador_telefono_pkey PRIMARY KEY (telefono)
)
WITH (OIDS=FALSE);
ALTER TABLE sms_operador OWNER TO postgres;
GRANT ALL ON TABLE sms_operador TO postgres;
GRANT ALL ON TABLE sms_operador TO data;
COMMENT ON TABLE sms_operador IS 'Lista de aparatos telefónicos comunicados con el servidor y su último estado';