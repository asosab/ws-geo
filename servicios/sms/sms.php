<?php 

require_once $_SERVER["DOCUMENT_ROOT"] ."/servicios/contactos/contactos.php";

/*
	
	Se requiere instalar este apk en celulares android para recibir y enviar mensajes:
	https://f-droid.org/repo/org.addhen.smssync_26.apk
*/

class sms {

	/**
	 * accesoDef
	 * valores por defecto de acceso a cada método.
	 * @param string $metodo el nombre del método a validar 
	 * @return int nivel  de acceso
	 */
	public static function accesoDef($metodo){
		switch ($metodo) {
		    case "getBD": 			return 1; break;
		    case "total": 			return 1; break;
		    case "analyze": 		return 1; break;
		    case "accesoDef": 	return 1; break;
		    case "getDato": 		return 1; break;
		    case "getCron": 		return 1; break;
		    case "test": 				return 1; break;
		    case "toFile": 			return 1; break;
		    case "responder": 	return 1; break;
		    case "get": 				return 3; break;

		    default: 						return 1; 
		}
	}

	/**
	 * getBD
	 * retorna la base de datos en uso.
	 * @return string con nombre de la bd
	 */
	public static function getBD($dar='BD'){
			if($dar=='BD') return "BD";
			if($dar=='Tb') return array('sms_get','sms_snd','sms_operador','sms_tlfcor');
	}

	/**
	 * clave
	 * retorna la clave.
	 * @return string con la clave
	 */
	public static function clave(){
		return "1qazxsw2";
	}

	public static function test(){
    if (strpos($_SERVER['REQUEST_URI'], 'task=send') !== false) {
		  $json = file_get_contents('php://input');
		  echo "send";
		  print_r($json);
		}
	}

	/**
	 * total
	 * Total registros relacionados
	 * @return array de datos de persona
	 */
	public static function total(){
		$data = array();
		foreach (rnc::getBD('Tb') as $tabla) $data[$tabla] = totalRegBDTable($tabla, rnc::getBD());
		if(strpos("$_SERVER[REQUEST_URI]", __FUNCTION__) !== false)return entregar($data); else return $data;
	}

	/**
	 * analyze
	 * Actualiza las estadísticas de cada tabla
	 * @return array de datos de persona
	 */
	public static function analyze(){
		$data = array();
		foreach (rnc::getBD('Tb') as $tabla) $data[$tabla] = analyzeBDTable($tabla, rnc::getBD());
		if(strpos("$_SERVER[REQUEST_URI]", __FUNCTION__) !== false)return entregar($data); else return $data;
	}

	/**
	 * toBD
	 * 
	 * @return array 
	 */
	public static function toBD($msj){
		$ref = [
			"from" => "de",
			"sent_to" => "para",
			"message" => "msj",
			"sent_timestamp" => "fecha",
			"message_id" => "msjid",
			"device_id" => "deviceid"
		];
		$tbl = self::getBD('Tb')[0];
//var_dump($msj);die();
		$set = bd::loadData($msj, $tbl, $ref, self::getBD());

		return $set;
	}

	/**
	 * toFile
	 * 
	 * @return array de datos de persona
	 */
	public static function toFile($toFile=null){
		if(empty($toFile)) $toFile	= traerValorDe("toFile");
		if(empty($toFile)) $toFile	= "sin data";

		$archivo =  $_SERVER["DOCUMENT_ROOT"]."/privado/archivos/".date('YmdHis').".txt";
		return file_put_contents($archivo, $toFile);
	}

	/**
	 * responder
	 * @return array de datos de persona
	 */
	public static function responder($respuesta){
    header("Cache-Control: no-cache, must-revalidate"); 
    header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); 
    header("Content-type: application/json; charset=utf-8");
    echo json_encode($respuesta);
	}


	/**
	 * getNumberByKey
	 * @return string apikey
	 */
	public static function getNumberByKey($getNumberByKey=null){
		$data = [];
		$errores = [];
		if(empty($getNumberByKey)) $getNumberByKey = traerValorDe("getNumberByKey");
		if(empty($getNumberByKey)) $errores['llave'] = $data['mensaje'] ='falta la llave';
		$apikey = $getNumberByKey;

		if(empty($errores)){
			$sql = "SELECT correo FROM ws_usuarios WHERE apikey = '$apikey'";
			$resultado = DbLink::resultados($sql,self::getBD());
			if($resultado !==false){
				$data['correo'] = $resultado[0]['correo'];
			}else{
				$errores['apikey'] = $data['mensaje'] ='error de llave';
			}
		}

		if(empty($errores)){
			$persona = self::numerosDe($data['correo']);
			if(empty($persona['numeros'])) $errores['numeros'] = $data['mensaje'] ='No se encontraron números relacionados';
		}
		
		if(empty($errores)){
			$data['numeros'] = $persona['numeros'];
			$data['estatus'] = '1';
		}

		if(!empty($errores)){$data['estatus'] = '0'; $data['error']  = $errores;}
		if(strpos("$_SERVER[REQUEST_URI]", __CLASS__."/".__FUNCTION__) !== false) return entregar($data); else return $data;
	}

	/**
	 * getKeyByNumber
	 * @return string apikey
	 */
/*
	public static function getKeyByNumber($getKeyByNumber=null){
		$data = [];
		$errores = [];
		if(empty($getKeyByNumber)) $getKeyByNumber = traerValorDe("getKeyByNumber");
		if(empty($getKeyByNumber)) $errores['llave'] = $data['mensaje'] ='falta la llave';
		$numero = $getKeyByNumber;

		if(empty($errores)){
			$persona = contactos::buscar($numero);
			if(empty($persona['EMAIL'])) $errores['correo'] = $data['mensaje'] ='No se encontró en la agenda';
		}
		
		if(empty($errores)){
			$correos = "";
			foreach ($persona['EMAIL'] as $key => $value) {
				if(!empty($value['Value'])) $correos .= " " . $value['Value'];
			}

			$sql = "SELECT apikey FROM ws_usuarios WHERE '$correos' LIKE '%'||correo||'%'";
			$resultado = DbLink::resultados($sql,self::getBD());
			if($resultado !==false){
				$data['apikey'] = $resultado[0]['apikey'];
				$data['estatus'] = '1';
			}else{
				$errores['correo'] = $data['mensaje'] ='No se encontró entre los usuarios';
			}
		}

		if(!empty($errores)){$data['estatus'] = '0'; $data['error']  = $errores;}
		if(strpos("$_SERVER[REQUEST_URI]", __CLASS__."/".__FUNCTION__) !== false) return entregar($data); else return $data;
	}
*/
	/**
	 * getSomeMsj
	 * @return array 
	 */
	public static function getSomeMsj($getSomeMsj=null){
		$data = [];
		if(empty($getSomeMsj)) $getSomeMsj = traerValorDe("getSomeMsj");
		if(empty($getSomeMsj)) $getSomeMsj = 3;
		$tbl = self::getBD('Tb')[1];
		$sql = "SELECT * FROM $tbl WHERE fecha_envio < now() AND (enviado IS NULL OR enviado = 'f') ORDER BY fecha_creacion DESC limit $getSomeMsj";
//    $sql = "SELECT * FROM $tbl WHERE fecha_envio IS NULL ORDER BY fecha_creacion DESC limit $getSomeMsj";
    $resultado = DbLink::resultados($sql,acceso::getBD());
    if($resultado !==false){
			foreach ($resultado as $key => $value) {
				$data[] = [
					"to" => $value['para'], 
					"message" => $value['msj'], 
					"uuid" => $value['id']
				];
				bd::loadData(["id"=>$value['id'], "enviado"=>'t'],$tbl,null,self::getBD());
			}

		}
		if(!empty($errores)){$data['estatus'] = '0'; $data['error']  = $errores;}
		if(strpos("$_SERVER[REQUEST_URI]", __CLASS__."/".__FUNCTION__) !== false) return entregar($data); else return $data;
	}

	/**
	 * numerosDe
	 * Relación de direcciones de corrreo y números telefónicos vinculados a una frase dada, buscada entre los contactos
	 * @param string $mailNumber una frase a buscar entre contactos
	 * @return array de datos de persona
	 */
	public static function numerosDe($numerosDe=null){
		$data = [];
		$errores = [];
		if(empty($numerosDe)) $numerosDe = traerValorDe("numerosDe");
		if(empty($numerosDe)) $errores['input'] = $data['mensaje'] ='falta un correo o un número telefónico';

		if(empty($errores)){ 
			$contactos = contactos::buscar($numerosDe);

			if($contactos['estatus']!=='0' && !empty($contactos['N'])){
				$data['contactos'][] = ["id" => $contactos['id'],"nombre" => $contactos['FN'][0]];
				if(!empty($contactos['TEL'])){
					foreach ($contactos['TEL'] as $lTEL => $vTEL) {
						if(ctype_digit(strval($lTEL))){ // evita los tipos "old"
							if(!empty($vTEL['Value'])){
								$dataTel = self::celNumber($vTEL['Value']);
								if($dataTel['tipo']=="celular") $data['numeros'][] = $dataTel['format'];
							}
						}
					}
				}
				if(!empty($contactos['EMAIL'])){
					foreach ($contactos['EMAIL'] as $lEMAIL => $vEMAIL) {
						if(ctype_digit(strval($lEMAIL))){
							if(!empty($vEMAIL['Value'])){
								$data['correos'][] = $vEMAIL['Value'];
							}
						}
					}
				}
			}elseif($contactos['estatus']!=='0' && empty($contactos['N'])){
				foreach ($contactos as $key => $value) {
					if(empty($value['id'])) break;
					$contacto = contactos::getContacto($value['id']);
					$data['contactos'][] = ["id" => $contacto['id'], "nombre" => $contacto['FN'][0]];
					if(!empty($contacto['TEL'])){
						foreach ($contacto['TEL'] as $lTEL => $vTEL) {
							if(ctype_digit(strval($lTEL))){ // evita los tipos "old"
								if(!empty($vTEL['Value'])){
									$dataTel = self::celNumber($vTEL['Value']);
									if($dataTel['tipo']=="celular") $data['numeros'][] = $dataTel['format'];
								}
							}
						}
					}
					if(!empty($contacto['EMAIL'])){
						foreach ($contacto['EMAIL'] as $lEMAIL => $vEMAIL) {
							if(ctype_digit(strval($lEMAIL))){
								if(!empty($vEMAIL['Value'])) $data['correos'][] = $vEMAIL['Value'];
							}
						}
					}
				}	
			}else{
				$errores['destinatario'] = $data['mensaje'] ='falta el destinatario';
			}
		}

		if(empty($errores)){
			if(!empty($data['numeros'])) $data['numeros'] = array_unique($data['numeros']);
			if(!empty($data['correos'])) $data['correos'] = array_unique($data['correos']);
		}

		if(!empty($errores)){$data['estatus'] = '0'; $data['error']  = $errores;}
		if(strpos("$_SERVER[REQUEST_URI]", __CLASS__."/".__FUNCTION__) !== false) return entregar($data); else return $data;
	}

	/**
	 * celNumber
	 * Validar el numero telefónico
	 * @param string $validNumber un correo o un número celular
	 * @return array de datos de persona
	 */
	public static function celNumber($celNumber=null){
		$data = [];
		$errores = [];
		if(empty($celNumber)) $celNumber = traerValorDe("celNumber");
		if(empty($celNumber)) $errores['input'] = $data['mensaje'] ='falta el número celular';

		if(empty($errores)){ 
			$phoneUtil = \libphonenumber\PhoneNumberUtil::getInstance();
			$carrierMapper = \libphonenumber\PhoneNumberToCarrierMapper::getInstance();
			$timeZoneMapper = \libphonenumber\PhoneNumberToTimeZonesMapper::getInstance();
			$geocoder = \libphonenumber\geocoding\PhoneNumberOfflineGeocoder::getInstance();
			$shortNumberInfo = \libphonenumber\ShortNumberInfo::getInstance();
			try {
			  $nProto = $phoneUtil->parse($celNumber, "VE");
			  $data['valido'] = $phoneUtil->isValidNumber($nProto);
			  $data['numerocorto'] = $shortNumberInfo->isValidShortNumber($nProto, "VE");
			  if($data['valido']){
			  	$data['tipo'] = $phoneUtil->getNumberType($nProto);
			  	$data['tipo'] = ($data['tipo']==0)?"fijo":"celular";
				  $data['format'] = $phoneUtil->format($nProto, \libphonenumber\PhoneNumberFormat::E164);
				  $data['geocod'] = $geocoder->getDescriptionForNumber($nProto, "es_ES");
				  $data['carrier'] = $carrierMapper->getNameForNumber($nProto, "es");
				  if(empty($data['carrier'])) $data['carrier'] = "CANTV";
				  $data['timezone'] = $timeZoneMapper->getTimeZonesForNumber($nProto)[0];
			  }elseif($data['numerocorto']){
			  	$data['emergencia'] = $shortNumberInfo->connectsToEmergencyNumber($nProto, "VE");
			  }
			} catch (\libphonenumber\NumberParseException $e) {
			    $errores['input'] = $data['mensaje'] =$e;
			}
		}

		if(!empty($errores)){$data['estatus'] = '0'; $data['error']  = $errores;}
		if(strpos("$_SERVER[REQUEST_URI]", __CLASS__."/".__FUNCTION__) !== false) return entregar($data); else return $data;
	}

	/**
	 * nuevoSMS
	 * Crea un mensaje y lo agrega a la cola de envío
	 * @param string $send mensaje a enviar
	 * @param string $numero número al cual enviar el mensaje. de estar vacío el mensaje se envía a quien lo envía
	 * @return array de datos de persona
	 */
	public static function nuevoSMS($nuevoSMS=null, $numero=null, $send=null, $cuando=null){
		$data = [];
		$errores = [];
		if(empty($nuevoSMS)) $nuevoSMS = traerValorDe("nuevoSMS");
		if(empty($nuevoSMS)) $errores['msj'] = $data['mensaje'] ='no hay mensaje';
		$msj = $nuevoSMS;
		if(empty($numero)) $numero = traerValorDe("numero");
		if(empty($send)) $send = traerValorDe("send");
		if(empty($send)) $send = false;
		if(empty($cuando)) $cuando = traerValorDe("cuando");
		if(empty($cuando)) $cuando = date("Y-m-d H:i:s");

		if(empty($errores)){ // verificar usuario que recibe
			if(empty($numero)){
				$persona = acceso::getSesion();
				if($persona['estatus']=='1'){
					$data = self::numerosDe($persona['correo']);
				}else{
					$errores['para'] = $data['mensaje'] ='no hay destinatario';
				}
			}else{
				$data = self::numerosDe($numero);
			}
		}

		if(empty($errores)){ // guardar mensaje en la cola de envío
			$data['msj'] = $msj;
			$data['enviar'] = $send;
			$data['fecha_envio'] = $cuando;

			if($send !==false && !empty($data['numeros'])){
				$load = [];
				foreach ($data['numeros'] as $key => $value) {
					$load[] = [
						'para' 						=> $value, 
						'msj' 						=> $data['msj'],
						'fecha_creacion' 	=> date("Y-m-d H:i:s"),
						'fecha_envio' 		=> $data['fecha_envio']
					];
				}
				$data['set'] = bd::loadDatasTable($load, self::getBD('Tb')[1], null, self::getBD());
			}
		}
		if(!empty($errores)){$data['estatus'] = '0'; $data['error']  = $errores;}
		if(strpos("$_SERVER[REQUEST_URI]", __CLASS__."/".__FUNCTION__) !== false) return entregar($data); else return $data;
	}

	/**
	 * get
	 * Recibe sms
	 * @return array datos de empresa según INCES
	 */
	public static function get($from=null,$message=null,$secret=null,$sent_timestamp=null,$sent_to=null,$message_id=null,$device_id=null, $task=null){
		$data=["payload"=>["success"=>false,"error"=>file_get_contents('php://input')]];
    if(empty($from)) $from 											= traerValorDe("from");
    if(empty($message)) $message 								= traerValorDe("message");
    if(empty($secret)) $secret 									= traerValorDe("secret");
    if(empty($sent_timestamp)) $sent_timestamp 	= traerValorDe("sent_timestamp");
    if(empty($sent_to)) $sent_to 								= traerValorDe("sent_to");
    if(empty($message_id)) $message_id 					= traerValorDe("message_id");
    if(empty($device_id)) $device_id 						= traerValorDe("device_id");
    if(empty($task)) $task 											= traerValorDe("task");
    $msj = compact('from','message','secret','sent_timestamp','sent_to','message_id','device_id');

    $json = file_get_contents('php://input');
    $uri = $_SERVER['REQUEST_URI'];

    $arrive = json_decode($json, true);

    if(!empty($from) && empty($secret)){ // mensaje entrante sin clave
			$data['payload']['success'] = false;
    	$data['payload']['error'] = 'No hay clave';
    }elseif( !empty($from) 
			    && !empty($secret) 
			    && !empty($message) 
			    && !empty($sent_timestamp) 
			    && !empty($message_id) ){ // mensaje entrante con clave
    	if($secret !== self::clave()){
    		$data['payload']['success'] = false;
    		$data['payload']['error'] = 'Clave incorrecta';
    	}else{
    		$data['payload']['success'] = true;
    		$data['payload']['error'] = null;
    		$msj['sent_timestamp'] = gmdate("Y-m-d\TH:i:s\Z", $sent_timestamp);
    		self::toBD($msj);
    		self::acciones(json_encode($msj));
    	}
    }elseif(strpos($uri, 'secret='.self::clave()) !== false) { // petición de tareas
			$data['payload']['success'] = true;
    	$data['payload']['error'] = null;
		}
		
		if(!empty($arrive['queued_messages'])){
			$data['payload']['success'] = true;
    	$data['payload']['error'] = null;
    	$data['message_uuids'] = $arrive['queued_messages'];
    }

    if($data['payload']['success']){
	    $mensajes = self::getSomeMsj(10);
	    if(count($mensajes)>0){
	    	$data['payload']["task"] = "send";
	    	$data['payload']["secret"] = self::clave();
	    	$data['payload']["messages"] = $mensajes;
	    }
    }

    $resp = json_encode($data);
//		self::toFile("uri:'".$uri."'\ninput:'".$json."' \nresp:".$resp);
	  self::responder($data);
	}

	/**
	 * acciones
	 * toma acciones según la primera palabra alfanumérica del mensaje y de quién lo envía
	 * @param string $acciones json del mensaje recibido
	 * @return array 
	 */
	public static function acciones($acciones=null){
		$data = [];
		$errores = [];
		if(empty($acciones)) $acciones = traerValorDe("acciones");
		if(empty($acciones)) $errores['msj'] = $data['mensaje'] ='Falta información del mensaje';
		if(empty($errores)){
			$msj = json_decode($acciones,true);
			if(empty($msj['from'])) $errores['from'] = $data['mensaje'] ='Falta el remitente';
			if(empty($msj['message'])) $errores['msj'] = $data['mensaje'] ='Falta el mensaje';
			if(!empty($msj['from'])) $msj['from'] = substr($msj['from'], -10);
		}

		if(empty($errores)){
			$palabras = preg_split('/[^\w]/',$msj['message']);
			$orden = strtolower($palabras[0]);
			$submensaje = substr($msj['message'], strlen($orden)+1);
			$submensaje = trim($submensaje);
			$faccion = $_SERVER["DOCUMENT_ROOT"]."/servicios/sms/acciones/{$orden}.php";
  		if(is_file($faccion)){
  			require_once $faccion;
	  		if (class_exists($orden)){
	  			if (method_exists($orden, 'send')){
	  				$data['send'] = $orden::send($submensaje,$msj['from']);
	  			}
	  		}
  		}else $errores['msj'] = $data['mensaje'] ='orden no encontrada';
		}
//		$resp = json_encode($data);
//		self::toFile("accion:'".$msj['message']."' \nresp:". $resp);
		if(!empty($errores)){$data['estatus'] = '0'; $data['error']  = $errores;}
		if(strpos("$_SERVER[REQUEST_URI]", __CLASS__."/".__FUNCTION__) !== false) return entregar($data); else return $data;
	}


/*



curl -D - -X GET https://asosab.sytes.net/sms/get/?task=send


*/

// fin clase sms
}

