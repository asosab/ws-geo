<?php

require_once $_SERVER["DOCUMENT_ROOT"] ."/servicios/rastrear/rastrear.php";

class host {
	public static function send($msj=null,$from=null){
		if(empty($msj)||empty($from)) return false;
		//if(from !== '4261214789') return false;
		if($msj == "help") return sms::nuevoSMS(self::help(), $from, 1);
		$persona = sms::numerosDe($from);
		$correo = $persona['correos'][0];

		$data = rastrear::get($msj);

		$json_string = json_encode($data['host'], JSON_PRETTY_PRINT);
/*
		$dom = dom_import_simplexml($data)->ownerDocument;
		$dom->formatOutput = TRUE;
		$formatted = $dom->saveXML();
*/
//print_r($data);die;
		$hosts = 0;
		$puertos = "''";
		if(!empty($data['runstats']['hosts']['@attributes']['up'])){
			$hosts = $data['runstats']['hosts']['@attributes']['up'];
			if($hosts==1){
				$lista = [];
				if(!empty($data['host']['ports']['port'])){
					foreach ($data['host']['ports']['port'] as $key => $puerto) {
						if($puerto['state']['@attributes']['state'] =="open"){
							$lista[] = $puerto['@attributes']['portid'];
						}
					}
					$puertos = implode(", ", $lista);
				} 
			}elseif($hosts>1){
				$puertos = "Informe en su correo";
			}else{
				$puertos = '0';
			}
		} 



		$mensaje = "$msj hosts=$hosts, puertos='$puertos'";

		$carta = [];
		$carta['para'] = $correo;
		$carta['titulo'] = "Escaneo de $msj";
		$carta['cuerpoplano'] = (string)$mensaje . "\n\r" . (string)$json_string;
		$data['mail'] = correo::enviar($carta);

		return sms::nuevoSMS($mensaje, $from, 1);
	}

	public static function test(){
		return 'curl -D - -X POST https://asosab.sytes.net/sms/get/ -F "from=+584261214789" -F "secret=1qazxsw2" -F "message=host inces.gob.ve" -F "device_id=1" -F "sent_timestamp=1472572997" -F "message_id=80" -F "sent_to=04261214789"';
	}

	public static function help(){
		return "host ###.###.###.### -indica el estado de un equipo y sus servicios";
	}

	public static function hosts(){
		return [
			"192.168.1.1/24"
		];
	}
}
