<?php

class info {
	public static function send($msj=null,$from=null){
		if(empty($from)) return false;
		//if(from !== '4261214789') return false;
		$data = [];
		if($msj == "help") return sms::nuevoSMS(self::help(), $from, 1);
		$acciones = self::acciones();
		$mensaje = "Lista de acciones sms:\r\n $acciones";
		if(!empty($acciones)){
			$data['sms'] = sms::nuevoSMS($mensaje, $from, 1);

			$persona = sms::numerosDe($from);
			$carta = [];
			$carta['para'] = $persona['correos'][0];
			$carta['titulo'] = "Lista de comandos sms";
			$carta['cuerpoplano'] = $mensaje;
			$data['mail'] = correo::enviar($carta);
			return $data;
		}else return false;
	}

	public static function test(){
		return 'curl -D - -X POST https://asosab.sytes.net/sms/get/ -F "from=+584261214789" -F "secret=1qazxsw2" -F "message=info" -F "device_id=1" -F "sent_timestamp=1472572997" -F "message_id=80" -F "sent_to=04261214789"';
	}

	public static function help(){
		return "info (no hace falta escribir nada más) - retorna la lista de acciones que puede ejecutar este servicio de sms, cómo llamarlas y su utilidad";
	}

	public static function acciones(){
		$lista = dirToArray($_SERVER["DOCUMENT_ROOT"]."/servicios/sms/acciones/");
		$listado = "";
		foreach ($lista as $key => $archivo) {
			$faccion = $_SERVER["DOCUMENT_ROOT"]."/servicios/sms/acciones/{$archivo}";
  		if(is_file($faccion)){
  			$orden = str_replace(".php", "", $archivo);
  			require_once $faccion;
	  		if (class_exists($orden)){
	  			if (method_exists($orden, 'help')){
	  				$listado .= "* ". $orden::help();
	  				$listado .= "\r\n";
	  			}
	  		}
  		}
		}
		return $listado;
	}
}
