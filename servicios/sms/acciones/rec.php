<?php 

class rec {
	public static function send($msj=null,$from=null){
		if(empty($msj)||empty($from)) return false;
		//if(from !== '4261214789') return false;
		if($msj == "help") return sms::nuevoSMS(self::help(), $from, 1);
		$pos = strpos($msj, "/");
		$fecha = substr($msj, 0, $pos);
		$mensaje = substr($msj, $pos+1);
		if (DateTime::createFromFormat('Y-m-d G:i:s', $fecha) !== FALSE) {
			sms::nuevoSMS("Confirmamos que en la fecha $fecha te llegará el mensaje '$mensaje'", $from, 1);
		  return sms::nuevoSMS($mensaje, $from, 1,$fecha);
		}else return false;
	}

	public static function test(){
		return 'curl -D - -X POST https://asosab.sytes.net/sms/get/ -F "from=+584261214789" -F "secret=1qazxsw2" -F "message=rec 2016-09-12 08:35:00/¡Buenos días!" -F "device_id=1" -F "sent_timestamp=1472572997" -F "message_id=80" -F "sent_to=04261214789"';
	}

	public static function help(){
		$fecha = date("Y-m-d H:i:s");
		return "rec $fecha/TEXO A ENTREGAR-Crea un mensaje sms para ser enviado en la fecha determinada";
	}

}
