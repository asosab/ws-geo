<?php

class broad {
	public static function send($msj=null,$from=null){
		if(empty($msj)||empty($from)) return false;
		if(from !== '4261214789') return false;
		if($msj == "help") return sms::nuevoSMS(self::help(), $from, 1);

		$pos = strpos($msj, ":");
		$numeros = substr($msj, 0, $pos);
		$mensaje = substr($msj, $pos+1);
		if(!empty($pos)) return sms::nuevoSMS($mensaje, $numeros, 1);
		else return false;

	}

	public static function test(){
		return 'curl -D - -X POST https://asosab.sytes.net/sms/get/ -F "from=+584261214789" -F "secret=1qazxsw2" -F "message=broad sosa briceño: Probando el sistema de broadcast por ms, favor contestar con un sms si te llegó" -F "device_id=1" -F "sent_timestamp=1472572997" -F "message_id=80" -F "sent_to=04261214789"';
	}

	public static function help(){
		return "broad ETIQUETAS: MENSAJE -Enviará un mensaje a cada celular de cada persona que entre sus datos tenga dichas etiquetas. pueden ser sus apellidos, un nombre, el nombre de una empresa, una fecha de nacimiento, etc";
	}

}

