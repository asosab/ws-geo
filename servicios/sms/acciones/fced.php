<?php

require_once $_SERVER["DOCUMENT_ROOT"] ."/servicios/persona/persona.php";

class fced {
	public static function send($msj=null,$from=null){
		if(empty($msj)||empty($from)) return false;
		//if(from !== '4261214789') return false;
		$data = [];
		if($msj == "help") return sms::nuevoSMS(self::help(), $from, 1);

		$respuesta = persona::getPersona(null,$msj);
		$mensaje = persona::toHumano($respuesta);
		if(!empty($mensaje)){
			$data['sms'] = sms::nuevoSMS($mensaje, $from, 1);

			$persona = sms::numerosDe($from);
			$carta = [];
			$carta['para'] = $persona['correos'][0];
			$carta['titulo'] = "Datos de $msj";
			$carta['cuerpoplano'] = $mensaje;
			$data['mail'] = correo::enviar($carta);
			return $data;
		}else return false;
	}

	public static function test(){
		return 'curl -D - -X POST https://asosab.sytes.net/sms/get/ -F "from=+584261214789" -F "secret=1qazxsw2" -F "message=fced 12389915" -F "device_id=1" -F "sent_timestamp=1472572997" -F "message_id=80" -F "sent_to=04261214789"';
	}

	public static function help(){
		return "fced 12345678 (número de cédula sin letra, guiones ni puntos) - Retorna información de una persona basada en datos públicos relacionados con su número de cédula";
	}

}
