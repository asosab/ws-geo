<?php 

require_once $_SERVER["DOCUMENT_ROOT"] ."/servicios/wikipedia/wikipedia.php";

class wp {
	public static function send($msj=null,$from=null){
		if(empty($msj)||empty($from)) return false;
		//if(from !== '4261214789') return false;
		if($msj == "help") return sms::nuevoSMS(self::help(), $from, 1);

		$respuesta = wikipedia::consultar($msj);
		if(!empty($respuesta[2][0])) return sms::nuevoSMS($respuesta[2][0], $from, 1);
		else return false;
	}

	public static function test(){
		return 'curl -D - -X POST https://asosab.sytes.net/sms/get/ -F "from=+584261214789" -F "secret=1qazxsw2" -F "message=wp dinamarca" -F "device_id=1" -F "sent_timestamp=1472572997" -F "message_id=80" -F "sent_to=04261214789"';
	}

	public static function help(){
		return "wp SEGUIDO DEL CONCEPTO -Retorna un sms y un correo con la respuesta corta proveniente de la Wikipedia";
	}
}
