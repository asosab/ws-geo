<?php

class y2mp3 {
	public static function send($msj=null,$from=null){
		if(empty($msj)||empty($from)) return false;
		//if(from !== '4261214789') return false;
		if($msj == "help") return sms::nuevoSMS(self::help(), $from, 1);
		$persona = sms::numerosDe($from);
		$correo = $persona['correos'][0];

		exec("/bin/bash /home/asosab/linux/audio/ytubetomp3.sh $msj $correo > /dev/null 2>&1 &");
		return sms::nuevoSMS("El mp3 llegará pronto a $correo", $from, 1);
	}

	public static function test(){
		return 'curl -D - -X POST https://asosab.sytes.net/sms/get/ -F "from=+584261214789" -F "secret=1qazxsw2" -F "message=y2mp3 3T1c7GkzRQQ" -F "device_id=1" -F "sent_timestamp=1472572997" -F "message_id=80" -F "sent_to=04261214789"';
	}

	public static function help(){
		return "y2mp3 XXXXXXXXX -genera un archivo de audio mp3 a partir de un video de youtube cuyo ID sea XXXXXXXXX";
	}
}
