<?php 
require_once $_SERVER["DOCUMENT_ROOT"] ."/controlador/color.php";
require_once $_SERVER["DOCUMENT_ROOT"] . '/servicios/geo/geo.php';
//	require_once $_SERVER["DOCUMENT_ROOT"] . '/vendor/colors_of_image/colorsofimage.class.php';
//	if (!ini_get("auto_detect_line_endings")) ini_set("auto_detect_line_endings", '1');
//https://maps.google.com/maps/api/geocode/json?latlng=8.3650243,-62.6529962
//https://maps.googleapis.com/maps/api/elevation/json?locations=8.3650243,-62.6529962

	use League\ColorExtractor\Color;
	use League\ColorExtractor\ColorExtractor;
	use League\ColorExtractor\Palette;

class google {

	/**
	 * accesoDef
	 * valores por defecto de acceso a cada método.
	 * @param string $metodo el nombre del método a validar 
	 * @return int nivel  de acceso
	 */
	public static function accesoDef($metodo){
		switch ($metodo) {
		    case "getBD": 				return 1; break;
		    case "total": 				return 1; break;
		    case "analyze": 			return 1; break;
		    case "accesoDef": 		return 1; break;
		    case "buscar": 				return 1; break;
		    case "test": 					return 1; break;
		    case "oauth2callback":return 1; break;
		    default: 							return 1; 
		}
	}

	/**
	 * oauth2callback
	 * aqui llegan los que hhan dado alguna autorización
	 * @return 
	 */
	public static function oauth2callback(){

	}

	/**
	 * hexToName
	 * retorna el color más parecido dado un rgb
	 * @param string $rgb el color a comparar 
	 * @return string nombre del color
	 */
	public static function hexToName($hexToName=null){
		$colors = colors();
		if(empty($hexToName)) $hexToName = traerValorDe("hexToName");
		$rgb = $hexToName;
		$distances = array();
		$val = html2rgb($rgb);
		foreach ($colors as $name => $c) $distances[$name] = distancel2($c, $val);
		$mincolor = "";
		$minval = pow(2, 30); 
		foreach ($distances as $k => $v) {
	    if ($v < $minval) {
	      $minval = $v;
	      $mincolor = $k;
	    }
		}
		$data = $mincolor;
		if(strpos("$_SERVER[REQUEST_URI]", __CLASS__."/".__FUNCTION__) !== false)return entregar($data); else return $data;
	}

	/**
	 * cx
	 * valores por defecto de acceso a cada método.
	 * @param string $metodo el nombre del método a validar 
	 * @return int nivel  de acceso
	 */
	public static function cx($interes){
		switch ($interes) {
		    case "tsj": 		return "017656191504085686521:v98vgo2fl8a"; break;
		    case "social": 	return "017656191504085686521:rbyoljxxfsu"; break;
		    default: 				return false; 
		}
	}

	/**
	 * apykey
	 * valores por defecto de acceso a cada método.
	 * @param string $metodo el nombre del método a validar 
	 * @return int nivel  de acceso
	 */
	public static function apikey(){
		$apikey = "";
		$llaves = $_SERVER["DOCUMENT_ROOT"]."/privado/google";
		$apikey =  file($llaves, FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES)[0];
		return $apikey;
	}

	/**
	 * buscar
	 * valores por defecto de acceso a cada método.
	 * @param string $metodo el nombre del método a validar 
	 * @return int nivel  de acceso
	 */
	public static function buscar($data){
		$llaves = $_SERVER["DOCUMENT_ROOT"]."/privado/google";
		$apikey =  file($llaves, FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES)[0];
		$url = "https://www.googleapis.com/customsearch/v1?key=$apikey&". implode("&", $data);
		return $url;
	}

	/**
	 * img
	 * Información tomada de una imagen estática de google maps en base a una coordenada
	 */
	public static function img($img=null,$nocache=null,$zoom=null){
		$data = array();
		$apikey = self::apikey();
		if(empty($img)) 		$img 			= traerValorDe("img");
		if(empty($nocache)) $nocache 	= traerValorDe("nocache");
		if(empty($zoom)) 		$zoom 		= traerValorDe("zoom"); if(empty($zoom)) $zoom = 19;
		$coord = geo::geohash($img);
		$maptype = "hybrid"; //  "terrain";  //  "roadmap";  //   "satellite";  //  
		$agua = "style=feature:water|element:all|color:0x021019";
		$etiquetas = "style=feature:all|element:labels|visibility:off";
		$formato = "jpeg";
		$url3 = "https://maps.googleapis.com/maps/api/staticmap?center={$coord['coord']}&zoom=$zoom&scale=2&size=300x322&maptype=$maptype&format=$formato&$etiquetas&$agua&key=$apikey";
		$imgid = $coord['geohash'] . "_$zoom";
		$img = $_SERVER["DOCUMENT_ROOT"]."/image/google/$imgid.$formato";
		if($nocache){file_put_contents($img, file_get_contents($url3));} 
		else{if(!file_exists($img)) file_put_contents($img, file_get_contents($url3));};
		$data['coord'] = $coord['coord'];
		$data['zoom'] = $zoom;
		$data['url'] = "http://".$_SERVER['HTTP_HOST']."/image/google/$imgid.$formato";

		$image = new Imagick($img);
		$image->cropImage(600,600,0,0);
//		$image->equalizeImage();
//		$blackThreshold =0.2;
//		$whiteThreshold = 0.4;
//		$pixels = $image->getImageWidth() * $image->getImageHeight();
//    $image->linearStretchImage($blackThreshold * $pixels, $whiteThreshold * $pixels);
		$image->normalizeImage();
//		$image->sigmoidalcontrastimage(true, 5, 0.5 * Imagick::getQuantum());
		$image->writeImage($img);

		$palette = Palette::fromFilename($img);
		$top = $palette->getMostUsedColors(30);

		$arTmp = array();
		foreach($top as $color => $count) $arTmp[] = self::hexToName(Color::fromIntToHex($color));
		$arTmp = array_unique($arTmp);
		foreach($arTmp as $index => $color) $data['colores'][] = $color;

		if(in_array($data['colores'][0], array("blanco","negro","#020202"))){
			$data['buena'] = false;
		}else{
			$data['buena'] = true;
		}

		if(!$data['buena'] && $zoom>5) return self::img($coord['coord'],$nocache,$zoom-1);
		if(strpos("$_SERVER[REQUEST_URI]", __CLASS__."/".__FUNCTION__) !== false)return entregar($data); else return $data;
	}

	/**
	 * elev
	 * Retorna información sobre la altura de un punto geográfico.
	 * @param string $elev una representacion de lat y lon ejm: "8.3650243,-62.6529962" 
	 * @return array con datos sobre esa coordenada
	 */
	public static function elev($elev=null){
		$data = array();
		$apikey = self::apikey();

		if(empty($elev)) $elev = traerValorDe("elev");
		$coord = $elev;
		$url = "https://maps.googleapis.com/maps/api/elevation/json?locations=$coord&key=$apikey";
		$elev = json_decode(file_get_contents($url), true);
		$data['alt'] = $elev['results'][0]['elevation'];
		$data['res'] = $elev['results'][0]['resolution'];
		if(strpos("$_SERVER[REQUEST_URI]", __CLASS__."/".__FUNCTION__) !== false)return entregar($data); else return $data;
	}

	/**
	 * coord
	 * Retorna información sobre una coordenada.
	 * @param string $coord una representacion de lat y lon ejm: "8.3650243,-62.6529962" 
	 * @return array con datos sobre esa coordenada
	 */
	public static function coord($coord=null){
		$data = array();
		$apikey = self::apikey();

		if(empty($coord)) $coord = traerValorDe("coord");
		$url1 = "https://maps.google.com/maps/api/geocode/json?latlng=$coord&key=$apikey";

		$geo = json_decode(file_get_contents($url1), true);
		$data['nombre'] = $geo['results'][0]['formatted_address'];
		foreach ($geo['results'][0]['address_components'] as $key => $value) {
			$data['geo'][] = array(
				'nombre'  => $value['short_name'],
				'tipo' 		=> $value['types']
				);
		}
		$elev = self::elev($coord);
		$data['alt'] = $elev['alt'];
		$data['res'] = $elev['res'];
		$data['img'] = self::img($coord);
		
		if(strpos("$_SERVER[REQUEST_URI]", __CLASS__."/".__FUNCTION__) !== false)return entregar($data); else return $data;
	}

	/**
	 * persona
	 * valores por defecto de acceso a cada método.
	 * @param string $persona El arreglo de datos que representa a una persona 
	 * @return array con la lista de url's que apuntan a esa persona
	 */
	public static function persona($persona=null,$cx="social"){
		$data = array();
		$pagina = 1;
		if(empty($persona)) $persona = traerValorDe("persona");
		$persona = str_replace(" ", "+", $persona);
		$data[] = "start=$pagina&num=10";
		$data[] = "fields=items(title,link,snippet,pagemap(person))";
		$data[] = "q=$persona";
//		$data[] = "exactTerms=$persona";
		$data[] = "lr=lang_es";
		$data[] = "cx=" . google::cx($cx);
		$url = google::buscar($data);
		$data = json_decode(file_get_contents($url), true);
		return $data;
	}

		/**
	 * test
	 */
	public static function test($persona=null){
		$data = array();
		$pagina = 1;
		if(empty($persona)) $persona	= traerValorDe("persona");
		$persona = str_replace(" ", "+", $persona);
		$data[] = "start=$pagina";
		$data[] = "q=$persona";
		$data[] = "cx=" . google::cx('social');
		
		google::buscar($data);
	}

// fin clase google
}


						 		
