<?php 
	require_once $_SERVER["DOCUMENT_ROOT"] ."/servicios/persona/persona.php";
class fii {
	/**
	 * getBD
	 * retorna la base de datos en uso.
	 * @return string con nombre de la bd
	 */
	public static function getBD($dar='BD'){
			if($dar=='BD') return "BD";
			if($dar=='Tb') return array('fii_cert');
	}



	/**
	 * accesoDef
	 * valores por defecto de acceso a cada método.
	 * @param string $metodo el nombre del método a validar 
	 * @return int nivel  de acceso
	 */
	public static function accesoDef($metodo){
		switch ($metodo) {
		    case "getBD": 			return 1; break;
		    case "total": 			return 1; break;
		    case "analyze": 		return 1; break;
		    case "getCron": 		return 1; break;
		    case "accesoDef": 	return 1; break;
		    default: 						return 1; 
		}
	}

	/**
	 * getCron
	 * Tareas a ejecutar por crontab
	 * @return array tareas a ejecutar
	 */
	public static function getCron(){ 
		$wbase = "http://" . $_SERVER['SERVER_NAME'];
		$fn = "wget -O /dev/null $wbase/fii/getSerialFaltante/";
		$tareas = array();
		$tarea1 = array(
			'nombre'	=>'getSerialFaltante',
			'command'	=> $fn, 
			'schedule'=>'*/2 15-19 * * 1-5',
			'output'	=>'', 
			'enabled'	=> true,
		);
		$tareas[] = $tarea1;
		$nombre = $tarea1['nombre'];
//		for ($x =0; $x <= 9; $x++){$tarea1['nombre'] = $nombre . $x;$tareas[] = $tarea1;};
		return $tareas;
	}

	/**
	 * getDato
	 * Extrae un dato del html de una página específica basandose en una pista
	 * @param $html string el cuerpo de la página Web
	 * @param $pista string un texto que sirve para ubicar el texto buscado
	 * @return string con cadena buscada
	 */
	public static function getDato($frase, $pista){ 
		$pos = strpos($frase, $pista);
		if($pos == false) return false;
		$vi = strpos($frase, "<td", $pos);
		$vi = strpos($frase, ">", $vi) + 1;
		$vf = strpos($frase, "</td>",$vi);
		$v = trim(substr($frase, $vi, $vf - $vi ));
		return $v;
	}



	/**
	 * getFIICert
	 * Extrae datos de personas 
	 * @param $cedula string el numero de cédula de la persona a buscar
	 * @return array con datos de la persona
	 */
	public static function getFIICert($serial=null, $set=null){
		$errores = array();
		$data    = array();
		$post    = array();

		if($set==null)          	$set = valorde("set");
		if($set== "")       			$set = "1";
		if($serial==null)         $serial = valorde("serial");
		if($serial == "")         $errores['serial']  = 'Falta el serial';
		if(!is_numeric($serial))  $errores['serial']  = 'Se esperaba un número';

		if (empty($errores)){
			$url = "https://ar.fii.gob.ve/cgi-bin/openca/pub/pki?cmd=viewCert&key=$serial";
			//$respuesta = Curl_lib::post($url, $post);
			//$result = implode(" ",$respuesta['body']);
			$pagina = get_web_page($url);
			//print_r($pagina) ;die();
			if(isset($pagina['http_code']) && intval($pagina['http_code']) == 200 && intval($pagina['size_download'])>1000) $result = utf8_encode($pagina['content']);
			else $data['situacion']  = 'No existe';
			if(strpos($pagina['content'], 'Error de Sistema')!== false) $data['situacion']  = 'No existe';
		}
//print_r($result) ;die();
		$existe = true;
		if(isset($data['situacion']) && $data['situacion']  == 'No existe') $existe = false;
		if (empty($errores)){
			if($existe){
				$tmp = fii::getDato($result, "Versión del Certificado");if($tmp) $data["versioncert"] 		= $tmp;
				$tmp = fii::getDato($result, "Correo Electrónico"); 		if($tmp) $data["email"] 		= $tmp;
				$tmp = fii::getDato($result, "Válido desde"); 					if($tmp) $data["fechaini"] 	= $tmp;
				$tmp = fii::getDato($result, "Expira en"); 							if($tmp) $data["fechafin"] 	= $tmp;
				$tmp = fii::getDato($result, "Estatus actual"); 				if($tmp) $data["situacion"]	= $tmp;

				$tmp = fii::getDato($result, "Nombre Distinguido"); 		if($tmp) //$data["tmp"]	= $tmp;
				{
					$detalle = explode("<BR>\n", $tmp);

					foreach ($detalle as $frase) {
					  if(strpos($frase, 'serialNumber')!== false || strpos($frase, 'ucturedName')!== false){
					  	$aextraer = $frase;
					  	if(strpos($aextraer, 'UTF8')!== false) $aextraer = "=". explode(":", $frase)[1];

					  	if(strpos($aextraer, '=CI')!== false){
					  		//unstructuredName=CI-V-6452539
					  		$data["letra"] 			= explode("-", $aextraer)[1];
					  		if(count(explode("-", $aextraer))>2)	$data["cedula"] 		= explode("-", $aextraer)[2];
					  	}else{
					  		if(is_numeric(explode("-", $aextraer)[1])){
						  		//unstructuredName=V-14015415
		 							$data["letra"] 			= explode("=", $aextraer)[1][0];
		 							$data["cedula"] 		= explode("-", $aextraer)[1];
					  		}

					  	}
					  } 	
					  if(strpos($frase, 'emailAddress')!== false) 	$data["email"] 			= explode("=", $frase)[1];
					  if(strpos($frase, 'CN=')!== false) 						$data["nombre"] 		= explode("=", $frase)[1];
					  if(strpos($frase, 'L=')!== false) 						$data["municipio"] 	= explode("=", $frase)[1];
					  if(strpos($frase, 'ST=')!== false) 						$data["estado"] 		= explode("=", $frase)[1];
					  if(strpos($frase, 'OU=')!== false) 						$data["cargo"] 			= explode("=", $frase)[1];
					  if(strpos($frase, 'O=')!== false) 						$data["empresa"] 		= explode("=", $frase)[1];
					  if(strpos($frase, 'C=')!== false) 						$data["pais"] 			= explode("=", $frase)[1];
					}
				}
			}else{
				$data['situacion']  = 'No existe';
			}
			$data["serial"] = $serial;
			$data["estatus"] = '1';
		}else{
			$data["estatus"] = '0';
			$data['errores']  = $errores;
		}

		if($data["estatus"]=='1' && $set == '1' && $existe){
			$data['set'] = fii::setFIICert($data);
			$data['set']['persona'] = persona::getPersona($data["letra"], $data["cedula"], 1);
		}else{
			$data['set'] = array('estatus' => '0', 'set' => $set, 'existe' => $existe);
		}

		if(strpos("$_SERVER[REQUEST_URI]", __FUNCTION__) !== false) return entregar($data); else return $data;
	}


		/**
		 * getLocal
		 * Extrae datos de personas de la base de dtos local
		 * @param $cedula string el numero de cédula de la persona a buscar
		 * @return array con datos de la persona
		 */
		public static function getLocal($letra=null, $cedula=null){
				$errores    = array();
				$data       = array();
				$data["estatus"] = '0';

				if($letra==null)          $letra = valorde("letra");
				if($letra == "")          $errores['letra']   = 'Falta la nacionalidad';
				if($cedula==null)         $cedula = valorde("cedula");
				if($cedula == "")         $errores['cedula']  = 'Falta la cédula de identidad';
				if(!is_numeric($cedula))  $errores['cedula']  = 'Se esperaba un número';

				if (empty($errores)){
						$sql="select * from fii_cert where cedula=$cedula AND letra='$letra'";
						$resultado = DbLink::resultados($sql,saime::getBD());
						if($resultado !== false){
								$data = $resultado;
								$data["estatus"] = '1';
						}
				}else{
					$data['errores']  = $errores;
				}

				if(strpos("$_SERVER[REQUEST_URI]", __FUNCTION__) !== false){
						return entregar($data);
				}else{
						return $data;
				}
		}


		/**
		 * getSerialNuevo
		 * Retorna un serial no visto
		 * @param $cedula string el numero de cédula de la persona a buscar
		 * @return array con datos de la persona
		 */
		public static function getSerialNuevo($tiempo=null){
			if($tiempo!=null && microtime(true) - $tiempo > 3 ) return null;
			if($tiempo==null) $tiempo = microtime(true);
			$total = 13469;
			$azar = rand(0, $total);
			$sql = "select serial from fii_cert where serial=$azar";
			$resultado = DbLink::resultados($sql,fii::getBD());
    	if($resultado !== false){
    		return fii::getSerialNuevo($tiempo);
    	}else{
    		return $azar;
    	}
		}

		/**
		 * getSerialFaltante
		 * Retorna un serial no registrado
		 * @return array con datos de la persona
		 */
		public static function getSerialFaltante(){
			$data = array();
			$sql = "select min(serial+1) as id from fii_cert where serial+1 not in (select serial from fii_cert)";
			$resultado = DbLink::resultados($sql,fii::getBD());

    	if($resultado !== false) $data = fii::getFIICert($resultado[0]['id'], 1);
    	else $data = array('result'=>'0');

			if(strpos("$_SERVER[REQUEST_URI]", __FUNCTION__) !== false)	return entregar($data);
			else return $data;
		}

		/**
		 * setFIICertNext
		 * @return string con cadena buscada
		 */
		public static function setFIICertNext(){
			$data = array();
			$errores = array();
			$tiempo = microtime(true);
			
			$serial = fii::getSerialNuevo();
			$data = fii::getFIICert($serial,1);

			$data['funcion']['serial'] = $serial;
			$data['funcion']['tiempo'] = round(microtime(true) - $tiempo);

			if(strpos("$_SERVER[REQUEST_URI]", __FUNCTION__) !== false) return entregar($data); else return $data;
		}


	/**
	 * setFIICert
	 * @param $datos
	 * @return array de datos de persona
	 */
	public static function setFIICert($datos=null){
		$errores = array();
		$data = array();
		if($datos==null) $errores['datos'] 	= 'No hay datos para actualizar';
		if($datos['estatus']!="1") $errores['serial'] 	= 'El serial no se ha encontrado';

    if (empty($errores)){
			$versioncert = isset($datos['versioncert'])? "'". $datos['versioncert'] . "'":"null";
			$email = isset($datos['email'])? "'".$datos['email'] . "'":"null";
			$fechaini = isset($datos['fechaini'])? "'". date("Y-m-d", strtotime($datos["fechaini"])) . "'":"null";
			$fechafin = isset($datos['fechafin'])? "'". date("Y-m-d", strtotime($datos["fechafin"])) . "'":"null";
			$cedula = isset($datos['cedula'])? "".$datos['cedula'] . "":"null";
			$letra = isset($datos['letra'])? "'".substr($datos['letra'], 0, 1) . "'":"null";
			$nombre = isset($datos['nombre'])? "'".$datos['nombre'] . "'":"null";
			$cargo = isset($datos['cargo'])? "'". $datos['cargo'] . "'":"null";
			$empresa = isset($datos['empresa'])? "'". $datos['empresa'] . "'":"null";
			$estado = isset($datos['estado'])? "'". $datos['estado'] . "'":"null";
			$municipio = isset($datos['municipio'])? "'". $datos['municipio'] . "'":"null";
			$pais = isset($datos['pais'])? "'". $datos['pais'] . "'":"null";
			$situacion = isset($datos['situacion'])? "'". $datos['situacion'] . "'":"null";

    	//verificar si el serial ya existe
    	$sql = "select serial from fii_cert 
    		where serial = {$datos['serial']}
			";
    	$resultado = DbLink::resultados($sql,fii::getBD());
    	if($resultado !== false){ 
    		$actualizado = date("Y-m-d H:i:s");
				$sql = "
						UPDATE fii_cert 
						SET 
							cedula = $cedula,
							letra = $letra,
							nombre = $nombre,
							situacion = $situacion,
							fecha_modificacion = '$actualizado'
						where serial = {$datos['serial']}
						RETURNING serial;
				";
				$resultado = DbLink::resultados($sql,fii::getBD());
				if($resultado !== false){
					$data['update'] = '1';
				}else{
					$data['update'] = '0';
				}
    	}else{ //insert
    		$actualizado = date("Y-m-d H:i:s");
    		$sql = "
    			INSERT INTO fii_cert (serial, versioncert, cedula, letra, email, cargo, empresa, estado, municipio, pais, fechaini, fechafin, situacion, fecha_modificacion, nombre)
					VALUES ({$datos['serial']}, 
									$versioncert,
									$cedula,
									$letra,
									$email,
									$cargo,
									$empresa,
									$estado,
									$municipio,
									$pais,
									$fechaini,
									$fechafin,
									$situacion,
									'$actualizado',
									$nombre
					)
					RETURNING cedula;
    		";
    		//echo $sql; die();
				$resultado = DbLink::resultados($sql,fii::getBD());
				if($resultado !== false){
					$data['insert'] = '1';
				}else{
					$data['insert'] = '0';
				}
    	}
		}else{
			$data['estatus'] = '0';
      $data['errores']  = $errores;
		}
		return $data;
	}


	/**
	 * total
	 * Total registros relacionados
	 * @return array de datos de persona
	 */
	public static function total(){
		$data = array();
		foreach (fii::getBD('Tb') as $tabla) $data[$tabla] = totalRegBDTable($tabla, fii::getBD());
		if(strpos("$_SERVER[REQUEST_URI]", __FUNCTION__) !== false)return entregar($data); else return $data;
	}

	/**
	 * analyze
	 * Actualiza las estadísticas de cada tabla
	 * @return array de datos de persona
	 */
	public static function analyze(){
		$data = array();
		foreach (fii::getBD('Tb') as $tabla) $data[$tabla] = analyzeBDTable($tabla, fii::getBD());
		if(strpos("$_SERVER[REQUEST_URI]", __FUNCTION__) !== false)return entregar($data); else return $data;
	}

	/**
	 * lastUpdate
	 * Retorna el último registro actualizado para cada tablla registrada
	 * @return array de datos de persona
	 */
	public static function lastUpdate(){
		$data = array();
		$temp = bd::lastUpdate(self::getBD('Tb')[0], self::getBD());
		$data = self::getFIICert($temp['serial'], 0);
		if(strpos("$_SERVER[REQUEST_URI]", __CLASS__."/".__FUNCTION__) !== false) return entregar($data); else return $data;
	}


// fin clase fii
}
