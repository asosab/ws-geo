<?php 
    require_once $_SERVER["DOCUMENT_ROOT"] ."/vendor/PHPMailer/PHPMailerAutoload.php";

class correo {

  /**
   * accesoDef
   * valores por defecto de acceso a cada método.
   * @param string $metodo el nombre del método a validar 
   * @return int nivel  de acceso
   */
  public static function accesoDef($metodo){
    switch ($metodo) {
        case "getBD":       return 1; break;
        case "total":       return 1; break;
        case "analyze":     return 1; break;
        case "accesoDef":   return 1; break;
        case "enviar":      return 1; break; 
    }
  }

  /**
   * setRegistro
   * Registra el tráfico de usuarios por la página
   * @return true si la página pedida se ha cargado correctamente
   */
  public static function enviar($datos=null){
    global $conf;
    $errores = array();
    if(empty($datos)){
    	$datos = array();
    	$datos['para'] 				= traerValorDe("para");
    	$datos['cuerpoplano'] = traerValorDe("cuerpoplano");
    	$datos['dir_html'] 		= traerValorDe("dir_html");
    	$datos['nombre'] 			= traerValorDe("nombre");
    	$datos['titulo'] 			= traerValorDe("titulo");
    	$datos['archivos'] 		= traerValorDe("archivos");
    }
    // Verifico errores
    if(empty($datos['para'])) $errores['para'] = "Falta el destinatario";

    if (empty($errores)){
    	date_default_timezone_set('America/Caracas');
    	$config         = $conf['correo'];
    	$mail           = new PHPMailer;
    	$nombre_para    = isset($datos['nombre'])?$datos['nombre']:"";
    	$titulo         = isset($datos['titulo'])?$datos['titulo']:"Mensaje de " . $conf['app']['nombre_largo'];

      try {
        $mail->IsSMTP();
        $mail->CharSet          = $config['CharSet'];
        $mail->SMTPDebug        = $config['SMTPDebug']; 
        $mail->Debugoutput      = $config['Debugoutput'];
        $mail->Host             = $config['Host'];
        $mail->Port             = $config['Port'];
        $mail->SMTPAuth         = $config['SMTPAuth'];
        $mail->Username         = $config['username'];   
        $mail->Password         = $config['password'];   
        $mail->WordWrap         = $config['WordWrap'];
        $mail->setFrom($config['From'], $conf['app']['nombre_corto']);
        $mail->addReplyTo($config['From'], $conf['app']['nombre_corto']);
        $mail->AddCustomHeader('X-Mailer: '.$conf['app']['nombre_corto'] . ' (' . $conf['app']['web_base'] . ')');

        if(is_array($datos['para']))foreach($datos['para'] as $para) $mail->addAddress($para, '');
        else $mail->addAddress($datos['para'], $nombre_para);
        $mail->Subject = $titulo;
        if(isset($datos['dir_html']) && $datos['dir_html'] !='' ){
          $mail->isHTML(true);
          $mail->msgHTML(file_get_contents($datos['dir_html']));
          if(isset($datos['cuerpoplano'])) $mail->AltBody = $datos['cuerpoplano']; 
        }else{
          $mail->Body = $datos['cuerpoplano'];
        }

        if(isset($datos['archivos'])){
          if(is_array($datos['archivos'])){
            foreach ($datos['archivos'] as $archivo){
              $mail->AddStringAttachment($archivo['binario'],$archivo['nombre']);
            }
          }else{
            $mail->AddAttachment($datos['archivos']);
          }
        }
        
        if(!$mail->Send()) $errores['envío'] = $mail->ErrorInfo;
        else $datos['estatus'] = '1';
      } catch (phpmailerException $e) {
          $errores['phpmailerException'] = $e->errorMessage(); // from PHPMailer
      } catch (Exception $e) {
          $errores['Exception'] = $e->getMessage(); // from anything else!
      }
    }else{
      $datos['error'] = $errores;
    }

    if (!empty($errores)){
      $datos['estatus'] = '0';
      $datos['error'] = $errores;
    }

    if(strpos("$_SERVER[REQUEST_URI]", __FUNCTION__) !== false) return entregar($datos); else return $datos;
  }

// fin clase correo	
}

?>