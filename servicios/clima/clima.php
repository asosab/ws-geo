<?php 
	require_once $_SERVER["DOCUMENT_ROOT"] . '/servicios/google/google.php';
	require_once $_SERVER["DOCUMENT_ROOT"] . '/servicios/geo/geo.php';

class clima {
	/**
	 * accesoDef
	 * valores por defecto de acceso a cada método.
	 * @param string $metodo el nombre del método a validar 
	 * @return int nivel  de acceso
	 */
	public static function accesoDef($metodo){
		switch ($metodo) {
		    case "getBD": 			return 1; break;
		    case "total": 			return 1; break;
		    case "analyze": 		return 1; break;
		    case "accesoDef": 	return 1; break;
		    case "buscar": 			return 1; break;
		    case "test": 				return 1; break;
		    case "getCron": 		return 1; break;
		    default: 						return 1; 
		}
	}

	/**
	 * getCron
	 * Tareas a ejecutar por crontab
	 * @return array tareas a ejecutar
	 */
	public static function getCron(){ 
		$tareas = array();
		$wbase = "http://" . $_SERVER['SERVER_NAME'];

		$coords = array(
			"5.1317339,-60.8285019",		// gran sabana
			"7.8337766,-72.226252", 		// Patiecitos, Guásimos, Táchira
			"10.5017661,-66.849789",   // Caracas
			);
		foreach ($coords as $index => $coord) {
			$tarea = array(
				'nombre'	=>"clima$index",
				'command'	=> "wget -O /dev/null $wbase/clima/get/coord/$coord/set/1/",
				'schedule'=>'*/20 * * * *',
				'output'	=>'',
				'enabled'	=> false,
			);
			$tareas[] = $tarea;
		}
		return $tareas;
	}


	/**
	 * getBD
	 * retorna la base de datos en uso.
	 * @return string con nombre de la bd
	 */
	public static function getBD($dar='BD'){
			if($dar=='BD') return "BD";
			if($dar=='Tb') return array('clima');
	}


	/**
	 * k2c
	 * Grados Kelvin a centígrados
	 * @param string $temp temperatura 
	 * @return int temperatura en grados centígrados
	 */
  public static function k2c($temp) {
    if ( !is_numeric($temp) ) { return false; }
    return round(($temp - 273.15));
  }

	/**
	 * sensTerm
	 * Sensación térmica
	 * Formula: AT = Ta+ 0.348 * e-0.70 * ws + (0.70* (Q * (ws+10)) - 4.25)
	 * e = (rh)/100)*6.105*e((17.27 *Ta) / (237.7+Ta))
	 * Where, AT = Apparent Temperature, Ta = Dry Bulb Temperature, e = Water Vapour Pressure, ws = Wind Speed at an Elevation of 10 meters, Q = Net Radiation Absorbed per unit area of Body Surface, rh = Relative Humidity
	 * @param array $data array(Ta,ws,Q,rh)
	 * @return int temperatura en grados centígrados
	 */
  public static function sensTerm($data) {
  	//TODO
  }

	/**
	 * llave
	 * valores por defecto de acceso a cada método.
	 * @param string $metodo el nombre del método a validar 
	 * @return int nivel  de acceso
	 */
	public static function llave($nombre){
		switch ($nombre) {
		    case "ws": 				return "5578c6742080858fa2d8461f4bc7e385"; break;
		    case "carauren": 	return "3cb7185819a915a56c583342c6a5a131"; break;
		    default: 				return false; 
		}
	}

	/**
	 * gradosToDir
	 * grados (270 es sur) a dirección.
	 * @param string $grados numero entre 0 y 270 
	 * @return int nivel  de acceso
	 */
	public static function gradosToDir($grados){
		$valor = intval($grados);
		switch (true) {
	    case in_array($valor, range(341,360)): return "E"; break;
	    case in_array($valor, range(0,20)): 		return "E"; break;
	    case in_array($valor, range(21,60)): 	return "NE"; break;
	    case in_array($valor, range(61,110)): 	return "N"; break;
	    case in_array($valor, range(111,160)): return "NO"; break;
	    case in_array($valor, range(161,200)): return "O"; break;
	    case in_array($valor, range(201,250)): return "SO"; break;
	    case in_array($valor, range(251,290)): return "S"; break;
	    case in_array($valor, range(291,340)): return "SE"; break;
	    default: return false; 
		}
	}

	/**
	 * codToHuman
	 * valores por defecto de acceso a cada método.
	 * @param string $metodo el nombre del método a validar 
	 * @return int nivel  de acceso
	 */
	public static function codToHuman($code){
		switch ($code) {
	    case 200: return "tormenta con lluvia ligera"; break;
	    case 201: return "tormenta con lluvia"; break;
	    case 202: return "tormenta con fuertes lluvias"; break;
	    case 210: return "tormenta ligera"; break;
	    case 211: return "tormenta"; break;
	    case 212: return "tormenta fuerte"; break;
	    case 221: return "tormenta irregular"; break;
	    case 230: return "tormenta con llovizna ligera"; break;
	    case 231: return "tormenta con llovizna"; break;
	    case 232: return "tormenta con llovizna pesada"; break;
	    case 300: return "llovizna con intensidad suave"; break;
	    case 301: return "garúa"; break;
	    case 302: return "llovizna fuerte"; break;
	    case 310: return "Lluvia casi llovizna"; break;
	    case 311: return "Garúa casi lluvia"; break;
	    case 312: return "llovizna fuerte"; break;
	    case 313: return "Lluvia copiosa y llovizna"; break;
	    case 314: return "aguacero y lloviznas"; break;
	    case 321: return "llovizna abundante"; break;
	    case 500: return "lluvia ligera"; break;
	    case 501: return "lluvia moderada"; break;
	    case 502: return "lluvia intensa"; break;
	    case 503: return "lluvia muy intensa"; break;
	    case 504: return "palo de agua"; break;
	    case 511: return "lluvia helada"; break;
	    case 520: return "aguacero ligero"; break;
	    case 521: return "aguacero"; break;
	    case 522: return "fuerte aguacero"; break;
	    case 531: return "aguacero caótico"; break;
	    case 600: return "nieve ligera"; break;
	    case 601: return "nieve"; break;
	    case 602: return "nevada fuerte"; break;
	    case 611: return "aguanieve"; break;
	    case 612: return "aguanieve lluvioso"; break;
	    case 615: return "lluvia ligera y neve"; break;
	    case 616: return "lluvia y neve"; break;
	    case 620: return "aguacero moderado y nieve"; break;
	    case 621: return "aguacero y nieve"; break;
	    case 622: return "palo de agua y nieve"; break;
	    case 701: return "niebla"; break;
	    case 711: return "humo"; break;
	    case 721: return "calina"; break;
	    case 731: return "remolinos de arena"; break;
	    case 741: return "bruma"; break;
	    case 751: return "arena"; break;
	    case 761: return "polvo"; break;
	    case 762: return "cenizas volcánicas"; break;
	    case 771: return "chubascos"; break;
	    case 781: return "tornado"; break;
	    case 800: return "cielo despejado"; break;
	    case 801: return "pocas nubes"; break;
	    case 802: return "nubes dispersas"; break;
	    case 803: return "nubosidad"; break;
	    case 804: return "nublado"; break;
	    case 900: return "tornado"; break;
	    case 901: return "tormenta tropical"; break;
	    case 902: return "huracán"; break;
	    case 903: return "frío"; break;
	    case 904: return "caliente"; break;
	    case 905: return "ventoso"; break;
	    case 906: return "granizo"; break;
	    case 951: return "calmo"; break;
	    case 952: return "brisa ligera"; break;
	    case 953: return "brisa suave"; break;
	    case 954: return "brisa moderada"; break;
	    case 955: return "brisa fresca"; break;
	    case 956: return "brisa fuerte"; break;
	    case 957: return "fuerte viento, casi vendaval"; break;
	    case 958: return "vendaval"; break;
	    case 959: return "fuerte vendaval"; break;
	    case 960: return "tormenta"; break;
	    case 961: return "tormenta violenta"; break;
	    case 962: return "huracán"; break;
	    default: return false; 
		}
	}

	/**
	 * get
	 * datos de valores climáticos según coordenadas geográficas.
	 * @param string $metodo el nombre del método a validar 
	 * @return int nivel  de acceso
	 */
	public static function get($get=null, $set=null){
		$errores    = array();
		$data       = array();
		$segundos = rand(0, 10);
		sleep($segundos);
		// icon http://openweathermap.org/img/w/10d.png
		$apiID = clima::llave('ws');
		if(empty($get)) $get = traerValorDe("get");
		if(!$get) $errores['get'] = $data['mensaje'] = 'Faltan las coordenadas';

		$coord = geo::geohash($get);

		if(empty($set)) $set = traerValorDe("set");
/*
		if(empty($errores)){
			if(is_string($coord)){
				if(strpos($coord, ",")){
					$ar = array();
					$ar['lat'] = explode(",", $coord)[0];
					$ar['lon'] = explode(",", $coord)[1];
					$coord = $ar;
				}else{
					$errores['coord'] = $data['mensaje'] = 'no se entienden las coordenadas';
				}
			}
		}
*/
		if(empty($errores)){
			// verificar si se ha guardado el registro dentro de los últimos 15 minutos, en ese caso sacarlo de la bd
			$tbClima = clima::getBD('Tb')[0];
			$sql ="SELECT * FROM $tbClima where geohash = '".$coord['geohash']."' order by fecha desc limit 1";
			$resultado = DbLink::resultados($sql,self::getBD());
			if($resultado !==false){
				$fecha = new DateTime();
				if($fecha->getTimestamp() - $resultado[0]['fecha'] < 1800){
					$data = $resultado[0];
					$data['viento_dir'] = clima::gradosToDir($data['viento_g']);
					$data['estatus'] 		= '1';
				}
			}else{

			}
		}

		if(empty($errores) && empty($data)){
			$url = "http://api.openweathermap.org/data/2.5/weather?lat={$coord['lat']}&lon={$coord['lon']}";
			$url .="&APPID=$apiID";
			$json = file_get_contents($url);
//			echo $json; die();
			if($json)$datos = json_decode($json, true);
			else $errores['url'] = $data['mensaje'] = 'No se pudo obtener la información';
		}

		if(empty($errores) && empty($data)){	 
			$data['descripcion']= clima::codToHuman($datos['weather'][0]['id']);
			$data['lat'] 				= $coord['lat'];
			$data['lon'] 				= $coord['lon'];
			$data['geohash'] 		= $coord['geohash'];
			$data['fecha'] 			= $datos['dt'];
			$data['icon'] 			= $datos['weather'][0]['icon'];
			$data['clima_id'] 	= $datos['weather'][0]['id'];
			$data['temp'] 			= clima::k2c($datos['main']['temp']);
			$data['presion'] 		= $datos['main']['pressure'];
			$data['humedad'] 		= $datos['main']['humidity'];
			$data['viento_v'] 	= $datos['wind']['speed'];
			$data['viento_g'] 	= $datos['wind']['deg'];
			$data['viento_dir'] = clima::gradosToDir($datos['wind']['deg']);
			$data['nuvocidad'] 	= $datos['clouds']['all'];
			$data['ciudad_no'] 	= $datos['name'];
			$data['ciudad_id'] 	= $datos['id'];
			$data['estatus'] 		= '1';
			$data['sunrise'] 		= $datos['sys']['sunrise'];
			$data['sunset'] 		= $datos['sys']['sunset'];
			if(!empty($datos['rain']['3h'])) $data['lluvia_vol'] = $datos['rain']['3h'];
			else $data['lluvia_vol'] = 0;
			if(!empty($datos['snow']['3h'])) $data['nieve_vol'] = $datos['snow']['3h'];
			else $data['nieve_vol'] = 0;
			$ct = self::corregirTemp($data);
			$data['tempC'] = round($ct['tempC']);
			$data['tempb'] = $data['temp'];
			$data['temp'] = $data['tempC'];
			$data['alt'] = round($ct['alt']);
			unset($data['tempC']);
			if($set) $data['set'] = loadDataTable(clima::getBD(), clima::getBD('Tb')[0], $data);
		}

		if(!empty($errores)){
			$data['estatus'] 		= '0';
			$data['error']  = $errores;
		}

		if(strpos("$_SERVER[REQUEST_URI]", __CLASS__."/".__FUNCTION__) !== false) return entregar($data); else return $data;
	}


	/**
	 * corregirTemp
	 * corrige la temperatura en base a la diferencia de alturas respecto a la estación climática
	 Palmia = 3630982, alt 1097
	 * @param string $coord coordenadas "lat,lon"
	 * @return int temperatura corregida
	 */
	public static function corregirTemp($data=null){
		if(empty($data)) return false;
		$coord=$data['lat'].",".$data['lon'];
		$temp=$data['temp'];
		$ciudad_id=$data['ciudad_id'];
		$data['alt'] = google::elev($coord)['alt'];
		
		// la temperatura disminuye a un ritmo de unos 0,65 ºC por cada 100 metros de elevación
		//verifico si la estación cllimática tiene altura registrada, si es así la tomo
		$TblClima = "geo_clima";
		$sql = "select * from $TblClima where id = $ciudad_id";
		$resultado = DbLink::resultados($sql,self::getBD());
		// doy por sentado que la estacion existe
		if(empty($resultado[0]['alt'])){ // no existe un registro de altitud, lo agrego
			$estCoord = $resultado[0]['lat'].",".$resultado[0]['lon'];
			$info = google::elev($estCoord);
			$resultado[0]['alt'] = round($info['alt']);
			$set = loadDataTable(clima::getBD(), $TblClima, $resultado[0]);
		}
		if(!empty($resultado[0]['alt'])){ // calculo nueva temperatura
			$data['tempC'] = $data['temp'] + (0.65 * (($resultado[0]['alt'] - $data['alt'])/100));
		}else{
			$data['error']['tempC'] = "Falta altitud en estación climática para hacer el cálculo";
		}

		if(strpos("$_SERVER[REQUEST_URI]", __CLASS__."/".__FUNCTION__) !== false) return entregar($data); else return $data;
	}

// fin clase clima
}


						 		
