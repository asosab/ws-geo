-- creo la tabla
CREATE TABLE clima
(
  geohash     character varying(10) NOT NULL,
  fecha       bigint NOT NULL,
  clima_id    smallint,
  icon        character varying(3),
  temp        smallint,
  presion     smallint,
  humedad     smallint,
  viento_v    numeric(3,2),
  viento_g    smallint,
  nuvocidad   smallint,
  ciudad_id   bigint,
  ciudad_no   character varying(150),
  sunrise     bigint,
  sunset      bigint,
  lluvia_vol  smallint,
  nieve_vol   smallint,
  CONSTRAINT clima_pkey PRIMARY KEY (geohash,fecha)
)
WITH (OIDS=FALSE);
ALTER TABLE clima OWNER TO postgres;
GRANT ALL ON TABLE clima TO postgres;
GRANT ALL ON TABLE clima TO data;
