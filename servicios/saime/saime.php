<?php 

class saime {

    /**
     * getBD
     * retorna la base de datos en uso.
     * @return string con nombre de la bd
     */
    public static function getBD($dar='BD'){
      if($dar=='BD') return "BD";
      if($dar=='Tb') return array('saime');
    }

  /**
   * accesoDef
   * valores por defecto de acceso a cada método.
   * @param string $metodo el nombre del método a validar 
   * @return int nivel  de acceso
   */
  public static function accesoDef($metodo){
    switch ($metodo) {
        case "getBD":       return 1; break;
        case "total":       return 1; break;
        case "analyze":     return 1; break;
        case "accesoDef":   return 1; break;
        default:            return 1; 
    }
  }

    /**
     * getDato
     * Extrae un dato del html de una página específica basandose en una pista
     * @param $html string el cuerpo de la página Web
     * @param $pista string un texto que sirve para ubicar el texto buscado
     * @return string con cadena buscada
     */
    public static function getDato($html, $pista){
        $pos = strpos($html, $pista);
        if($pos == false) return false;
        $vi = strpos($html, ":",$pos) +2;
        $vf = strpos($html, "</li>",$vi);
        $v = trim(substr($html, $vi, $vf - $vi ));
        return $v;
    }

    /**
     * getDatos
     * Extrae datos de personas 
     * @param $cedula string el numero de cédula de la persona a buscar
     * @param $set 1=grabar local, 0=no grabar
     * @param $fuente local,dateas,cnti, auto
     * @return array con datos de la persona
     */
    public static function getDatos($letra=null, $cedula=null, $set=null, $fuente=null){
        $errores         = array();
        $data           = array();

        if($fuente==null)               $fuente = valorde("fuente");
        if($fuente== "")                    $fuente = "auto";
        if($set==null)                      $set = valorde("set");
        if($set== "")                           $set = "1";

        if($letra==null)                    $letra = valorde("letra");
        if($letra == "")                    $errores['letra']   = 'Falta la nacionalidad';
        if($cedula==null)               $cedula = valorde("cedula");
        if($cedula == "")               $errores['cedula']  = 'Falta la cédula de identidad';
        if(!is_numeric($cedula))    $errores['cedula']  = 'cédula: se esperaba un número';

        if (empty($errores)){
            if($fuente == "local")      $data = saime::getLocal($letra,$cedula);
            //if($fuente == "dateas")     $data = saime::getDateas($cedula,$set);
            if($fuente == "cnti")       $data = saime::getCNTI($letra,$cedula,$set);
            if($fuente == "auto"){
                $data = saime::getLocal($letra,$cedula);
                $local = saime::getLocal($letra,$cedula);
                if($data['estatus']=='1') $ano = explode("-", $data['fecha_modificacion'])[0];
                else $ano = "2013";
                if($ano !== date("Y")){
                    $data = saime::getCNTI($letra,$cedula,$set);
                    if($data["estatus"] == '0'){
                        //$data = saime::getDateas($cedula,$set);
                        if($data["estatus"] == '0'){
                            $data = $local;
                            $data['set']['error'] = "no hay fuentes de actualización";
                        }
                    } 
                }
            }
        }else{
          $data["estatus"] = '0';
          $data['errores']  = $errores;
        }

        if(strpos("$_SERVER[REQUEST_URI]", __CLASS__."/".__FUNCTION__) !== false) return entregar($data); else return $data;
    }

    /**
     * getDateas
     * Extrae datos de personas 
     * @param $cedula string el numero de cédula de la persona a buscar
     * @return array con datos de la persona
     */
    public static function getDateas($cedula=null, $set=null){
        $errores         = array();
        $data           = array();

        if($set==null)            $set = valorde("set");
        if($set== "")             $set = "1";
        if($cedula==null)         $cedula = valorde("cedula");
        if($cedula == "")         $errores['cedula']  = 'Falta la cédula de identidad';
        if(!is_numeric($cedula))  $errores['cedula']  = 'cédula: se esperaba un número';
        if (empty($errores)){
            $post = array();
            $serverURL = "https://www.dateas.com/es/consulta_venezuela?cedula=$cedula";
            $respuesta = Curl_lib::post($serverURL, $post);
            if(!isset($respuesta['header'][8])) return array('cedula'=>$cedula,'estatus' => '0', 'error' => 'no hay respuesta de página Dateas');
            if(explode(" ", $respuesta['header'][8])[1] == "Accept-Encoding,User-Agent") return array('estatus' => '0');
            $respuesta = Curl_lib::post(explode(" ", $respuesta['header'][8])[1], $post);
            $result = utf8_encode(implode(" ",$respuesta['body']));
            $data = array();
            $tmp = saime::getDato($result, "Nombre:");  if($tmp) $data["nombre"]    = $tmp;
            $tmp = saime::getDato($result, "Cédula:");  if($tmp) $data["cedula"]    = str_replace(".", "", $tmp); 
            $tmp = saime::getDato($result, "Fecha de Nacimiento:");if($tmp){
                $date = DateTime::createFromFormat('d/m/Y', $tmp);
                $data["fechanac"] = $date->format('Y-m-d'); 
            } 
            $tmp = saime::getDato($result, "Estado:");      if($tmp) $data["estado"]        = $tmp;
            $tmp = saime::getDato($result, "Municipio:"); if($tmp) $data["municipio"] = $tmp;
            $tmp = saime::getDato($result, "Parroquia:"); if($tmp) $data["parroquia"] = $tmp;
            $data["estatus"] = '1';
        }else{
            $data["estatus"] = '0';
      $data['errores']  = $errores;
        }

        if($data["estatus"]=='1' && $set == '1'){
            $data['set'] = saime::setDateas($data);
        }

        if(strpos("$_SERVER[REQUEST_URI]", __FUNCTION__) !== false){
            return entregar($data);
        }else{
            return $data;
        }
    }

    /**
     * getLocal
     * Extrae datos de personas de la base de dtos local
     * @param $cedula string el numero de cédula de la persona a buscar
     * @return array con datos de la persona
     */
    public static function getLocal($letra=null, $cedula=null){
        $errores    = array();
        $data       = array();
        $data["estatus"] = '0';

        if(empty($letra))   $letra    = traerValorDe("letra");
        if(empty($letra))   $letra    = "V"; else $letra = strtoupper($letra);
        if(empty($cedula))  $cedula   = traerValorDe("cedula");
        if(!$cedula) $errores['cedula'] = $data['mensaje'] = 'Falta la cédula de identidad';
        if(!is_numeric($cedula)) $errores['cedula'] = $data['mensaje'] = 'Se esperaba un número';

        if (empty($errores)){
          $sql="select * from saime where cedula=$cedula AND letra='$letra'";
          $resultado = DbLink::resultados($sql,self::getBD());
          if($resultado !== false){
            $data = $resultado[0];
            $data["estatus"] = '1';
          }
        }else{
          $data["estatus"] = '0';
          $data['errores']  = $errores;
        }

        if(strpos("$_SERVER[REQUEST_URI]", __CLASS__."/".__FUNCTION__) !== false) return entregar($data); else return $data;
    }

    /**
     * getCNTI
     * Extrae datos de personas 
     * @param $cedula string el numero de cédula de la persona a buscar
     * @return array con datos de la persona
     */
    public static function getCNTI($letra=null, $cedula=null, $set=null, $pass='m23gk·$·8bgkiyqw3tqwriow823we·$fwr2'){
        $errores    = array();
        $data       = array();

        if($set==null)                      $set = valorde("set");
        if($set== "")                           $set = "1";

        if($letra==null)                $letra = valorde("letra");
        if($letra == "")                $errores['letra']   = 'Falta la nacionalidad';

        if($cedula==null)               $cedula = valorde("cedula");
        if($cedula == "")               $errores['cedula']  = 'Falta la cédula de identidad';
        if(!is_numeric($cedula))    $errores['cedula']  = 'cédula: se esperaba un número';

        if (empty($errores)){
            $post = array();
            $serverURL = "https://sinsera.inces.gob.ve/cnti/SAIME/$cedula/$pass/";
            $datapre = get_web_page($serverURL)['content'];
            $datapr = json_decode($datapre,true)['return'];
            $data = json_decode($datapr,true); 
            if(isset($data["NUMCEDULA"])){$data["estatus"] = '1';} else{$data["estatus"] = '0';}
        }else{
            $data["estatus"] = '0';
      $data['errores']  = $errores;
        }

        if($data["estatus"]=='1' && $set == '1'){
            $data['set'] = saime::setCNTI($data);
        }

        if(strpos("$_SERVER[REQUEST_URI]", __FUNCTION__) !== false){
            return entregar($data);
        }else{
            return $data;
        }
    }

    /**
     * setCNTI
     * @param $datos obtenidos de saime 
     * @return array de datos de persona
     */
    public static function setCNTI($datos=null){
        $errores        = array();
        $data           = array();

        if($datos==null) $errores['datos']  = 'No hay datos para actualizar';
        if($datos['estatus']=="0") $errores['cedula']   = 'El número de cédula no ha sido encontrado';

    if (!empty($errores)){
        $data['estatus'] = '0';
        $data['errores']  = $errores;
    }else{
        $cedula = $datos['NUMCEDULA'];
        $letra = $datos['LETRA'];
        $fechanac = $datos['FECHANAC'];
        $setfecha = ($fechanac !=="")? "fechanac = '$fechanac',":"";
        $naturalizado = ($datos['NATURALIZADO']=="0")? "false":"true";
        $actualizado = date("Y-m-d H:i:s");

//TEMPORAL!!!!!
                if($letra=="E"){
                $sql = "select cedula from saime where cedula=$cedula AND letra='V'";
                $resultado = DbLink::resultados($sql,saime::getBD());
                    if($resultado !== false){
                        $sql = "
                            UPDATE saime 
                            SET fecha_modificacion = '$actualizado'
                            WHERE cedula = $cedula AND letra  = 'V';
                        ";
                        $resultado = DbLink::resultados($sql,saime::getBD());
                    }
                }
//TEMPORAL!!!!!


        //verificar si existe en BD
        $sql = "select cedula from saime where cedula=$cedula AND letra='$letra'";
        $resultado = DbLink::resultados($sql,saime::getBD());
            if($resultado !== false){

                if(!isset($datos['CODESTADOCIVIL'])) $datos['CODESTADOCIVIL'] = 'null';

                $sql = "
                    UPDATE saime 
                    SET 
                        $setfecha
                        edocivil_id   = ". $datos['CODESTADOCIVIL'] .",
                        objecion_id   = '". $datos['CODOBJECION'] ."',
                        nacionalidad  = '". $datos['NACIONALIDAD'] ."',
                        naturalizado  = $naturalizado,
                        paisorigen    = '". $datos['PAISORIGEN'] ."',
                        sexo          = '". $datos['SEXO'] ."',
                        fecha_modificacion = '$actualizado'
                    WHERE cedula = $cedula AND letra  = '$letra'
                    RETURNING cedula;
                ";
                //echo $sql; die();
                $resultado = DbLink::resultados($sql,saime::getBD());
                $data['tipo'] = 'UPDATE';

            }else{
                if(isset($datos['SEGUNDONOMBRE'])) $datos['SEGUNDONOMBRE'] = "'".$datos['SEGUNDONOMBRE']."'";else $datos['SEGUNDONOMBRE'] = 'null';
                if(isset($datos['SEGUNDOAPELLIDO'])) $datos['SEGUNDOAPELLIDO'] = "'".$datos['SEGUNDOAPELLIDO']."'";else $datos['SEGUNDOAPELLIDO'] = 'null';



                $sql = "
                    INSERT INTO saime (letra, cedula, apellido1,apellido2,nombre1,nombre2,fechanac,sexo,edocivil_id,objecion_id,nacionalidad,naturalizado,paisorigen)
                    VALUES(
                        '$letra',
                        '$cedula',
                        '". $datos['PRIMERAPELLIDO'] ."',
                        ". $datos['SEGUNDOAPELLIDO'] .",
                        '". $datos['PRIMERNOMBRE'] ."',
                        ". $datos['SEGUNDONOMBRE'] .",
                        '". $datos['FECHANAC'] ."',
                        '". $datos['SEXO'] ."',
                        ". $datos['CODESTADOCIVIL'] .",
                        '". $datos['CODOBJECION'] ."',
                        '". $datos['NACIONALIDAD'] ."',
                        $naturalizado,
                        '". $datos['PAISORIGEN'] ."'
                     )
                     RETURNING cedula; 
                ";
                $resultado = DbLink::resultados($sql,saime::getBD());
                $data['tipo'] = 'INSERT';
            }

            if($resultado !== false){
                $data['estatus'] = '1';
            }else{
                $data['estatus'] = '0';
                $errores['BD'] = 'Error al registrar los datos';
                $errores['sql'] = $sql;
          $data['errores']  = $errores;
            }
        }
        return $data;
    }


    /**
     * setDateas
     * @param $datos obtenidos de saime 
     * @return array de datos de persona
     */
    public static function setDateas($datos=null){
        $errores        = array();
        $data           = array();

        if($datos==null) $errores['datos']  = 'No hay datos para actualizar';
        if($datos['estatus']=="0") $errores['cedula']   = 'El número de cédula no ha sido encontrado';

    if (!empty($errores)){
        $data['estatus'] = '0';
        $data['errores']  = $errores;
    }else{
        $cedula = $datos['cedula'];
        $fechanac = $datos['fechanac'];
        $setfecha = ($fechanac !=="")? "fechanac = '$fechanac',":"";
        $actualizado = date("Y-m-d H:i:s");
            $sql = "
                UPDATE saime 
                SET 
                    $setfecha
                    fecha_modificacion = '$actualizado'
                WHERE cedula = $cedula AND letra = 'V'
                RETURNING cedula;
            ";
            $resultado = DbLink::resultados($sql,saime::getBD());
            if($resultado !== false){
                $data['estatus'] = '1';
            }else{
                $data['estatus'] = '0';
                $errores['BD'] = 'Error al registrar los datos';
                $errores['sql'] = $sql;
          $data['errores']  = $errores;
            }
        }
        return $data;
    }


// fin clase saime
}

?>