<?php 
//error_reporting(E_ALL);
//ini_set('display_errors', 1);
//ini_set("max_execution_time","950000000000");


class cnti {

	/**
	 * accesoDef
	 * valores por defecto de acceso a cada método.
	 * @param string $metodo el nombre del método a validar 
	 * @return int nivel  de acceso
	 */
	public static function accesoDef($metodo){
		switch ($metodo) {
		    case "getBD": 			return 1; break;
		    case "total": 			return 1; break;
		    case "analyze": 		return 1; break;
		    case "accesoDef": 	return 1; break;
		    case "getDato": 		return 1; break;
		    case "getCron": 		return 1; break;
		    case "test": 				return 1; break;
		    default: 						return 1; 
		}
	}

	/**
	 * getBD
	 * retorna la base de datos en uso.
	 * @return string con nombre de la bd
	 */
	public static function getBD($dar='BD'){
			if($dar=='BD') return "BD";
			if($dar=='Tb') return array('seniat'); // saime es muy grande
	}

	public static function test(){
	//echo	php_ini_loaded_file();
	}

	/**
	 * getCron
	 * Tareas a ejecutar por crontab
	 * @return array tareas a ejecutar
	 */
	public static function getCron(){ 
		$serializer = new SuperClosure\Serializer();
		$fn = function(){self::setRNCNext();};
		$wbase = "http://" . $_SERVER['SERVER_NAME'];
//		$wbase = "http://ws/";
		$fn = "wget -O /dev/null $wbase/inces/setSaimeNext/";
		$tareas = array();
		$tarea1 = array(
			'nombre'	=>'setINCESNext',
			'command'	=> $fn, // function(){return self::setRNCNext();},  //  'rnc::setRNCNext()', // $FnS, // 
			'schedule'=>'* * * * *',
			'output'	=>'',  // /home/asosab/www/ws/rnc.json
			'enabled'	=> false,
		);
		$nombre = $tarea1['nombre'];
		for ($x =0; $x <= 9; $x++){$tarea1['nombre'] = $nombre . $x;$tareas[] = $tarea1;};
		return $tareas;
	}

		/**
		 * total
		 * Total registros relacionados
		 * @return array de datos de persona
		 */
		public static function total(){
			$data = array();
			foreach (rnc::getBD('Tb') as $tabla) $data[$tabla] = totalRegBDTable($tabla, rnc::getBD());
			if(strpos("$_SERVER[REQUEST_URI]", __FUNCTION__) !== false)return entregar($data); else return $data;
		}

		/**
		 * analyze
		 * Actualiza las estadísticas de cada tabla
		 * @return array de datos de persona
		 */
		public static function analyze(){
			$data = array();
			foreach (rnc::getBD('Tb') as $tabla) $data[$tabla] = analyzeBDTable($tabla, rnc::getBD());
			if(strpos("$_SERVER[REQUEST_URI]", __FUNCTION__) !== false)return entregar($data); else return $data;
		}


	/**
	 * getSaime
	 * Obtiene datos de saime.
	 * 
	 * @param string $getSaime número de cédula con o sin letra
	 * @param string $set si es 1 forza la búsqueda en Internet y guarda el resultado localmente
	 * @param string $espera agrega demora de entre 0 y 10 segundos antes de iniciar la petición
	 * @return array datos de SAIME
	 */
	public static function getSaime($getSaime=null, $set=null, $espera=false){
		$errores    = array();
		$data       = array();
		$segundos = rand(0, 10);
		$fecha = new DateTime();

		ini_set("soap.wsdl_cache_enabled", "0");
		ini_set("soap.wsdl_cache", "0");
		ini_set("display_errors","On");
		ini_set("track_errors","On");

		if(empty($getSaime)) $getSaime 	= traerValorDe("getSaime");
		if(empty($getSaime)) $errores['cedula'] = $data['mensaje'] ='falta el número de cédula';
		if(empty($set)) 		$set 		= traerValorDe("set");
		if(empty($espera)) 	$espera = traerValorDe("espera");
		if(!empty($espera)) sleep($segundos);
		
		if(empty($errores)){
			$cedula = cedulaFormat($getSaime);
			if($cedula['letra'] ==false || $cedula['numero'] ==false){
				$errores['rif'] = $data['mensaje'] ='la cédula es incomprensible';
			}
		}

		if(empty($errores) && !$set){
			$tb = "saime";  // self::getBD('Tb')[0];
			$sql ="
			SELECT * FROM $tb WHERE 
					  letra = '{$cedula['letra']}'
				AND cedula = {$cedula['numero']}";
//echo $sql;die();
			$resultado = DbLink::resultados($sql,self::getBD());
			if($resultado !==false){
				if($fecha->getTimestamp() - $resultado[0]['fecha'] < 31536000){  // 31536000 un año
					$data = $resultado[0];
				}
			}
		}

		if(empty($errores) && empty($data)){

			$serverURL = 'https://150.188.14.11/services/ConsultaPnsii?wsdl';
			//$serverURL = 'https://150.188.14.7/services/ConsultaPNSIISeguro?wsdl';
			$opts = array(
//				'http'=>array('user_agent' => 'PHPSoapClient'),
				'ssl' => array('ciphers'=>'RC4-SHA', 'verify_peer'=>false, 'verify_peer_name'=>false));
			$context = stream_context_create($opts);
			$local_cert = $_SERVER["DOCUMENT_ROOT"] . "/privado/cnti.jks";
			$llaves = $_SERVER["DOCUMENT_ROOT"]."/privado/cnti";
			$passphrase =  file($llaves, FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES)[0];
			$params = array(

'encoding' => 'UTF-8', 
'verifypeer' => false, 
'verifyhost' => false, 
'soap_version' => SOAP_1_2, 
'trace' => 1, 
'exceptions' => 1, 
"connection_timeout" => 180, 

				'stream_context' => $context,
				'cache_wsdl' => 'WSDL_CACHE_NONE', 
				'local_cert'=>  $local_cert,
				'passphrase'=>$passphrase
			);
//			libxml_disable_entity_loader(false);

			try {
			  $client = new SoapClient($serverURL,$params);
			} catch (SoapFault $e) {
		    var_dump(libxml_get_last_error());
		    $errores['cedula'] = $data['mensaje'] =$e;
			}

//			$data['set'] = loadDataTable(self::getBD(), self::getBD('Tb')[0], $toBD);
//			$data['estatus'] = '1';

//TODO ¿En qué caso?	$errores['url'] = $data['mensaje'] = 'No se pudo obtener la información';
		}

		if(empty($errores)){
//			$data = $client->__getFunctions();
			$data = $client->consultaPorSujeto(array('clave' => $cedula['numero'],'institucion' => 'saime'));
		}

		if(!empty($errores)){
			$data['estatus'] 		= '0';
			$data['error']  = $errores;
		}

		if(strpos("$_SERVER[REQUEST_URI]", __CLASS__."/".__FUNCTION__) !== false) return entregar($data); else return $data;
	}


// fin clase cnti
}

