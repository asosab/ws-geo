<?php 

function vivss($frase, $pista){
	$pos = strpos($frase, $pista);
	if($pos == false) return false;
	$vi = strpos($frase, "<td", $pos);
	$vi = strpos($frase, ">", $vi) + 1;
	$vf = strpos($frase, "</td>",$vi);
	$v = trim(substr($frase, $vi, $vf - $vi ));
	return $v;
}

class ivss {
	/**
	 * getBD
	 * retorna la base de datos en uso.
	 * @return string con nombre de la bd
	 */
	public static function getBD($dar='BD'){
		if($dar=='BD') return "BD";
		if($dar=='Tb') return array('ivss_persona','ivss_per_emp','ivss_empresa');
	}

	/**
	 * accesoDef
	 * valores por defecto de acceso a cada método.
	 * @param string $metodo el nombre del método a validar 
	 * @return int nivel  de acceso
	 */
	public static function accesoDef($metodo){
		switch ($metodo) {
		    case "getBD": 			return 1; break;
		    case "total": 			return 1; break;
		    case "analyze": 		return 1; break;
		    case "accesoDef": 	return 1; break;
		    default: 						return 1; 
		}
	}

	/**
	 * getDato
	 * Extrae un dato del html de una página específica basandose en una pista
	 * @param $html string el cuerpo de la página Web
	 * @param $pista string un texto que sirve para ubicar el texto buscado
	 * @return string con cadena buscada
	 */
	public static function getDato($frase, $pista){ //vivss
		$pos = strpos($frase, $pista);
		if($pos == false) return false;
		$vi = strpos($frase, "<td", $pos);
		$vi = strpos($frase, ">", $vi) + 1;
		$vf = strpos($frase, "</td>",$vi);
		$v = trim(substr($frase, $vi, $vf - $vi ));
		return $v;
	}

	/**
   * getDatos
   * trae datos de ivss
   * @param String $rif
   * @return Json
   * @throws Exception
   */
  public static function getDatos($letra=null, $cedula=null, $set=null, $fuente=null) {
		$data 		= array(); 

		if($letra==null)           $letra = valorde("letra");
		if($letra == "")           $errores['letra']   = 'Falta la nacionalidad';
		if($cedula==null)          $cedula = valorde("cedula");
		if($cedula == "")          $errores['cedula']  = 'Falta la cédula de identidad';
		if(!is_numeric($cedula))   $errores['cedula']  = 'Se esperaba un número';

		if($fuente==null) 	$fuente = valorde("fuente");
		if($fuente== "")		$fuente = "auto";
  	if($fuente == "web") $data = ivss::getIVSS($letra, $cedula, $set);
  	if($fuente == "local") $data = ivss::getLocal($letra, $cedula);
		if($fuente == "auto"){
			$data = ivss::getLocal($letra, $cedula);
			$local = ivss::getLocal($letra, $cedula);
			if($data['estatus']=='1') $ano = explode("-", $data['fecha_actualizacion'])[0];
			else $ano = "2015";
			if($ano !== date("Y")){
				$data = ivss::getIVSS($letra, $cedula, $set);
				if($data["estatus"] != '1'){
					$data = $local;
					if($set == '1') $data['set']['error'] = "no hay fuentes de actualización";
				} 
			}
		}
		if(isset($data['empresas'])){
			foreach ($data['empresas'] as $key => $value){
				$tmp = self::getEmpresa($value['npatronal']);
				$data['empresas'][$key] = array_merge($data['empresas'][$key], $tmp);
			}
		}
		if(strpos("$_SERVER[REQUEST_URI]", __CLASS__."/".__FUNCTION__) !== false)return entregar($data); else return $data;
		
  }


	/**
	 * getLocal
	 * Extrae datos de personas de la base de dtos local
	 * @param $cedula string el numero de cédula de la persona a buscar
	 * @return array con datos de la persona
	 */
	public static function getLocal($letra=null, $cedula=null){
			$errores    = array();
			$data       = array();
			$data["estatus"] = '0';
			$tper = self::getBD('Tb')[0];
			$tperEmp = self::getBD('Tb')[1];

			if($letra==null)           $letra = valorde("letra");
			if($letra == "")           $errores['letra']   = 'Falta la nacionalidad';
			if($cedula==null)          $cedula = valorde("cedula");
			if($cedula == "")          $errores['cedula']  = 'Falta la cédula de identidad';
			if(!is_numeric($cedula))   $errores['cedula']  = 'Se esperaba un número';

			if (empty($errores)){
				$sql="select * from $tper where cedula=$cedula AND nacionalidad='$letra'";
				$resultado = DbLink::resultados($sql,ivss::getBD());
				if($resultado !== false){
						$data = $resultado[0];
						$data["estatus"] = '1';
				}else{
					$errores['cedula']  = 'No se encontró registro local';
				}
			}
			if (empty($errores)){
				$sql="select npatronal, empresa, fingreso, fegreso, salario, estatusa from $tperEmp where cedula=$cedula AND nacionalidad='$letra'";
				$resultado = DbLink::resultados($sql,ivss::getBD());
				if($resultado !== false){
						$data['empresas'] = $resultado;
						$data["estatus"] = '1';
				}else{
					$errores['empresas']  = 'No se encontraron empresas';
				}
			}else{
				$data['errores']  = $errores;
			}

			if(strpos("$_SERVER[REQUEST_URI]", __CLASS__."/".__FUNCTION__) !== false)return entregar($data); else return $data;
	}

	/**
	 * getEmpByPer
	 * Retorna datos de empresas por la cedulade su representante 
	 * @param $getEmpresa string el numero patronal de la empresa
	 * @return array con datos de la persona
	 */
	public static function getEmpByPer($getEmpByPer=null){
		$errores = array();
		$data    = array();
		if(empty($getEmpByPer)) $getEmpByPer = traerValorDe("getEmpByPer");
		if(empty($getEmpByPer)) $errores['cedula'] = $data['mensaje'] = 'Falta la cédula';
		if(!empty($getEmpByPer)) $cedula = cedulaFormat($getEmpByPer);
		if(empty($errores)){
			$tempresa = self::getBD('Tb')[2];
			$where = array();
			$where[] = "cedula = {$cedula['numero']}";
			if(!empty($cedula['letra'])) $where[] = "letra = '{$cedula['letra']}'";
			$wherel = implode(" AND ", $where);
			$sql = "SELECT * FROM $tempresa WHERE $wherel";
			$resultado = DbLink::resultados($sql,self::getBD());
			if($resultado !== false){
				$data = $resultado;
				$data["estatus"] = '1';
			}else $errores['cedula'] = $data['mensaje'] = 'cero empresas coinciden';
		}
		if(!empty($errores)){
			$data["estatus"] = '0';
			$data['errores']  = $errores;
		}
		if(strpos("$_SERVER[REQUEST_URI]", __CLASS__."/".__FUNCTION__) !== false)return entregar($data); else return $data;
	}

	/**
	 * getEmpresa
	 * Busca datos de empresas 
	 * @param $getEmpresa string el numero patronal de la empresa
	 * @return array con datos de la persona
	 */
	public static function getEmpresa($getEmpresa=null, $set=null){
		$errores = array();
		$data    = array();

		if(empty($getEmpresa)) $getEmpresa = traerValorDe("getEmpresa");
		if(empty($getEmpresa)) $errores['patronal'] = $data['mensaje'] = 'Falta el código patronal';
		if(!empty($getEmpresa)) $patronal = strtoupper($getEmpresa);
		if(empty($set)) $set = traerValorDe("set");
//if($ano !== date("Y"))

		if(empty($errores)){
			if(!$set){
				$tempresa = self::getBD('Tb')[2];
				$sql = "SELECT * FROM $tempresa WHERE patronal = '$patronal'";
				$resultado = DbLink::resultados($sql,self::getBD());
				if($resultado !== false){
					$ano = date('Y', strtotime($resultado[0]['fecha_actualizacion']));
					if(date('Y', strtotime($resultado[0]['fecha_actualizacion'])) < date("Y"))$set = true;
					else{
						$data = $resultado[0];
						$data["estatus"] = '1';
					} 
				}else $set = true;
			}
			if($set){
				$post = array();
				//TODO obtener fecha
				$url = "http://www.ivss.gob.ve:28081/consultasIntra/consultarEmpresaAccion.ivss?numero_empresa=$patronal";
				$respuesta = Curl_lib::post($url, $post);
				$cuerpo = utf8_encode(implode(" ",$respuesta['body']));

				$tmp = scrapDato($cuerpo, "mero Patronal:", "td"); if($tmp) $data['patronal'] = limpiar($tmp);
				$tmp = scrapDato($cuerpo, "Nombre Empresa:", "td"); if($tmp) $data['nombre'] = limpiar($tmp);
				$tmp = scrapDato($cuerpo, "n:", "td"); if($tmp) $data['direccion'] = limpiar($tmp);
				$tmp = scrapDato($cuerpo, "fono:", "td"); 
						if($tmp) $data['telefono'] = "+58".limpiarEntero(limpiar($tmp));
				$tmp = scrapDato($cuerpo, "Numero de RIF:", "td"); 
				if($tmp){
					$tmp = limpiar($tmp);
					$tmp = substr($tmp, 0, 10);  // bcd
					$data['rif'] = limpiar($tmp);
				} 
				$tmp = scrapDato($cuerpo, "Riesgo:", "td"); if($tmp) $data['riesgo'] = limpiar($tmp);
				$tmp = scrapDato($cuerpo, "C.I Representante:", "td");
				if($tmp){
					$cf = cedulaFormat(limpiar($tmp));
					$data['letra'] = $cf['letra'];
					$data['cedula'] = $cf['numero'];
				} 
				$tmp = scrapDato($cuerpo, "TOTAL DEUDA", "td"); if($tmp) $data['deuda'] = getAmount(limpiar($tmp));
				$data['fecha_actualizacion'] = date("Y-m-d H:i:s");

				if(isset($data["patronal"])){ 
					$data["estatus"] = '1';
				}else{
					$data["estatus"] = '0'; 
					$errores['web'] = $data['mensaje'] = 'No se encontró la página';
				}
			}
		}

		if($data["estatus"]=='1' && $set){
			$data["set"] = loadDataTable(self::getBD(), self::getBD('Tb')[2], $data);
			//TODO registrar cedula de representante
		}

		if(!empty($errores)){
			$data["estatus"] = '0';
			$data['errores']  = $errores;
		}

		if(strpos("$_SERVER[REQUEST_URI]", __CLASS__."/".__FUNCTION__) !== false)return entregar($data); else return $data;
	}

	/**
	 * getIVSS
	 * Extrae datos de personas 
	 * @param $cedula string el numero de cédula de la persona a buscar
	 * @return array con datos de la persona
	 */
	public static function getIVSS($letra=null, $cedula=null, $set=null){
		$errores = array();
		$data    = array();

		if($letra==null)          $letra = valorde("letra");
		if($letra == "")          $errores['letra']   = 'Falta la nacionalidad';
		if($letra ."" != "") 			$letra = strtoupper($letra);
		if($set==null)          	$set = valorde("set");
		if($set== "")       			$set = "1";
		if($cedula==null)         $cedula = valorde("cedula");
		if($cedula == "")         $errores['cedula']  = 'Falta la cédula de identidad';
		if(!is_numeric($cedula))  $errores['cedula']  = 'cédula: se esperaba un número';
		if (empty($errores)){
			$sql = "SELECT fechanac from saime where cedula=$cedula AND letra = '$letra'";
			$resultado = DbLink::resultados($sql,ivss::getBD());
			if($resultado !== false){
				if($resultado[0]['fechanac'] . "" !=""){
					list($ano, $mes, $dia) = explode("-", $resultado[0]['fechanac']);
				}else{
					$errores['fecha'] = "No tiene fecha de nacimiento en BD local";
				}
			}else{
				$errores['cedula'] = "Persona no encontrada";
			}
		}
		if (empty($errores)){
			$post = array();
			//TODO obtener fecha
			$url = "http://www.ivss.gob.ve:28083/CuentaIndividualIntranet/CtaIndividual_PortalCTRL?cedula_aseg=$cedula&d=$dia&m=$mes&nacionalidad_aseg=$letra&y=$ano";
			$respuesta = Curl_lib::post($url, $post);
			$result = utf8_encode(implode(" ",$respuesta['body']));
			//echo $result;die();
			$tmp = vivss($result, "Nombre y Apellido"); 		if($tmp) $data["nombre"] 		= $tmp;
			$tmp = vivss($result, "Sexo"); 									if($tmp) $data["sexo"] 			= $tmp[0]; 
			$tmp = vivss($result, "Fecha de Nacimiento"); 	if($tmp) $data["fechana"] 	= $tmp;
			$tmp = vivss($result, "mero Patronal"); 				if($tmp) $data["npatronal"] = $tmp;
			$tmp = vivss($result, "Nombre Empresa"); 				if($tmp) $data["empresa"] 	= $tmp;
			$tmp = vivss($result, "Fecha de Ingreso"); 			if($tmp) $data["fingreso"] 	= $tmp;
			$tmp = vivss($result, "Fecha de Egreso"); 			if($tmp) $data["fegreso"] 	= $tmp;
			$tmp = vivss($result, "ltimo Salario"); 				if($tmp) $data["salario"] 	= $tmp;
			$tmp = vivss($result, "Estatus del Asegurado"); if($tmp) $data["estatusa"] 	= $tmp;
			$tmp = vivss($result, "Fecha Primera Afilia"); 	if($tmp) $data["fafilia"] 	= $tmp;
			$tmp = vivss($result, "Fecha Contingencia"); 		if($tmp) $data["fcontin"] 	= $tmp;
			$data["letra"] = $letra;
			$data["cedula"] = $cedula;
			//print_r($data);die();
			if(isset($data["npatronal"])){ 
				$data["letra"] = $letra;
				$data["cedula"] = $cedula;
				$data["estatus"] = '1';
			}
			else{$data["estatus"] = '0'; $data['error'] = 'no se encontró la página';}
		}else{
			$data["estatus"] = '0';
			$data['errores']  = $errores;
		}
		if($data["estatus"]=='1' && $set == '1'){
			$data['set'] = ivss::setIVSS($data);
			if(!empty($data["sexo"])) loadDataTable(self::getBD(), "saime", $data);
		}
		if(strpos("$_SERVER[REQUEST_URI]", __CLASS__."/".__FUNCTION__) !== false)return entregar($data); else return $data;
	}


	/**
	 * setIVSS
	 * @param $datos
	 * @return array de datos de persona
	 */
	public static function setIVSS($datos=null){
		$errores = array();
		$data = array();
		$tper = self::getBD('Tb')[0];
		$tperEmp = self::getBD('Tb')[1];
		if($datos==null) $errores['datos'] 	= 'No hay datos para actualizar';
		if($datos['estatus']!="1") $errores['cedula'] 	= 'El número de cédula no ha sido encontrado';
		if(!isset($datos['npatronal'])) $errores['npatronal'] 	= 'Falta el número patronal';

    if (!empty($errores)){
        $data['estatus'] = '0';
        $data['errores']  = $errores;
    }else{
			$fingreso = isset($datos['fingreso'])? "'". date("Y-m-d", strtotime($datos["fingreso"])) . "'":"null";
			$fegreso = isset($datos['fegreso'])? "'". date("Y-m-d", strtotime($datos["fegreso"])) . "'":"null";
			$fafilia = isset($datos['fafilia'])? "'". date("Y-m-d", strtotime($datos["fafilia"])) . "'":"null";
			$fcontin = isset($datos['fcontin'])? "'". date("Y-m-d", strtotime($datos["fcontin"])) . "'":"null";
			$fechanac = isset($datos['fechanac'])? "'". date("Y-m-d", strtotime($datos["fechanac"])) . "'":"null";
			$estatusa = substr($datos['estatusa'], 0, 1);
			$salario = str_replace(".", "", $datos['salario']);
			$salario = str_replace(",", ".", $salario);
			if(!isset($datos['fafilia']) && isset($datos['fcontin'])) $fafilia = $fcontin;
    	//verificar si la persona ya esta registrada en IVSS y agregarla si no
    	$sql = "select cedula from $tper 
    		where cedula = {$datos['cedula']}
    		AND nacionalidad = '{$datos['letra']}'";
    	$resultado = DbLink::resultados($sql,ivss::getBD());
    	if($resultado !== false){ 
    		// sin update por ahora
    		$data['set'] = loadDataTable(ivss::getBD(), "saime", $datos);
    	}else{ //insert
    		$actualizado = date("Y-m-d H:i:s");
    		$sql = "
    			INSERT INTO $tper (cedula, nacionalidad, fafilia, fecha_actualizacion)
					VALUES ({$datos['cedula']}, 
									'{$datos['letra']}',
									$fafilia,
									'$actualizado'
					)
					RETURNING cedula;
    		";
				$resultado = DbLink::resultados($sql,ivss::getBD());
				if($resultado !== false){
					$data['ivss']['insert'] = '1';
				}else{
					$data['ivss']['insert'] = '0';
				}
    	}
    	//verificar si el numero patronal ya esta registrado y agregarlo si no
    	$sql = "select cedula from $tperEmp 
    		where cedula = {$datos['cedula']}
    		AND nacionalidad = '{$datos['letra']}'
    		AND npatronal = '{$datos['npatronal']}'
    	";
    	$resultado = DbLink::resultados($sql,ivss::getBD());
    	if($resultado !== false){ // hacer update
    		$actualizado = date("Y-m-d H:i:s");
				$sql = "
						UPDATE $tperEmp 
						SET 
							salario = $salario,
							fegreso = $fegreso
						where cedula = {$datos['cedula']}
			    		AND nacionalidad = '{$datos['letra']}'
			    		AND npatronal = '{$datos['npatronal']}'
						RETURNING cedula;
				";
				$resultado = DbLink::resultados($sql,ivss::getBD());
				if($resultado !== false){
					$data['ivss_empresa']['update'] = '1';
				}else{
					$data['ivss_empresa']['update'] = '0';
				}
				$data['set'] = loadDataTable(ivss::getBD(), "saime", $datos);
    	}else{ //insert
    		$actualizado = date("Y-m-d H:i:s");
    		$empresa = pg_escape_string($datos['empresa']);
    		$sql = "
    			INSERT INTO $tperEmp (cedula, nacionalidad, npatronal, empresa, fingreso, fegreso, salario, estatusa)
					VALUES ({$datos['cedula']}, 
									'{$datos['letra']}',
									'{$datos['npatronal']}',
									'{$empresa}',
									$fingreso,
									$fegreso,
									$salario,
									'$estatusa'
					)
					RETURNING cedula;
    		";
    		$data['set'] = loadDataTable(ivss::getBD(), "saime", $datos);
				$resultado = DbLink::resultados($sql,ivss::getBD());
				if($resultado !== false){
					$data['ivss_empresa']['insert'] = '1';
				}else{
					$data['ivss_empresa']['insert'] = '0';
				}
    	}
		}
		return $data;
	}



// fin clase ivss
}

?>