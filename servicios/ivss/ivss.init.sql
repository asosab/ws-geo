
CREATE TABLE ivss_persona
(
  cedula bigint NOT NULL,
  nacionalidad character varying(1) NOT NULL,
  fafilia date NOT NULL,
  fecha_actualizacion date,
  CONSTRAINT ivss_nacionalidad_cedula_pkey PRIMARY KEY (nacionalidad, cedula)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE ivss OWNER TO postgres;
GRANT ALL ON TABLE ivss TO postgres;
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE ivss TO data;


CREATE TABLE ivss_per_emp
(
  cedula bigint NOT NULL,
  nacionalidad character varying(1) NOT NULL,
  npatronal character varying(11) NOT NULL,
  empresa character varying(512) NOT NULL,
  fingreso date,
  fegreso date,
  salario money,
  estatusa character(1) NOT NULL,
  CONSTRAINT ivss_empresa_nacionalidad_cedula_patron_pkey PRIMARY KEY (nacionalidad, cedula, npatronal)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE ivss_empresa OWNER TO postgres;
GRANT ALL ON TABLE ivss_empresa TO postgres;
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE ivss_empresa TO data;

CREATE TABLE ivss_empresa(
  patronal character varying(11) NOT NULL,
  nombre text NOT NULL,
  direccion text NOT NULL,
  telefono character varying(15),
  rif character varying(11) NOT NULL,
  riesgo character varying(20),
  letra character varying(1) NOT NULL,
  cedula bigint NOT NULL,
  deuda money, 
  fecha_actualizacion date,
  CONSTRAINT ivss_empresa_patronal_pkey PRIMARY KEY (patronal)
)
WITH ( OIDS=FALSE);
ALTER TABLE ivss_empresa OWNER TO postgres;
GRANT ALL ON TABLE ivss_empresa TO postgres;
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE ivss_empresa TO data;
