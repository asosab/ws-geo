CREATE TABLE inces_solvencia
(
  rif character varying(12) NOT NULL, 
  fecha bigint NOT NULL,
  data jsonb NOT NULL,                          -- datos recabados de servicios web
  CONSTRAINT inces_solvencia_pkey PRIMARY KEY (rif)
)
WITH (OIDS=FALSE);
ALTER TABLE inces_solvencia OWNER TO postgres;
GRANT ALL ON TABLE inces_solvencia TO postgres;
GRANT ALL ON TABLE inces_solvencia TO data;