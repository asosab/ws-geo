<?php 

class inces {

	/**
	 * accesoDef
	 * valores por defecto de acceso a cada método.
	 * @param string $metodo el nombre del método a validar 
	 * @return int nivel  de acceso
	 */
	public static function accesoDef($metodo){
		switch ($metodo) {
		    case "getBD": 			return 1; break;
		    case "total": 			return 1; break;
		    case "analyze": 		return 1; break;
		    case "accesoDef": 	return 1; break;
		    case "getDato": 		return 1; break;
		    case "getCron": 		return 1; break;
		    case "test": 				return 1; break;
		    default: 						return 2; 
		}
	}

	/**
	 * getBD
	 * retorna la base de datos en uso.
	 * @return string con nombre de la bd
	 */
	public static function getBD($dar='BD'){
			if($dar=='BD') return "BD";
			if($dar=='Tb') return array('inces_solvencia');
	}

	public static function test(){
		echo date('h:i:s') . "<br>";
		//sleep for 5 seconds
		usleep(5000000);
		//start again
		echo date('h:i:s');
	}

	/**
	 * getCron
	 * Tareas a ejecutar por crontab
	 * @return array tareas a ejecutar
	 */
	public static function getCron(){ 
		$serializer = new SuperClosure\Serializer();
		$fn = function(){self::setRNCNext();};
		$wbase = "http://" . $_SERVER['SERVER_NAME'];
//		$wbase = "http://ws/";
		$fn = "wget -O /dev/null $wbase/inces/setINCESNext/";
		$tareas = array();
		$tarea1 = array(
			'nombre'	=>'setINCESNext',
			'command'	=> $fn, // function(){return self::setRNCNext();},  //  'rnc::setRNCNext()', // $FnS, // 
			'schedule'=>'* * * * *',
			'output'	=>'',  // /home/asosab/www/ws/rnc.json
			'enabled'	=> false,
		);
		$nombre = $tarea1['nombre'];
		for ($x =0; $x <= 9; $x++){$tarea1['nombre'] = $nombre . $x;$tareas[] = $tarea1;};
		return $tareas;
	}

		/**
		 * total
		 * Total registros relacionados
		 * @return array de datos de persona
		 */
		public static function total(){
			$data = array();
			foreach (rnc::getBD('Tb') as $tabla) $data[$tabla] = totalRegBDTable($tabla, rnc::getBD());
			if(strpos("$_SERVER[REQUEST_URI]", __FUNCTION__) !== false)return entregar($data); else return $data;
		}

		/**
		 * analyze
		 * Actualiza las estadísticas de cada tabla
		 * @return array de datos de persona
		 */
		public static function analyze(){
			$data = array();
			foreach (rnc::getBD('Tb') as $tabla) $data[$tabla] = analyzeBDTable($tabla, rnc::getBD());
			if(strpos("$_SERVER[REQUEST_URI]", __FUNCTION__) !== false)return entregar($data); else return $data;
		}


	/**
	 * getSolvencia
	 * Obtiene estatus de solvencia INCES de empresas.
	 * 
	 * @param string $getSolvencia puede ser un rif, un nombre de empresa o su identificador ante INCES 
	 * @param string $set si es 1 forza la búsqueda en Internet y guarda el resultado localmente
	 * @param string $espera agrega demora de entre 0 y 10 segundos antes de iniciar la petición
	 * @return array datos de empresa según INCES
	 */
	public static function getSolvencia($getSolvencia=null, $set=null, $espera=false){
		$errores    = array();
		$data       = array();
		$segundos = rand(0, 10);
		$fecha = new DateTime();

		if(empty($getSolvencia)) $getSolvencia 	= traerValorDe("getSolvencia");
		if(empty($getSolvencia)) $errores['rif'] = $data['mensaje'] ='falta la empresa a buscar';
		if(empty($set)) 		$set 		= traerValorDe("set");
		if(empty($espera)) 	$espera = traerValorDe("espera");
		if(!empty($espera)) sleep($segundos);
		$str = $getSolvencia;
		
		if(empty($errores) && !$set){
			// verificar si se ha guardado el registro dentro de los últimos 15 días, en ese caso sacarlo de la bd
			$str = remove_accents(strtolower($getSolvencia));
			$tbSolv = self::getBD('Tb')[0];
			$sql ="
			SELECT * FROM $tbSolv WHERE 
					 data @> '{\"naport\":\"$str\"}'
				OR data @> '{\"ncert\":\"$str\"}'
				OR lower(sp_ascii(data::json->>'rif')) LIKE '%$str%'
				OR lower(sp_ascii(data::json->>'razsoc')) LIKE '%$str%'
			ORDER BY fecha DESC LIMIT 1";
//echo $sql;die();
			$resultado = DbLink::resultados($sql,self::getBD());
			if($resultado !==false){
				if($fecha->getTimestamp() - $resultado[0]['fecha'] < 1296000){
					$json = $resultado[0]['data'];
					$data = json_decode($json,true);
				}
			}
		}

		if(empty($errores) && empty($data)){
			$url = "http://ws.inces.gob.ve/s/todo/$str/";
			$json = file_get_contents($url);
//echo $json; die();
			$data = json_decode($json, true);
			if(empty($data['error'])){
				$data['razsoc'] = trim($data['razsoc']);
				$data['razsoc'] = str_replace('  ', ' ', $data['razsoc']);
				$data['razsoc'] = str_replace(array("\n", "\t", "\r"), '', $data['razsoc']);
				$json = json_encode($data);
				$toBD = array(
						"rif" => $data['rif'],
						"fecha" => $fecha->getTimestamp(),
						"data" => $json
					);
				$data['set'] = loadDataTable(self::getBD(), self::getBD('Tb')[0], $toBD);
				$data['estatus'] = '1';
			} 
			else $errores['url'] = $data['mensaje'] = 'No se pudo obtener la información';
		}

		if(!empty($errores)){$data['estatus'] = '0';$data['error'] = $errores;}
		if(strpos("$_SERVER[REQUEST_URI]", __CLASS__."/".__FUNCTION__) !== false) return entregar($data); else return $data;
	}


// fin clase INCES
}
