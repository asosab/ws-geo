<?php 

class seniat {

	/**
	 * getBD
	 * retorna la base de datos en uso.
	 * @return string con nombre de la bd
	 */
	public static function getBD(){
		return "BD";  //  "BD_desa"; // 
	}

	/**
	 * accesoDef
	 * valores por defecto de acceso a cada método.
	 * @param string $metodo el nombre del método a validar 
	 * @return int nivel  de acceso
	 */
	public static function accesoDef($metodo){
		switch ($metodo) {
		    case "getBD": 			return 1; break;
		    case "total": 			return 1; break;
		    case "analyze": 		return 1; break;
		    case "accesoDef": 	return 1; break;
		    default: 						return 1; 
		}
	}

	/**
	 * setDatos
	 * @param $datos obtenidos de seniat 
	 * @return array de datos de seniat
	 */
	public static function setDatos($datos=null){
		$errores        = array();
		$data           = array();

		if($datos==null) $errores['datos'] 	= 'No hay datos para actualizar';
		if($datos['estatus']!="1") $errores['rif'] 	= 'El RIF no ha sido encontrado';
		if(!isset($datos['rif'])) $errores['rif'] 	= 'El RIF no ha sido seteado';

    if (!empty($errores)){
        $data['estatus'] = '0';
        $data['errores']  = $errores;
    }else{
    	$rif = $datos['rif'];
	    $actualizado = date("Y-m-d H:i:s");

    	//verificar si existe en BD
    	$sql = "select rif from seniat where rif='$rif'";
    	$resultado = DbLink::resultados($sql,seniat::getBD());
			if($resultado !== false){
				$sql = "
					UPDATE seniat 
					SET 
						nombre   						= '{$datos['nombre']}',
						retieneiva   				= '".substr($datos['agenteretencioniva'],0,1) ."',
						conribuyeiva  			= '".substr($datos['contribuyenteiva'],0,1) ."',
						tasa  							= {$datos['tasa']},
						fecha_modificacion 	= '$actualizado'
					WHERE rif  = '$rif'
					RETURNING rif;
				";
				//echo $sql; die();
				$resultado = DbLink::resultados($sql,seniat::getBD());
				$data['tipo'] = 'UPDATE';
			}else{
				$nombreEsc = pg_escape_string($datos['nombre']);
				$sql = "
					INSERT INTO seniat (rif,nombre, retieneiva, conribuyeiva,tasa,fecha_modificacion)
					VALUES(
						'$rif',
						'{$nombreEsc}',
						'".substr($datos['agenteretencioniva'],0,1) ."',
						'".substr($datos['contribuyenteiva'],0,1) ."',
						{$datos['tasa']},
						'$actualizado'
					 )
					 RETURNING rif; 
				";
				$resultado = DbLink::resultados($sql,seniat::getBD());
				$data['tipo'] = 'INSERT';
			}

			if($resultado !== false){
	        	$data['estatus'] = '1';
			}else{
				$data['estatus'] = '0';
				$errores['BD'] = 'Error al registrar los datos';
				$errores['sql'] = $sql;
	      $data['errores']  = $errores;
			}
		}
		return $data;
	}


	/**
   * getDatos
   * trae datos de seniat a partir de un rif
   * @param String $rif
   * @return Json
   * @throws Exception
   */
  public static function getDatos($RIF=null, $set=null, $fuente=null) {
		$data 		= array(); 
		if($fuente==null) 	$fuente = valorde("fuente");
		if($fuente== "")		$fuente = "auto";
  	if($RIF==null) 			$RIF = valorde("RIF");
  	if($fuente == "seniat") $data = seniat::getSeniat($RIF,$set);
  	if($fuente == "local") $data = seniat::getLocal($RIF);
		if($fuente == "auto"){
			$data = seniat::getLocal($RIF);
			$local = seniat::getLocal($RIF);
			if($data['estatus']=='1') $ano = explode("-", $data['fecha_modificacion'])[0];
			else $ano = "2013";
			if($ano !== date("Y")){
				$data = seniat::getSeniat($RIF,$set);
				if($data["estatus"] != '1'){
					$data = $local;
					if($set == '1') $data['set']['error'] = "no hay fuentes de actualización";
				} 
			}
		}
		if($data["estatus"]=='1' && $set == '1' && $data['fuente'] !='local'){
			$data['set'] = seniat::setDatos($data);
		}

		if(strpos("$_SERVER[REQUEST_URI]", __FUNCTION__) !== false){
			return entregar($data);
		}else{
			return $data;
		}
  }


	/**
	 * getLocal
	 * Extrae datos de personas de la base de dtos local
	 * @param $cedula string el numero de cédula de la persona a buscar
	 * @return array con datos de la persona
	 */
	public static function getLocal($RIF=null){
		$errores	= array();
		$data 		= array();
		$data["estatus"] = '0';

		if($RIF==null) $RIF = valorde("RIF");
		if($RIF == "") $errores['rif'] 	= 'Falta el RIF';
		if (!seniat::validarRIF($RIF)['valido']) $errores['rif'] 	= 'Rif Inexistente o Inválido';

		if (empty($errores)){
			$sql="select * from seniat where rif='$RIF'";
			$resultado = DbLink::resultados($sql,seniat::getBD());
			if($resultado !== false){
				$data = $resultado[0];
				$data["estatus"] = '1';
				$data['fuente'] ='local';
			}
		}else{
      $data['errores']  = $errores;
		}

		if(strpos("$_SERVER[REQUEST_URI]", __FUNCTION__) !== false){
			return entregar($data);
		}else{
			return $data;
		}
	}

	/**
   * getSeniat
   * trae datos de seniat a partir de un rif
   * @param String $rif
   * @return Json
   * @throws Exception
   */
  public static function getSeniat($RIF=null, $set=null) {
		$errores 	= array();
		$data 		= array();  	
		if($set==null) 	$set = valorde("set");
		if($set== "")		$set = "1";
  	if($RIF==null) 	$RIF = valorde("RIF");
    if (seniat::validarRIF($RIF)['valido']) {
      if(function_exists('curl_init')) {
    		$url = "http://contribuyente.seniat.gob.ve/getContribuyente/getrif?rif=$RIF";
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        $result = curl_exec ($ch);

        if ($result) {
            try {
                if(substr($result,0,1)!= '<' ) {
                    throw new Exception($result);
                }
                $xml = simplexml_load_string($result);
                if(!is_bool($xml)) {
                    $elements = $xml->children('rif');
                    //$seniat = array();
                    $data['rif'] = $RIF;
                    foreach($elements as $key => $node) {
                        $index = strtolower($node->getName());
                        $data[$index] = (string)$node;
                    }
                    $data['estatus'] = '1';
                    $data['fuente'] = 'seniat';
                }
            } catch(Exception $e) {
                $exception = explode(' ', $result, 2);
                $data['estatus'] =(string) $exception[0];
            }
        } else {
            // No hay conexión a internet
            $data['estatus'] = '0';
            $data['error'] = 'Sin Conexión a Internet';
        }
      } else {
          // No hay soporte CURL
          $data['estatus'] = '-1';
          $data['error'] = 'No Existe Soporte Curl';
      }
	  } else {
	      // Formato de RIF inválido
	      $data['estatus'] = '-2';
	      $data['error'] = 'Rif Inexistente o Inválido';
	  }

		if($data["estatus"]=='1' && $set == '1'){
			
			$data['set'] = seniat::setDatos($data);
		}

		if(strpos("$_SERVER[REQUEST_URI]", __FUNCTION__) !== false){
			return entregar($data);
		}else{
			return $data;
		}
  }


	/**
	 * toRIF
	 * Retorna el RIF correspondiente a una cédula 
	 * @param $letra string La letra correspondiente a nacionalidad o tipo de rif
	 * @param $cedula string el numero de cédula de la persona
	 * @return string con el RIF
	 */
	public static function toRIF($letra=null, $cedula=null){
		$errores 	= array();
		$data 		= array();
		$data['rif'] = "";

		if($letra==null) 					$letra = valorde("letra");
		if($letra == "") 					$errores['letra'] 	= 'Falta la nacionalidad';
		if($cedula==null) 				$cedula = valorde("cedula");
		if($cedula == "") 				$errores['cedula'] 	= 'Falta la cédula de identidad';

		if (empty($errores)){
			if(strlen($cedula)==2) $cedula = "000000$cedula";
			if(strlen($cedula)==3) $cedula = "00000$cedula";
			if(strlen($cedula)==4) $cedula = "0000$cedula";
			if(strlen($cedula)==5) $cedula = "000$cedula";
			if(strlen($cedula)==6) $cedula = "00$cedula";
			if(strlen($cedula)==7) $cedula = "0$cedula";

			$data['letra'] = $letra;
			$data['cedula'] = $cedula;
			$este = "";
			for ($x = 0; $x <= 9; $x++) {
				$este = "$letra$cedula$x";
	    	if(seniat::validarRIF($este)['valido']) $data['rif'] = strtoupper($este);
			} 
		}else{
      $data['errores']  = $errores;
		}

		if(strpos("$_SERVER[REQUEST_URI]", __FUNCTION__) !== false){
			return entregar($data);
		}else{
			return $data;
		}
	}

  /**
   * formatoRIF
   * da formato de rif a una frase
   * @return array
   */
  public static function formatoRIF($formatoRIF=null) {
		$errores 	= array();
		$data    	= array();
  	if(empty($formatoRIF)) $formatoRIF = traerValorDe("formatoRIF");
  	if(empty($formatoRIF)) $errores['RIF'] = $data['mensaje'] = 'Falta el RIF';
  	if(!empty($formatoRIF)) $RIF = $formatoRIF;

  	if(empty($errores)){
	  	$separador = array(" ", "-");
	    $RIF = str_replace($separador, "", $RIF);
	    $data['rif'] = $RIF;
			if(strlen($RIF)>=10) $RIF = substr($RIF, 0, 9); 
	    if(strlen($RIF)==9){
				for ($x = 0; $x <= 9; $x++) {
					$este = "$RIF$x";
		    	if(self::validarRIF($este)['valido']) $data['rif'] = strtoupper($este);
				} 
	    }
	    if(strlen($RIF)<9) $errores['RIF'] = $data['mensaje'] = 'el RIF es my corto';
  	}

		if(!empty($errores)){$data["estatus"] = '0';$data['errores']  = $errores;}
		if(strpos("$_SERVER[REQUEST_URI]", __CLASS__."/".__FUNCTION__) !== false)return entregar($data); else return $data;
  }

  /**
   * Validar formato del RIF
   *
   * Basado en el método módulo 11 para el cálculo del dígito verificador
   * y aplicando las modificaciones propias ejecutadas por el seniat
   * @link http://es.wikipedia.org/wiki/C%C3%B3digo_de_control#C.C3.A1lculo_del_d.C3.ADgito_verificador
   *
   * @return boolean
   */
  public static function validarRIF($validarRIF=null) {
		$errores 	= array();
		$data    	= array();
  	if(empty($validarRIF)) $validarRIF = traerValorDe("validarRIF");
  	if(empty($validarRIF)) $errores['RIF'] = $data['mensaje'] = 'Falta el RIF';
  	if(!empty($validarRIF)) $RIF = $validarRIF;

  	if(empty($errores)){
//print_r($errores);die();
  		$RIF = strtoupper($RIF);
      $retorno = preg_match("/^([VEJPG]{1})([0-9]{9}$)/", $RIF);
      if ($retorno) {
        $digitos = str_split($RIF);
        $digitos[8] *= 2;
        $digitos[7] *= 3;
        $digitos[6] *= 4;
        $digitos[5] *= 5;
        $digitos[4] *= 6;
        $digitos[3] *= 7;
        $digitos[2] *= 2;
        $digitos[1] *= 3;

        // Determinar dígito especial según la inicial del RIF
        // Regla introducida por el SENIAT
        switch ($digitos[0]) {
          case 'V':
            $digitoEspecial = 1;
            break;
          case 'E':
            $digitoEspecial = 2;
            break;
          case 'J':
            $digitoEspecial = 3;
            break;
          case 'P':
            $digitoEspecial = 4;
            break;
          case 'G':
            $digitoEspecial = 5;
            break;
        }
        $suma = (array_sum($digitos) - $digitos[9]) + ($digitoEspecial*4);
        $residuo = $suma % 11;
        $resta = 11 - $residuo;
        $digitoVerificador = ($resta >= 10) ? 0 : $resta;
        if ($digitoVerificador != $digitos[9]) $retorno = false;
      }
      $data['valido'] = $retorno;
  	}
		if(!empty($errores)){
			$data["estatus"] = '0';
			$data["valido"] = false;
			$data['errores']  = $errores;
		}
		if(strpos("$_SERVER[REQUEST_URI]", __CLASS__."/".__FUNCTION__) !== false)return entregar($data); else return $data;
  }

// fin clase seniat
}

?>