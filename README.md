Instalación:

 1.- descomprimes y subes a /var/www/ws (por ejemplo)
 2.- cargas la base de datos bd/geo.sql en postgresql 
 3.- edita controlador/config.php y coloca los datos de la base de datos con los que correspondan

    'BD' =>array(  
        'host'         => 'localhost',
        'puerto'       => '5432',
        'dbname'       => 'OMBRE_DE_LA_BD',
        'dbuser'       => 'NOMBRE_DE_USUARIO',        
        'dbpassword'   => 'CLAVE_DE_USUARIO',       
        'correo'       => 'DATO_NO_IMPORTANTE'
        ),

 4.- listo! prueba navegando a tu servicio con URL's como:

  *  http://ws/geo/region/ce/
  *  http://ws/geo/municipio/guas/
  *  http://ws/geo/municipio/
  *  http://ws/geo/municipio/10/
  *  http://ws/geo/entidad//columnas/iso31662,nombre,columnainexistente/
  *  http://ws/geo/ubigeo/18/
  *  http://ws/geo/ubigeo/030201/
  *  http://ws/geo/entidad/
  *  http://ws/geo/entidad/23/columnas/nombre,poblacion/

Si deseas usar control de acceso:

  *  en controlador/config.php
      * *  $conf['servicio']['acceso'] = true;
      * *  $conf['servicio']['correo'] = true;
      * *  configura una cuenta de correo en $conf['correo']
  *  puedes crearte una cuenta de dos modos:
      * *  http://ws/setUsr/correo/DIRECCION@DE.CORREO/clave/LA_CLAVE/
      * *  o usando un ejemplo de aplicación cliente ajax/POST
      * * *      http://ws/servicios/acceso/public/index.html
  *  El sistema enviará una carta con link para activar cuenta y una segunda carta con un apikey luego de activar la cuenta. luego usa el apikey en cualquier llamada (por url, GET o POST)
  * *  http://ws/geo/ubigeo/030201/apikey/giqi3o87387348f34f38gf38413/


Requerimientos:
*PostgreSQL V4.4 con postgis
*Apache con PHP

Servicios:

Activos
* geo/region/ Retorna la lista de regiones con población y Km2. geo/region/X/ retorna las regiones que coincidan con 'x' sin distinción de acentos o mayúsculas
* geo/ubigeo/######/ retorna un registro (entidad,municipo,parroquia y códigos de identificación)
* geo/ubigeo/X/ Retorna una lista de registros cuya parroquia corresponda con 'x' sin distinción de acentos o mayúsculas
* geo/analize/ ejecuta un analize sobre las tablas involucradas ('geo_ubigeo', 'geo_regiones','geo_entidades')
* geo/total/ retorna el total de registros en las tablas involucradas (actualmente solo geo_ubigeo)
* agregar /jsonCallback/ a cualquier url por ejemplo: geo/total/jsonCallback/ retornará un jsonp llamado jsonCallback({}), útil para peticiones ajax
* geo/ubigeo/####/ retorna un arreglo de registros con el muncipio #### en común 
* geo/ubigeo/##/ retorna un arreglo de registros con la entidad ## en común 
* geo/entidad/ retorna la lista de entidades con sus características
* geo/entidad/x/ retorna la lista de entidades que correspondan con 'x' sin distinción de acentos o mayúsculas
* geo/entidad/##/ retorna la entidad con sus características donde ## es su código ubigeo
* geo/entidad/[##|X|]/columnas/[c1,c2,c3,...]/ como ahorro de transferencia, se puede pedir específicamente qué datos se desean en la respuesta y en qué orden
* geo/municipio/x/ retorna un arreglo de registros con el municipio %x% en común (pudiendo ser distintos municipios con el nombre en común)
* geo/municipio/ retorna la lista de municipios (cod_ent, cod_mun, entidad, municipio)
* geo/municipio/##/ retorna una lista de municipios con la entidad ## en común 

Pendientes

* Incorporar centros poblados (IPOSTEL|CANTV|INE)
* Incorporar códigos postales (IPOSTEL) - Requiere tabla de centros poblados
* Incorporar códigos telefónicos (CANTV) - Requiere tabla de centros poblados
* Incluir coordenadas geográficas para entidades, municipios y parroquias y regiones
* Incluir polígonos correspondientes a entidades, municipios y parroquias y regiones
* Incorporar banderas y escudos
* Cualquier búsqueda de nombres debe poder buscar palabras parecidas fonéticamente como 'Sulia' por Zulia
* Realizar un ejemplo Web cliente con las posbilidades de uso del Web Service, como ejemplo a entegar a las instituciones

Usos prácticos para usuarios finales
* Un forulario Web podría servirse de ws/geo/entidad/, ws/geo/municipio/##/ y ws/geo/parroquia/####/ para llenar listas desplegables de forma dinámica
* Un formulario podría permitir que un usuario encuentre más rápido su dirección si se le permite seleccionar primero el nombre de su parroquia (ws/geo/parroquia/x/), luego aparecerían menos elementos a seleccionar en municipios (ws/geo/municipio/x/) y finalmente la entidad correpondiente. 
* Lo mismo ocurrirá si coloca su número telefónico y acto segido se despliega la entidad, municipio y parroquias correspondientes simplificando la tarea de llenar direcciones.


![](https://gitlab.com/asosab/ws-geo/raw/master/imagen/ubigeo01.png) 
![](https://gitlab.com/asosab/ws-geo/raw/master/imagen/ubigeo_mun.png) 
![](https://gitlab.com/asosab/ws-geo/raw/master/imagen/ubigeo_entidad.png) 
![](https://gitlab.com/asosab/ws-geo/raw/master/imagen/municipio01.png) 
![](https://gitlab.com/asosab/ws-geo/raw/master/imagen/geo.region.tal.png)
![](https://gitlab.com/asosab/ws-geo/raw/master/imagen/geo.entidad.01.png)
![](https://gitlab.com/asosab/ws-geo/raw/master/imagen/geo.entidad..clumnas.iso.png)


