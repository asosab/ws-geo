<?php
include_once("seguridad.php");
/*
*   Este archivo recibe las siguientes variables para trabajar:
*   $url la url a donde se envía el xml por GET con sus parámetros
*   $puerto el puerto a donde se envía el archivo
*/
$curlerror 	= "";
if(!isset($data)){$data="";}

$Curl = curl_init();
curl_setopt($Curl, CURLOPT_URL, $url);
curl_setopt($Curl, CURLOPT_PORT , $puerto);
curl_setopt($Curl, CURLOPT_VERBOSE, 0);
curl_setopt($Curl, CURLOPT_HEADER, 0);
curl_setopt($Curl, CURLOPT_SSLVERSION, 3);
//certificado ****************************************************
curl_setopt($Curl, CURLOPT_SSLCERT, getcwd() . "/cert/servidor_certificado.pem");
curl_setopt($Curl, CURLOPT_SSLKEY, getcwd() . "/cert/servidor_llave.pem");
curl_setopt($Curl, CURLOPT_CAINFO, getcwd() . "/cert/ca.pem");
//**************************************************************/*
curl_setopt($Curl, CURLOPT_POST, 0);
curl_setopt($Curl, CURLOPT_SSL_VERIFYPEER, 0);
curl_setopt($Curl, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($Curl, CURLOPT_HTTPHEADER, array("Content-Type: application/json", "Content-length: ".strlen($data)));

$respuesta = curl_exec($Curl);
if(!curl_errno($Curl)){
  //$info = curl_getinfo($Curl);
  echo $respuesta;
} else {
  $curlerror = curl_error($Curl);
  echo 'Curl error: "' . $curlerror . "\"<br />\n";
}
curl_close($Curl);
?>
