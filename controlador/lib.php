<?php 
/**
 * Archivo: lib.php
 * Usuario: alesosa
 * Fecha: 30/09/13
 * Hora: 04:42 PM
 * Proyecto: sinsera
 */

function zodiaco($fechanac, $icon=true){ 
  $fd=date("d", strtotime($fechanac)); 
  $fm=date("m", strtotime($fechanac)); 
  if((($fd >= "21") && ($fm == "3"))  or (($fd <= "19") && ($fm == "4")))   $sg= $icon? "Aries":"♈"; // "Aries (Impulsiva, irreflexiva, inconsciente: actúa antes de pensar)":"♈";  
  if((($fd >= "20") && ($fm == "4"))  or (($fd <= "20") && ($fm == "5")))   $sg= $icon? "Tauro":"♉";  // "Tauro (Fuerte y resistente, Seguro, firme y estable, Práctico y realista)":"♉";  
  if((($fd >= "21") && ($fm == "5"))  or (($fd <= "21") && ($fm == "6")))   $sg= $icon? "Geminis":"♊"; // "Geminis (Ágil y veloz, Mental y racional, Versátil e inquieto, Comunicativo y hablador)":"♊"; 
  if((($fd >= "22") && ($fm == "6"))  or (($fd <= "22") && ($fm == "7")))   $sg= $icon? "Cancer":"♋";  // "Cancer (hogareño, paternidad relevante, emocional,introvertido y prudente)":"♋";  
  if((($fd >= "23") && ($fm == "7"))  or (($fd <= "22") && ($fm == "8")))   $sg= $icon? "Leo":"♌";  // "Leo (atrevido con coraje, líder, Creativo, competente, necesita aprobación)":"♌";  
  if((($fd >= "23") && ($fm == "8"))  or (($fd <= "22") && ($fm == "9")))   $sg= $icon? "Virgo":"♍"; // "Virgo (Racional, Introvertido, curioso, Ordenado y metódico, costumbrista,detallista)":"♍"; 
  if((($fd >= "23") && ($fm == "9"))  or (($fd <= "22") && ($fm == "10")))  $sg= $icon? "Libra":"♎"; // "Libra (armónico, sensible al arte, reflexivo, conversador, no es sinsero,empático, mediador)":"♎"; 
  if((($fd >= "23") && ($fm == "10")) or (($fd <= "21") && ($fm == "11")))  $sg= $icon? "Escorpio":"♏"; // "Escorpio (emocional, resentido, manipulador, ambicioso, no expresa emociones, perfeccionista, desconfiado)":"♏"; 
  if((($fd >= "22") && ($fm == "11")) or (($fd <= "21") && ($fm == "12")))  $sg= $icon? "Sagitario":"♐"; // "Sagitario (generoso, optimista, alegre,confiado, busca la verdad, le interesan las religiones, dogmático. Quieren creer)":"♐"; 
  if((($fd >= "22") && ($fm == "12")) or (($fd <= "19") && ($fm == "1")))   $sg= $icon? "Capricornio":"♑"; // "Capricornio (Seria,responsable,verifica fuentes,usa dedal,ambiciosa con objetivos,voluntariosa,comportamiento rígido, Frialdad, imagen social importante, austera)":"♑"; 
  if((($fd >= "20") && ($fm == "1"))  or (($fd <= "18") && ($fm == "2")))   $sg= $icon? "Acuario":"♒"; // "Acuario (Original, creativo,Pensamiento vertiginoso enfocado al futuro, Intuitivo, Distante, desapegado,Inventor, innovador,sentido profundo de la libertad, Comprometido con causas sociales y humanitarias colectivas, Imprevisible.)":"♒"; 
  if((($fd >= "19") && ($fm == "2"))  or (($fd <= "20") && ($fm == "3")))   $sg= $icon? "Piscis":"♓"; // "Piscis (Intuitivo, perceptivo, se auntoengaña, nostálgico, silencioso,lunático,Alérgico.)":"♓"; 
  return $sg; 
} 


function fechaEs($sintax,$date = ''){
    $date = ($date) ? $date : time();
    $meses = array('Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre');
    $dias = array('domingo','lunes','martes','miercoles','jueves','viernes','sabado');
    $fechaes = str_replace('&diatexto',$dias[date('w',$date)],$sintax);
    $fechaes = str_replace('&dianum',date('d',$date),$fechaes);
    $fechaes = str_replace('&mestexto',$meses[date('m',$date)-1],$fechaes);
    $fechaes = str_replace('&mesnum',date('m',$date),$fechaes);
    $fechaes = str_replace('&año',date('Y',$date),$fechaes);
    return ($fechaes) ? $fechaes : '';
}

function urqu($valor){
    $valor = round($valor,2);
    $monto = explode(".", $valor);
    if (!isset($monto[1])) $monto[1] = "00"; 
    if (strlen($monto[1])==1) $monto[1] .= "0";
    $vez=ceil(strlen($monto[0])/3)-1;
    while ( $vez>0 ):
        $monto[0] = substr_replace($monto[0]," ",-3*$vez,0);
        $vez--;
    endwhile;
    return "Bs. ".$monto[0] . "&nbsp;" . "<span class=\"super\" style='font-size: 60%;vertical-align: top;text-decoration: underline;'>" . substr($monto[1],0,2) . "</span>"; // estoy truncando los céntimos
}

$todoslosarchivos = array();
function listar_Archivos_rec($ruta = '.'){
    global $todoslosarchivos;
    if($ruta == '.') $ruta = $_SERVER["DOCUMENT_ROOT"];
    if (is_dir($ruta)){
        if ($dh = opendir($ruta)){
            while (($file = readdir($dh)) !== false){
                if ($file!="." && $file!=".."){
                    $todoslosarchivos[] = "$ruta/$file";
                    listar_Archivos_rec("$ruta/$file"); 
                }
            }
            closedir($dh);
        }
    }
}

function ultimo_modificado($path = '.') {
    global $todoslosarchivos;
    if($path == '.') $path = $_SERVER["DOCUMENT_ROOT"];
    listar_Archivos_rec($path);
    $lista = $todoslosarchivos;
    $result = array();
    $result['ret'] = 0;
    foreach($lista as $f) {
        if (filemtime($f) > $result['ret']){$result['ret'] = filemtime($f);  $result['fn']  = $f;}
    }
    return $result;
}

function utf8_fopen_read($fileName) {
    $fc = iconv('windows-1250', 'utf-8', file_get_contents($fileName));
    $handle=fopen("php://memory", "rw");
    fwrite($handle, $fc);
    fseek($handle, 0);
    return $handle;
} 

/**
 * recibirINPUT
 * Recibe cualquier cosa vía php://input
 * @return str del archivo recibido
 */
function recibirINPUT(){ 
    $xmlstr = trim(file_get_contents('php://input'));
    return $xmlstr;
}
/**
 * guardarArchivo
 * Guarda un str en un archivo
 * @return xml guardado
 */

function guardarArchivo($str, $archivo){ 
 //echo  $archivo;die();
    $fh = fopen($archivo, 'w') || die();
    $fwrite = fwrite($fh,$str);
    fclose($fh);
    return $archivo;
}

/**
 * guardarArchivo
 * Guarda un str en un archivo
 * @return xml guardado
 */

function guardarINPUT($archivo){ 
    $str = file_get_contents('php://input');
    if(strlen($archivo) > 2 && strlen($str) > 2){
        $fh = fopen($archivo, 'w');
        $fwrite = fwrite($fh,$str);
        fclose($fh);
        return $archivo;
    }else return false;
}

/**
*  contarTodoDe
* Dado un directorio cuenta todos sus archivos
*/
function contarTodoDe($dir){
    $ficheros= 0;
    $handle = opendir($dir);
    while ($file = readdir($handle)) {
        if (is_file($dir.$file)) {
            $ficheros++;
        }
    }
    return $ficheros;
}

/**
*  borrarTodoDe
* Dado un directorio borra todos sus archivos y retorna el número de archivos borrados
*/
function borrarTodoDe($dir){
    $ficheroseliminados= 0;
    $handle = opendir($dir);
    while ($file = readdir($handle)) {
        if (is_file($dir.$file)) {
            if ( unlink($dir.$file) ){
                $ficheroseliminados++;
            }
        }
    }
    return $ficheroseliminados;
}

/**
*  Toma un xml y retorna un boleano según si encuentra errores o no
*/
function is_valid_xml($xml){
    libxml_use_internal_errors( true );
    $doc = new DOMDocument('1.0', 'utf-8');
    $doc->loadXML( $xml );
    $errors = libxml_get_errors();
    return empty( $errors );
}

/** genero un json encode  */
function jsonRemoveUnicodeSequences($struct) {
   return preg_replace("/\\\\u([a-f0-9]{4})/e", "iconv('UCS-4LE','UTF-8',pack('V', hexdec('U$1')))", json_encode($struct));
}

// Modificamos las variables pasadas por URL
foreach( $_GET as $variable => $valor ){
    $_GET [ $variable ] = str_replace ( "'" , "'" , $_GET [ $variable ]);
}
// Modificamos las variables de formularios
foreach( $_POST as $variable => $valor ){
    $_POST [ $variable ] = str_replace ( "'" , "'" , $_POST [ $variable ]); 
}

/** convierto todo lo enviado por POST en variables  */
if(isSet($_POST)){  // convirtiendo valores enviados por post a variables globales del mismo nombre
    $keys_post = array_keys($_POST);
    foreach ($keys_post as $key_post){
        $$key_post = $_POST[$key_post];
        //error_log("variable $key_post viene desde $ _POST");
    }
}

/** convierto todo lo enviado por GET en variables jeje */
if(isSet($_GET)){ // convirtiendo valores enviados por get a variables globales del mismo nombre
    $keys_get = array_keys($_GET);
    foreach ($keys_get as $key_get){
        $$key_get = $_GET[$key_get];
        //error_log("variable $key_get viene desde $ _GET");
    }
}

/** convierto todo lo almacenado en las sesiones en variables  */
if(isSet($_SESSION)){ // convirtiendo valores guardados en la sesión a variables globales del mismo nombre
    $keys_sesion = array_keys($_SESSION);
    foreach ($keys_sesion as $key_sesion){
        $$key_sesion = $_SESSION[$key_sesion];
        //error_log("variable $key_sesion viene desde $ _SESSION");
    }
}

function array_push_associative(&$arr) {
    $args = func_get_args();
    array_unshift($args); // remove &$arr argument
    foreach ($args as $arg) {
        if (is_array($arg)) {
            foreach ($arg as $key => $value) {
                $arr[$key] = $value;
                $ret++;
            }
        }
    }
    return $ret;
}

function CapturarIp(){
    if (isSet($_SERVER)) {
        if (isSet($_SERVER["HTTP_X_FORWARDED_FOR"])) {
            $ip = $_SERVER["HTTP_X_FORWARDED_FOR"];
            $proxy  = $_SERVER["REMOTE_ADDR"];
        } elseif (isSet($_SERVER["HTTP_CLIENT_IP"])) {
            $ip = $_SERVER["HTTP_CLIENT_IP"];
        } else {
            $ip = $_SERVER["REMOTE_ADDR"];
        }
    } else {
        if ( getenv( 'HTTP_X_FORWARDED_FOR' ) ) {
            $ip = getenv( 'HTTP_X_FORWARDED_FOR' );
            $proxy = getenv( 'REMOTE_ADDR' );
        } elseif ( getenv( 'HTTP_CLIENT_IP' ) ) {
            $ip = getenv( 'HTTP_CLIENT_IP' );
        } else {
            $ip = getenv( 'REMOTE_ADDR' );
        }
    }
    return $ip;
}

function getCurrentUrl(){  
    $domain = $_SERVER['HTTP_HOST'];  
    $url = "https://" . $domain . $_SERVER['REQUEST_URI'];  
    return $url;  
}  

function existeModelo($modelo){
  $arr = array();
  if($gestor=opendir('./servicios/')){
      while (($archivo=readdir($gestor))!==false){
          if ((!is_file($archivo))and($archivo!='.')and($archivo!='..'))
              $arr[]=$archivo;
      }
      closedir($gestor);
  }
  if(in_array ($modelo, $arr)){
      return true;
  }
}




function error($codigo){
  if($codigo = 404){
    header("HTTP/1.0 404 Not Found");
    //include_once $_SERVER['DOCUMENT_ROOT'].'/vista/404.php';
    die();  
  }
}

function valorde($accion){
  $url_actual = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
  $pos = strpos($url_actual, $accion);
  if($pos){
    $ini = strpos($url_actual, "/",$pos)+1;
    $fin = strpos($url_actual, "/",$ini);
    return urldecode(substr($url_actual, $ini, $fin - $ini));
  }else{
    return false;
  }
}

function entregar($arreglo){
  global $conf;
  $url = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";

  if(is_string($arreglo)){
    $arr = json_decode($arreglo, true);
    if(json_last_error() == JSON_ERROR_NONE){
      $arreglo = $arr;
    }else{
      $arreglo = array('data' => $arreglo);
    }
  }

  $paquete = $arreglo;
  if(isset($arreglo['error']) && isset($arreglo['error'][404])){header("HTTP/1.0 404 Not Found");}
  if(isset($arreglo['error']) && isset($arreglo['error'][403])){header("HTTP/1.0 403 Forbidden");}

  if(strpos($url, "alcorreo") !== false){
    require_once $_SERVER["DOCUMENT_ROOT"] ."/servicios/persona/persona.php";
    require_once $_SERVER["DOCUMENT_ROOT"] ."/servicios/correo/correo.php";
    if(empty($usuario) && !empty($_SESSION['usuario'])) $usuario = $_SESSION['usuario'];
    $datos = array();
    if(!empty($usuario)){
      $datos['para']    = $usuario['correo'];
      $titulo = $conf['app']['nombre_corto'];
      if(strpos($url, "getPersona") !== false){
        $nombre = $arreglo['saime']['nombre1'] . " " . $arreglo['saime']['nombre2'] . " ". $arreglo['saime']['apellido1'] . " ". $arreglo['saime']['apellido2'];
        $nombre = ucwords(strtolower($nombre));
        if(!empty($arreglo['cne']['estado']))$estado = "(". ucwords(strtolower($arreglo['cne']['estado'])) . ")";
        else $estado ="";

        $titulo .= " - $nombre $estado";
        $datos['titulo']  = $titulo;
      }else{$datos['titulo']  = $_SERVER['REQUEST_URI'];}
      $datos['titulo']  = $_SERVER['REQUEST_URI'];
      $datos['cuerpoplano'] = persona::toHumano($arreglo);
    }else{
      $datos['error'] = "no hay destinatario";
    }
    $arreglo['envio'] = correo::enviar($datos);
  }

  if(strpos($url, "jsonCallback") !== false){
    $resultado = json_encode($arreglo);
    $paquete = "jsonCallback(" . $resultado . ")";
  }elseif(strpos($url, "json") !== false){
    header('Content-Type: application/json');
    $paquete = json_encode($arreglo);
  }elseif(strpos($url, "toHumano") !== false){
    require_once $_SERVER["DOCUMENT_ROOT"] ."/servicios/persona/persona.php";
    $paquete = persona::toHumano($paquete);
  }else{ header('Content-Type: application/json'); $paquete = json_encode($arreglo);}

  echo $paquete;
}

function get_web_page($url, $movil=false){
  $agent= 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1; .NET CLR 1.0.3705; .NET CLR 1.1.4322)';
  if($movil) $agent= 'Mozilla/5.0 (iPhone; U; CPU like Mac OS X; en) AppleWebKit/420.1 (KHTML, like Gecko) Version/3.0 Mobile/3B48b Safari/419.3';
  $options = array(
    CURLOPT_RETURNTRANSFER => true,     // return web page
    CURLOPT_HEADER         => false,    // don't return headers
    CURLOPT_FOLLOWLOCATION => true,     // follow redirects
    CURLOPT_ENCODING       => "",       // handle all encodings
    CURLOPT_USERAGENT      => $agent, // who am i
    CURLOPT_AUTOREFERER    => true,     // set referer on redirect
    CURLOPT_CONNECTTIMEOUT => 120,      // timeout on connect
    CURLOPT_TIMEOUT        => 120,      // timeout on response
    CURLOPT_MAXREDIRS      => 10,       // stop after 10 redirects
    CURLOPT_SSL_VERIFYPEER => false,     // Disabled SSL Cert checks
    CURLOPT_SSL_VERIFYHOST => false,
    CURLOPT_VERBOSE        => true, 
  );

  $ch      = curl_init( $url );
  curl_setopt_array( $ch, $options );
  $content = curl_exec( $ch );
  $err     = curl_errno( $ch );
  $errmsg  = curl_error( $ch );
  $header  = curl_getinfo( $ch );
  curl_close( $ch );

  $header['errno']   = $err;
  $header['errmsg']  = $errmsg;
  $header['content'] = $content;
  return $header;
}

function strposOffset($string, $search, $offset){
  $arr = explode($search, $string);
  switch( $offset ){
    case $offset == 0:
      return false;
      break;
    case $offset > max(array_keys($arr)):
      return false;
      break;
    default:
    return strlen(implode($search, array_slice($arr, 0, $offset)));
  }
}

function tdToArray($strTable){
  $result = array();
  $dom = new DOMDocument;
  $dom->loadHTML($strTable);
  $xPath = new DOMXPath($dom);
  $td = $xPath->query('//table/tr/td');
  foreach($td as $val) $result[] = trim($val->nodeValue);
  return $result;
}

function getTable($pajar, $aguja){
  $pos = strpos($pajar, $aguja);
  if($pos == false){
    return false;
  }else{
    $estaEs = false;
    $tini = 0;
    while (!$estaEs) {
      $tini = strpos($pajar, "<table", $tini);
      if($tini == false) return false;
      $otra = strpos($pajar, "<table", $tini+1);
      $frase = strpos($pajar, $aguja, $tini+1);
      if($otra !=false && $frase !=false && $frase < $otra) $estaEs = true;
      if($otra !=false && $frase !=false && $frase > $otra) $tini = $otra;
      if($otra ==false && $frase !=false) $estaEs = true;
      if($otra ==false && $frase ==false) return false;
    }
    $tfin = strpos($pajar, "</table>", $tini+1);
    if($tfin ==false) return false;
    return substr($pajar, $tini, ($tfin - $tini) +8);
  }
}

function cedulaFormat($cedula){
  $cf = array();
  $cedula = trim($cedula);
  $letra = strtoupper(substr($cedula,0,1));
  if(in_array($letra, array('V','E','J','P'))){
    $cf['letra'] = $letra;
    $numero = preg_replace('/\D/', '', $cedula);
    if(is_numeric($numero)){
      $numero = intval($numero);
      $cf['numero'] = $numero;
    }else{
      $cf['numero'] = false;
    }
  }else{
    if(is_numeric($cedula)){
      $cf['letra'] = 'V';
      $cf['numero'] = $cedula;
    }else{
      $cf['letra'] = false;
      $cf['numero'] = false;
    }

  }
  return $cf;
}

function limpiar($frase){
  $nueva = $frase;
  $nueva = str_replace(array("´",'"',"'","&nbsp;","(",")"," insert "," delete ","´","\t","\r","\n","\r\n"), "", $nueva);
  $nueva = mb_convert_encoding($nueva, "HTML-ENTITIES", "UTF-8");
  $nueva = trim($nueva);
  if($nueva ."" =="") $nueva = 'null';
  return $nueva;
}



/**
 * formatDBColum
 * @param $campos
 * @param $campo
 * @param $valor
 * @return string 'valor' para arreglos y fechas y sin comillas para números 
 */
function formatDBColum($campos, $campo, $valor){
  $key = array_search($campo, array_column($campos, 'column_name'));
  $numerico = array('bigint','integer','decimal','smallint','numeric','real','double precision','smallserial','serial','bigserial','money');
  $texto = array('character varying','varchar','character','char','text','bytea','timestamp','date','time','interval','boolean','enum','json','xml');

  if (in_array($campos[$key]['data_type'], $numerico)) {
    if(is_numeric((0 + $valor))){

      $result = 0+$valor;
    }elseif($valor == "0" || $valor == 0 || $valor == 0.00){

      $result = 0;
    }elseif($valor."" ==""){

      $result = null;
    } else $result = 0.00;

  }else{
    $valor = pg_escape_string($valor);
    $result = "'{$valor}'";
  }
//  if($campos[$key]['column_name']=='lat') {echo "$result,$valor"; die();}
  //TODO debe formatear los valores, además
  return $result;
}

/**
 * getLlavesBDTable
 * Retorna un areglo con las columnas llave de una tabla y su tipo de datos
 * @param $db
 * @param $table
 * @return array ('estatus' = '1|0', 'accion' = 'update|insert|error')
 */
function getLlavesBDTable($db, $table){
  $llaves = array();
/*
  $resultado = $db->select("pg_index", 
    ["[<>]pg_attribute" => ["attrelid" => "indrelid", "attnum" => "ANY(indkey)"]], 
    ["attname","format_type(a.atttypid, a.atttypmod) AS data_type"], 
    ["indrelid" => "'$table'::regclass","indisprimary" => true]
  );
*/
/*
  $resultado = $db->query("SELECT a.attname, format_type(a.atttypid, a.atttypmod) AS data_type
          FROM   pg_index i
          JOIN   pg_attribute a ON a.attrelid = i.indrelid AND a.attnum = ANY(i.indkey)
          WHERE  i.indrelid = '$table'::regclass
          AND    i.indisprimary")->fetchAll();
*/

  $sql = "SELECT a.attname, format_type(a.atttypid, a.atttypmod) AS data_type
          FROM   pg_index i
          JOIN   pg_attribute a ON a.attrelid = i.indrelid AND a.attnum = ANY(i.indkey)
          WHERE  i.indrelid = '$table'::regclass
          AND    i.indisprimary";
  $resultado = DbLink::resultados($sql,$db);

  if($resultado !== false) $llaves = $resultado;
  return $llaves;
}

/**
 * loadDatasTable
 * Carga o actualiza una tabla con datos de un arreglo (varios registros)
 * @param $db
 * @param $table
 * @param $data
 * @return array ('estatus' = '1|0', 'accion' = 'update|insert|error')
 */
function loadDatasTable($db, $table, $data){
  $registros = array();
  if(empty($data)) return false;
  foreach ($data as $registro){
    $registros[] = loadDataTable($db, $table, $registro);
  }
  return $registros;
}

function getColumTable($table, $db='BD'){
  $sql = "select column_name, data_type from information_schema.columns where table_name = '$table'";
  $resultado = DbLink::resultados($sql,$db);
  if($resultado !== false) return $resultado;
  else return false;
}

/**
 * loadDataTable
 * Carga o actualiza una tabla con datos de un arreglo
 * @param $db
 * @param $table
 * @param $data
 * @return array ('estatus' = '1|0', 'accion' = 'update|insert|error')
 */
function loadDataTable($db, $table, $data){
//  print_r($data);die();
  $sql = "select column_name, data_type from information_schema.columns where table_name = '$table'";
  $resultado = DbLink::resultados($sql,$db);
  if($resultado == false){ 
    return array('estatus' => '0', 'accion'=>'error', 'error'=>'No existe la tabla o la BD');
  }else{
    $BDTable = $resultado;
//print_r($BDTable);die();
    $llaves = getLlavesBDTable($db, $table);

    if(count($llaves)>0){
      $estanTodas = true;
//print_r($llaves);die();
      foreach ($llaves as $llave) {

        if(!array_key_exists($llave['attname'], $data)) $estanTodas = false;
      }
    }

    if(count($llaves)>1){
      $debeTenerLlaves = true;
    }elseif(count($llaves)==1){
      if(in_array($llaves[0]['data_type'], array('serial','bigserial','integer'))) $debeTenerLlaves = false;
      else $debeTenerLlaves = true;
    }else{
      $debeTenerLlaves = false;
      $estanTodas = false;
    }

    if($debeTenerLlaves && $estanTodas){
      $where = array();
      foreach ($llaves as $llave){
        $vformat = formatDBColum($BDTable, $llave['attname'], $data[$llave['attname']]);
        $where[] = $llave['attname'] . " = $vformat";
      }
      $whereList = implode(' AND ', $where);
      $sql = "select 1 from $table where $whereList";
      $resultado = DbLink::resultados($sql,$db);
      if($resultado !== false)$accion = "UPDATE";
      else $accion = "INSERT";
    }elseif(!$debeTenerLlaves) $accion = "INSERT";
    else $accion = "Sin Acción, faltan llaves";

    if($accion=="INSERT"){
      $campos = array();
      $valores = array();
      foreach ($BDTable as $campo) {
        if(isset($data[$campo['column_name']])){
          $vformat = formatDBColum($BDTable, $campo['column_name'], $data[$campo['column_name']]);
          if(!empty($vformat) || $vformat=='0'){
            $campos[] = $campo['column_name'];
            $valores[] = $vformat;
          }

        }
      }
      $sql = "INSERT INTO $table (" . implode(', ', $campos) . ") VALUES (" . implode(', ', $valores) . ") RETURNING 1"; 
      if(count($valores)==0) return array('estatus' => '0', 'accion'=>'INSERT','error'=>'No hay valores que insertar');
    }

    if($accion=="UPDATE"){
      $pares = array();
      foreach ($BDTable as $campo) {
        if(isset($data[$campo['column_name']])){
          $vformat = formatDBColum($BDTable, $campo['column_name'], $data[$campo['column_name']]);
          if(!empty($vformat)) $pares[] = $campo['column_name'] . " = $vformat";
        }
      }
      $sql = "UPDATE $table SET " . implode(', ', $pares) . " WHERE $whereList RETURNING 1";
      if(count($pares)==0) return array('estatus' => '0', 'accion'=>'UPDATE','error'=>'No hay valores que actualizar');
    }
//echo $sql;die();
    if(true){
      if($accion != "Sin Acción, faltan llaves"){
        $resultado = DbLink::resultados($sql,$db);
        if($resultado !== false) return array('estatus' => '1', 'accion'=>$accion,'error'=>'0');
        else return array('estatus' => '0', 'accion'=>$accion,'error'=>'Error procesando la acción');
      }else{
        return array('estatus' => '0', 'accion'=>'0', 'error'=>$accion);
      }
    }else{
      $estatus = ($accion == "Sin Acción, faltan llaves")? '0':'1';
      $error = ($estatus=='0')? $accion:'0';
      return array('estatus' => $estatus, 'accion'=>$accion, 'error'=>'0', 'sql'=>$sql);
    }
  }
}

function fBDBool($valor){
  $valor = '0';
  $positivo = array('si','yes','SI','Si','Yes','YES','1',1);
  if (in_array($valor, $positivo)) $valor = '1';
  else $valor = '0';
  return $valor;
}

function getAmount($money){
  $cleanString = preg_replace('/([^0-9\.,])/i', '', $money);
  $onlyNumbersString = preg_replace('/([^0-9])/i', '', $money);
  $separatorsCountToBeErased = strlen($cleanString) - strlen($onlyNumbersString) - 1;
  $stringWithCommaOrDot = preg_replace('/([,\.])/', '', $cleanString, $separatorsCountToBeErased);
  $removedThousendSeparator = preg_replace('/(\.|,)(?=[0-9]{3,}$)/', '',  $stringWithCommaOrDot);
  return (float) str_replace(',', '.', $removedThousendSeparator);
}

function validateDate($date){
  $d = DateTime::createFromFormat('Y-m-d', $date);
  return $d && $d->format('Y-m-d') === $date;
}

function fBDdate($fecha){
  $separador = '-';
  if(strpos($fecha, '-')) $separador = '-';
  if(strpos($fecha, '/')) $separador = '/';
  $date = implode('-', array_reverse(explode($separador,$fecha)));
  if(validateDate($date)) return $date;
  else return null;
  
}

function totalRegBDTable($tabla, $db='BD', $nspname='public'){
  global $DBPG;
/*
  $resultado = $DBPG->select("pg_class", 
    ["[>]pg_namespace" => ["relnamespace" => "oid"]], 
    ["reltuples"], 
    ["nspname" => $nspname,"relname" => $tabla]
  );
*/

  $sql = "SELECT reltuples FROM pg_class JOIN pg_namespace ON (pg_class.relnamespace = pg_namespace.oid)
          WHERE nspname = '$nspname' AND relname = '$tabla'";
  $resultado = DbLink::resultados($sql,$db);

  if($resultado !== false) return $resultado[0]['reltuples'];
  else return false;
}

function analyzeBDTable($tabla, $db='BD'){
  $tiempo = microtime(true);
  $resultado = "";
  exec("psql -U postgres -d data -c \"analyze verbose $tabla\" 2>&1", $resultado);
  return array('resultado'=>$resultado, 'tiempo'=> microtime(true) - $tiempo);
}

function RegistroActualizado($letra,$numero,$tabla, $meses=12, $db='BD'){
  $fechaActual = date("Y-m-d H:i:s");
  $sql = "select EXTRACT(year FROM age('$fechaActual',fecha_modificacion))*12 + EXTRACT(month FROM age('$fechaActual',fecha_modificacion)) as fecha from $tabla
    WHERE letra = '$letra' AND cedula = $numero";
  $resultado = DbLink::resultados($sql,$db);
  if($resultado !== false) if($resultado[0]['fecha'] >= $meses) return false; else return true;
  else return false;
}

// quitando acentos
function stripAccents($string){
  return iconv('UTF-8', 'ASCII//TRANSLIT', $string);
}

function remove_accents($string) {
    if ( !preg_match('/[\x80-\xff]/', $string) ) return $string;
    $chars = array(
    // Decompositions for Latin-1 Supplement
    chr(195).chr(128) => 'A', chr(195).chr(129) => 'A',
    chr(195).chr(130) => 'A', chr(195).chr(131) => 'A',
    chr(195).chr(132) => 'A', chr(195).chr(133) => 'A',
    chr(195).chr(135) => 'C', chr(195).chr(136) => 'E',
    chr(195).chr(137) => 'E', chr(195).chr(138) => 'E',
    chr(195).chr(139) => 'E', chr(195).chr(140) => 'I',
    chr(195).chr(141) => 'I', chr(195).chr(142) => 'I',
    chr(195).chr(143) => 'I', chr(195).chr(145) => 'N',
    chr(195).chr(146) => 'O', chr(195).chr(147) => 'O',
    chr(195).chr(148) => 'O', chr(195).chr(149) => 'O',
    chr(195).chr(150) => 'O', chr(195).chr(153) => 'U',
    chr(195).chr(154) => 'U', chr(195).chr(155) => 'U',
    chr(195).chr(156) => 'U', chr(195).chr(157) => 'Y',
    chr(195).chr(159) => 's', chr(195).chr(160) => 'a',
    chr(195).chr(161) => 'a', chr(195).chr(162) => 'a',
    chr(195).chr(163) => 'a', chr(195).chr(164) => 'a',
    chr(195).chr(165) => 'a', chr(195).chr(167) => 'c',
    chr(195).chr(168) => 'e', chr(195).chr(169) => 'e',
    chr(195).chr(170) => 'e', chr(195).chr(171) => 'e',
    chr(195).chr(172) => 'i', chr(195).chr(173) => 'i',
    chr(195).chr(174) => 'i', chr(195).chr(175) => 'i',
    chr(195).chr(177) => 'n', chr(195).chr(178) => 'o',
    chr(195).chr(179) => 'o', chr(195).chr(180) => 'o',
    chr(195).chr(181) => 'o', chr(195).chr(182) => 'o',
    chr(195).chr(182) => 'o', chr(195).chr(185) => 'u',
    chr(195).chr(186) => 'u', chr(195).chr(187) => 'u',
    chr(195).chr(188) => 'u', chr(195).chr(189) => 'y',
    chr(195).chr(191) => 'y',
    // Decompositions for Latin Extended-A
    chr(196).chr(128) => 'A', chr(196).chr(129) => 'a',
    chr(196).chr(130) => 'A', chr(196).chr(131) => 'a',
    chr(196).chr(132) => 'A', chr(196).chr(133) => 'a',
    chr(196).chr(134) => 'C', chr(196).chr(135) => 'c',
    chr(196).chr(136) => 'C', chr(196).chr(137) => 'c',
    chr(196).chr(138) => 'C', chr(196).chr(139) => 'c',
    chr(196).chr(140) => 'C', chr(196).chr(141) => 'c',
    chr(196).chr(142) => 'D', chr(196).chr(143) => 'd',
    chr(196).chr(144) => 'D', chr(196).chr(145) => 'd',
    chr(196).chr(146) => 'E', chr(196).chr(147) => 'e',
    chr(196).chr(148) => 'E', chr(196).chr(149) => 'e',
    chr(196).chr(150) => 'E', chr(196).chr(151) => 'e',
    chr(196).chr(152) => 'E', chr(196).chr(153) => 'e',
    chr(196).chr(154) => 'E', chr(196).chr(155) => 'e',
    chr(196).chr(156) => 'G', chr(196).chr(157) => 'g',
    chr(196).chr(158) => 'G', chr(196).chr(159) => 'g',
    chr(196).chr(160) => 'G', chr(196).chr(161) => 'g',
    chr(196).chr(162) => 'G', chr(196).chr(163) => 'g',
    chr(196).chr(164) => 'H', chr(196).chr(165) => 'h',
    chr(196).chr(166) => 'H', chr(196).chr(167) => 'h',
    chr(196).chr(168) => 'I', chr(196).chr(169) => 'i',
    chr(196).chr(170) => 'I', chr(196).chr(171) => 'i',
    chr(196).chr(172) => 'I', chr(196).chr(173) => 'i',
    chr(196).chr(174) => 'I', chr(196).chr(175) => 'i',
    chr(196).chr(176) => 'I', chr(196).chr(177) => 'i',
    chr(196).chr(178) => 'IJ',chr(196).chr(179) => 'ij',
    chr(196).chr(180) => 'J', chr(196).chr(181) => 'j',
    chr(196).chr(182) => 'K', chr(196).chr(183) => 'k',
    chr(196).chr(184) => 'k', chr(196).chr(185) => 'L',
    chr(196).chr(186) => 'l', chr(196).chr(187) => 'L',
    chr(196).chr(188) => 'l', chr(196).chr(189) => 'L',
    chr(196).chr(190) => 'l', chr(196).chr(191) => 'L',
    chr(197).chr(128) => 'l', chr(197).chr(129) => 'L',
    chr(197).chr(130) => 'l', chr(197).chr(131) => 'N',
    chr(197).chr(132) => 'n', chr(197).chr(133) => 'N',
    chr(197).chr(134) => 'n', chr(197).chr(135) => 'N',
    chr(197).chr(136) => 'n', chr(197).chr(137) => 'N',
    chr(197).chr(138) => 'n', chr(197).chr(139) => 'N',
    chr(197).chr(140) => 'O', chr(197).chr(141) => 'o',
    chr(197).chr(142) => 'O', chr(197).chr(143) => 'o',
    chr(197).chr(144) => 'O', chr(197).chr(145) => 'o',
    chr(197).chr(146) => 'OE',chr(197).chr(147) => 'oe',
    chr(197).chr(148) => 'R',chr(197).chr(149) => 'r',
    chr(197).chr(150) => 'R',chr(197).chr(151) => 'r',
    chr(197).chr(152) => 'R',chr(197).chr(153) => 'r',
    chr(197).chr(154) => 'S',chr(197).chr(155) => 's',
    chr(197).chr(156) => 'S',chr(197).chr(157) => 's',
    chr(197).chr(158) => 'S',chr(197).chr(159) => 's',
    chr(197).chr(160) => 'S', chr(197).chr(161) => 's',
    chr(197).chr(162) => 'T', chr(197).chr(163) => 't',
    chr(197).chr(164) => 'T', chr(197).chr(165) => 't',
    chr(197).chr(166) => 'T', chr(197).chr(167) => 't',
    chr(197).chr(168) => 'U', chr(197).chr(169) => 'u',
    chr(197).chr(170) => 'U', chr(197).chr(171) => 'u',
    chr(197).chr(172) => 'U', chr(197).chr(173) => 'u',
    chr(197).chr(174) => 'U', chr(197).chr(175) => 'u',
    chr(197).chr(176) => 'U', chr(197).chr(177) => 'u',
    chr(197).chr(178) => 'U', chr(197).chr(179) => 'u',
    chr(197).chr(180) => 'W', chr(197).chr(181) => 'w',
    chr(197).chr(182) => 'Y', chr(197).chr(183) => 'y',
    chr(197).chr(184) => 'Y', chr(197).chr(185) => 'Z',
    chr(197).chr(186) => 'z', chr(197).chr(187) => 'Z',
    chr(197).chr(188) => 'z', chr(197).chr(189) => 'Z',
    chr(197).chr(190) => 'z', chr(197).chr(191) => 's'
    );
    $string = strtr($string, $chars);
    return $string;
}


  /**
   * scrapDato
   * Extrae un dato de una estructura web ($frase) partiendo de la proximidad a una $pista
   * @param $frase string el cuerpo de la página Web
   * @param $pista string un texto que sirve para ubicar el texto buscado
   * @return string con cadena buscada
   */
  function scrapDato($frase, $pista, $elem="td", $cuenta=1, $cierre = null){ 
    $pos = strpos($frase, $pista);
    if($pos == false) return false;
    $vi = $pos;
    if($cierre == null){
      $abre = "<$elem";
      $cierra = "</$elem>";
    }else{
      $abre = $elem;
      $cierra = $cierre;
    }

    for ($x = 1; $x <= $cuenta; $x++) {
      $npos = strpos($frase, $abre, $vi+1);
      if($npos == false) $x=$cuenta; else $vi = $npos;
    } 

    if($cierre == null) $vi = strpos($frase, ">", $vi) + 1; else $vi = $vi +1;
    $vf = strpos($frase, $cierra,$vi);
    $v = trim(substr($frase, $vi, $vf - $vi ));
    return strip_tags($v);
  }

  function limpiarEntero($numero){
    $limpio = $numero;
    $limpio = strip_tags($numero);
    $limpio = str_replace(array("&#160;", " "), "", $limpio);
    $limpio = filter_var($limpio, FILTER_SANITIZE_NUMBER_INT);
    return $limpio;
  }

function traerValorDe($variable){
  global $$variable;
//  $var = empty($$variable)? valorde($variable):$$variable;
  $var = valorde($variable);
  if(empty($var)) {
    if(!empty($_POST[$variable])) $var = $_POST[$variable];
    elseif(!empty($_GET[$variable])) $var = $_GET[$variable];
    else $var = false;
  }
  return $var;
}

function tdrows($elements){
  $str = "";
  foreach ($elements as $element) $str .= $element->nodeValue . ", ";
  return $str;
}

function getdatatable($contents){
  $tds = array();
  $DOM = new DOMDocument;
  $DOM->loadHTML($contents);
  $items = $DOM->getElementsByTagName('tr');
  foreach ($items as $node) $tds[] = tdrows($node->childNodes);
}

/**
  * Formats a line (passed as a fields  array) as CSV and returns the CSV as a string.
  * Adapted from http://us3.php.net/manual/en/function.fputcsv.php#87120
  */
function arrayToCsv( array &$fields, $delimiter = ';', $enclosure = '"', $encloseAll = false, $nullToMysqlNull = false ) {
  $delimiter_esc = preg_quote($delimiter, '/');
  $enclosure_esc = preg_quote($enclosure, '/');
  $output = array();
  foreach ( $fields as $field ) {
    if ($field === null && $nullToMysqlNull) {
      $output[] = 'NULL';
      continue;
    }
    // Enclose fields containing $delimiter, $enclosure or whitespace
    if ( $encloseAll || preg_match( "/(?:${delimiter_esc}|${enclosure_esc}|\s)/", $field ) ) {
      $output[] = $enclosure . str_replace($enclosure, $enclosure . $enclosure, $field) . $enclosure;
    }
    else {
      $output[] = $field;
    }
  }

  return implode( $delimiter, $output );
}

function outputCSV($data) {
  $outputBuffer = fopen("php://output", 'w');
  foreach($data as $val) {
    fputcsv($outputBuffer, $val,";","'");
  }
  fclose($outputBuffer);
}

function dirToArray($dir) {
  $result = array();
  $cdir = scandir($dir);
  foreach ($cdir as $key => $value){
    if (!in_array($value,array(".",".."))){
      if (is_dir($dir . DIRECTORY_SEPARATOR . $value)){
        $result[$value] = dirToArray($dir . DIRECTORY_SEPARATOR . $value);
      }
      else{
        $result[] = $value;
      }
    }
  }
  return $result;
} 

?>