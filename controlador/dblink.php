<?php //if(!isset($_SESSION['aplicacion'])) exit();

	class DbLink {
		//public $link;
		public static function getLinkPG($bd = "BD"){
			global $conf;
			$link = false;
			$host 		= $conf[$bd]['host'];  
			$puerto 	= $conf[$bd]['puerto']; 
			$dbname 	= $conf[$bd]['dbname'];  
			$dbuser 	= $conf[$bd]['dbuser']; 
			$dbpassword = $conf[$bd]['dbpassword']; 
			try{$link = @pg_connect("host=$host port=$puerto dbname=$dbname user=$dbuser password=$dbpassword"); //or die('Error de conexión: ' . pg_last_error());
			} catch (Exception $e) {$error = $e;}
			//print_r($link);exit;
			if($link != false){pg_query($link, "set client_encoding to 'UTF8'"); return $link;} 
			else{return false;}
			
			//$this->link = $link
		}

		public static function resultados($sql, $bd = "BD"){
			$link = DbLink::getLinkPG($bd);
			if(!$link) return false;
			$result = pg_query($link, $sql) or die('error de consulta: ' . pg_last_error());
			if (!$result) {echo 'error de consulta: ' . pg_last_error() . "<br/>\n";return false;}
			if (pg_num_rows($result) == 0) return false;
			$rows = array();
			while($r = pg_fetch_assoc($result)) {
			    $rows[] = $r;
			}
			return $rows;
		}

		public static function ejecutar($sql, $nombrequery="query", $bd = "BD"){
			$link = DbLink::getLinkPG($bd);
			$result = pg_prepare($link, $nombrequery, $sql);
			$result = pg_execute($link, $nombrequery,array());
			if ($result === false) {return "<br/>\n" . pg_last_error() . "<br/>\n";}
			else{return true;}
		}

	}
?>
