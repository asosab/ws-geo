<?php
/**
 * Archivo: index.php
 * Usuario: alesosa
 * Fecha: 06/05/16
 * Hora: 04:52 PM
 * Proyecto: webservice
 */
header("X-Clacks-Overhead: GNU Terry Pratchett");
require 'vendor/autoload.php';
require_once $_SERVER["DOCUMENT_ROOT"] ."/controlador/sesion.php";
require_once $_SERVER["DOCUMENT_ROOT"] ."/controlador/seguridad.php";
require_once $_SERVER["DOCUMENT_ROOT"] ."/controlador/config.php";
require_once $_SERVER["DOCUMENT_ROOT"] ."/controlador/lib.php";
if($conf['servicio']['inputs']){
  $fclase = $_SERVER["DOCUMENT_ROOT"] ."/servicios/inputs/inputs.php";
  if(is_file($fclase)){
    require_once $fclase;
    if(class_exists("inputs")) inputs::rec();
  }
} 
$data = array();
/**/
if(isset($servicio)){
  $fclase = $_SERVER["DOCUMENT_ROOT"]."/servicios/$servicio/$servicio.php";
  if(is_file($fclase)){
    require_once $fclase;
    if(isset($valor1)){
      if (method_exists($servicio, $valor1)){
        require_once $_SERVER["DOCUMENT_ROOT"] ."/controlador/dblink.php";
        require_once $_SERVER["DOCUMENT_ROOT"] ."/controlador/curl_lib.php";
        require_once $_SERVER["DOCUMENT_ROOT"] ."/servicios/bd/bd.php";
        if($conf['servicio']['acceso']){
          require_once $_SERVER["DOCUMENT_ROOT"] ."/servicios/acceso/acceso.php";
          $usuario = empty($_SESSION['usuario'])? null:$_SESSION['usuario'];
          $ejecutar = acceso::control("$servicio::$valor1", $usuario);
        } 
        else $ejecutar = true;
        if($ejecutar){
          $servicio::{$valor1}();
        }else{
          $data["error"][403] = "Acceso no autorizado a $valor1";
          entregar($data);
        }

      }else {
        $data["error"][404] = "El método '$valor1' no existe";
        entregar($data);
      }
    }else{
      $data["error"][404] = 'No se ha pedido un requerimiento al servicio';
      entregar($data);
    }

  }else{
    $lservicio = isset($lservicio)?$lservicio:"";
    $data["error"][404] = 'No existe el servicio ' . $lservicio;
    entregar($data);
  }
}else{
  $url = $conf['app']['web_base']."/client/index.html";
  header("Location: $url",true);
  die();
}

?>
