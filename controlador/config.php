<?php
/**
 * Archivo: config.php
 * Usuario: alesosa
 * Fecha: 30/09/13
 * Hora: 05:37 PM
 * Proyecto: webservice
 */

include_once("seguridad.php");

$conf = array(
	'app'						=>array(
		'version'								=> '19/06/2016',
		'nombre_corto'					=> 'WS',
		'nombre_largo'					=> 'Servicios Web',
		'path_inicio'						=> "",
		'web_base'							=> "https://" . $_SERVER['SERVER_NAME'],
		'puerto'								=> '443',
		'url_actual'						=> "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
		'url_partes'						=> explode('/', "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]"),
		'total_dir_profundidad'	=> max((count(explode('/', "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]")) - 4),0),
		'dir_profundidad'				=> str_repeat("../", max((count(explode('/', "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]")) - 4),0)),
		'produce' 							=> true // servidor en producción
	),
	'usuarios'			=>array( // bloque de usuarios especiales
		'super_usuario'					=> 'asosab@gmail.com',
		),
	'servicio'			=>array( // servicios activos en la ejecucion de cada petición al servidor
		'inputs'								=> false,// registro completo de todo lo que se envíe al servidor
		'FirePHP'								=> false,
		'controlip'							=> false, // control de accesos por IP
		'acceso'								=> false, // control de acceso a grupos de usuarios. 
		'correo'								=> false, // activa o desactiva el envío de correos
		'cron'									=> false, // coordina y ejecuta tareas definidas por base de datos o desde cada servicio
	),
	'BD'						=>array(   
		'host' 									=> '',
		'puerto' 								=> '',
		'dbname' 								=> '',
		'dbuser' 								=> '', 		
		'dbpassword' 						=> '',		
		'correo' 								=> ''
	),
	'BDmS' 			=> array (
    'host' 									=> '',
    'username' 							=> '', 
    'password' 							=> '',
    'db'										=> '',
    'port' 									=> '',
    'prefix' 								=> '',
    'charset' 							=> 'utf8'
    ),
	'correo'					=>array( 
		'CharSet' 		=> 'UTF-8',
		'SMTPDebug' 	=> '0', // 0 production, 1 client messages, 2 client and server messages
		'Debugoutput' => 'html',
		'Host' 				=> 'smtp.gmail.com',
		'Port' 				=> 587, 
		'SMTPSecure' 	=> 'tls', 
		'SMTPAuth'		=> true,
		'username' 		=> 'un.correo@gmail.com',
		'password' 		=> '',
		'From' 				=> 'un.correo@gmail.com',
		'ReplyTo'			=> 'un.correo@gmail.com',
		'Subject' 		=> 'test',
		'AltBody'			=> 'texto alternativo',
		'WordWrap'		=> 0
		),
);

getLlaves(); // Configuraciones personales no subidas a git

//echo $conf['BD']['dbname'];
//die();

$DBPG = new medoo([
    'database_type' 	=> 'pgsql',
    'database_name' 	=> 'data',
    'server' 					=> $conf['BD']['host'],
    'username' 				=> $conf['BD']['dbuser'],
    'password' 				=> $conf['BD']['dbpassword'],
    'charset' 				=> 'utf8'
]);



