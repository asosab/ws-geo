<?php
/**
 * Archivo: seguridad.php
 * Usuario: alesosa
 * Fecha: 30/09/13
 * Hora: 05:37 PM
 * Proyecto: webservice
 */
//echo $_SERVER['PHP_SELF'] ;
//if ($_SERVER['PHP_SELF'] !== "/webservice/index.php") {header( "Location: ../index.php" ); exit;}
$paginaTarda = false;
if(!empty($_SESSION['paginaTarda']))$paginaTarda = $_SESSION['paginaTarda'];
if(strpos($_SERVER['REQUEST_URI'], "paginaTarda")) $paginaTarda = true;

if($paginaTarda){
	ignore_user_abort(1); // sigue ejecutando aunque el usuario halla cerrado la conexión 
	set_time_limit(0); // el script no tiene límite de tiempo 
}

function getLlaves(){
	global $conf;
	$pfolder = $_SERVER["DOCUMENT_ROOT"]."/privado/";
	$fl = $pfolder."bdpg";
	if (file_exists($fl)) {
		$conf['BD']['host'] = file($fl, FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES)[0];
		$conf['BD']['puerto'] = file($fl, FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES)[1];
		$conf['BD']['dbname'] = file($fl, FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES)[2];
		$conf['BD']['dbuser'] = file($fl, FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES)[3];
		$conf['BD']['dbpassword'] = file($fl, FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES)[4];
		$conf['BD']['correo'] = file($fl, FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES)[5];
	}

	$fl = $pfolder."bdms";
	if (file_exists($fl)) {
		$conf['BDmS']['host'] = file($fl, FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES)[0];
		$conf['BDmS']['username'] = file($fl, FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES)[1];
		$conf['BDmS']['password'] = file($fl, FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES)[2];
		$conf['BDmS']['db'] = file($fl, FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES)[3];
		$conf['BDmS']['port'] = file($fl, FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES)[4];
	}	

	$fl = $pfolder."mail";
	if (file_exists($fl)) {
		$conf['correo']['username'] = file($fl, FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES)[0];
		$conf['correo']['password'] = file($fl, FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES)[1];
		$conf['correo']['From'] = $conf['correo']['username'];
		$conf['correo']['ReplyTo'] = $conf['correo']['username'];
	}

	$fl = $pfolder."servicios";
	if (file_exists($fl)) {
		$conf['servicio']['inputs'] = (file($fl, FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES)[0]==='true');
		$conf['servicio']['FirePHP'] = (file($fl, FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES)[1]==='true');
		$conf['servicio']['controlip'] = (file($fl, FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES)[2]==='true');
		$conf['servicio']['acceso'] = (file($fl, FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES)[3]==='true');
		$conf['servicio']['correo'] = (file($fl, FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES)[4]==='true');
		$conf['servicio']['cron'] = (file($fl, FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES)[5]==='true');
	}
}
