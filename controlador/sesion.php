<?php
	$muerte_de_sesion = 10000; //480; // ocho 360; // seis
	$segundos_galleta = 10000; //7200;
	// Sesión abierta por seis minutos (360 segundos)
	ini_set('session.gc_maxlifetime', $muerte_de_sesion);  
	// El cliente guardará su sesión
	session_set_cookie_params($segundos_galleta);
	session_start();
	if(!isset($_SESSION['aplicacion'])) $_SESSION['aplicacion'] = "ws";  // va a ser suplido por defined
	//defined('aplicacion') or define('aplicacion', 'ws');
	if(!isset($_SESSION['tiempo'])) $_SESSION['tiempo'] = time();

	$now = time();
	if (isset($_SESSION['discard_after']) && $now > $_SESSION['discard_after']) {
	    session_unset();
	    session_destroy();
	    session_start();
	}
	$_SESSION['discard_after'] = $now + $muerte_de_sesion;
?>